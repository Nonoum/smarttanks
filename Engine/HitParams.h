#pragma once
#include "imports.h"
#include "ObjectInterface.h"

namespace Engine {

struct HitParams {
	enum Type {
		DIRECT,
		ENVIRONMENT_HARD,
		ENVIRONMENT_SOFT,
		CROSS,
	};
	enum Flags {
		CONSUME_USEFUL = 1,
	};
	const Type type;
	const size_t power;
	const Geometry::Side side;
	Game::GameEngine& env;
	// uninitialized
	Geometry::Rect area;
	size_t n_injured;
	size_t n_killed;
	size_t n_detonated;
	size_t flags;
	size_t n_armors;
	size_t speeder_time;
	size_t player_cause_level;
	//------------------------------------
	HitParams(Type _type, size_t _power, Geometry::Side _side, Game::GameEngine& _env)
			: type(_type), power(_power), side(_side), env(_env) {

		n_injured = 0;
		n_killed = 0;
		n_detonated = 0;
		flags = 0;
		n_armors = 0;
		speeder_time = 0;
		player_cause_level = 0; // not caused by player
	};
};

}; // end Engine
