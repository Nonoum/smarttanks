#pragma once
#include "Object.h"
#include "../Game/GameEngine.h"
#include "../AnimationController.h"

namespace Engine {

namespace Templates {

/*
	Animation is always alpha-drawing here.
*/
template <class ChildClass, unsigned char _level, bool notify_on_done, bool _obstacle = false, bool _breakable = false>
	class AnimationObject : public Object<ChildClass>,
							public AnimationController<ChildClass> {

public:
	AnimationObject(Geometry::Rect rect = Geometry::Rect(), unsigned char level = _level)
		: Object<ChildClass>(rect, _breakable, _obstacle, true, level) {
	};
	bool live(Game::GameEngine&) {
		return AnimationController<ChildClass>::live() & notify_on_done;
	};
	bool draw(EngineCommon& env) const {
		if(! AnimationController<ChildClass>::hasAnimation()) {
			return false;
		}
		return this->drawImage(env, AnimationController<ChildClass>::getFrame());
	};
protected:
	static Geometry::Rect correctedRect(Geometry::Rect& offered) {
		return offered.getResizedRect( AnimationController<ChildClass>::getFramesSize(), 0, 0 ); // alignment = 0/0 (center/center)
	};
};

}; // end Templates

}; // end Engine
