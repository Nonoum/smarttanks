#include "ObjectSeparator.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Templates {

ObjectSeparator :: ~ObjectSeparator() {
	assert( backup_clone == NULL && "Backup protocol wasn't completed" );
	assert( rects_to_glue == NULL && "Registered rects wasn't processed" );
};

void ObjectSeparator :: addItem(ObjectInterface* item) {
	assert( item != NULL && "A valid ObjectInterface pointer expected" );
	items.addFirst(item);
};

void ObjectSeparator :: removeItem(ObjectInterface* item) {
	assert( item != NULL && "A valid ObjectInterface pointer expected" );
	for(CollectionType::LockIterator it(items); ! it.isDone(); ++it) {
		if(item == *it) {
			it.remove();
			return;
		}
	}
	assert( NULL && "Object not found on attempt of removing from separator's list" );
};

void ObjectSeparator :: separateByRect(ObjectInterface* item, const Geometry::Rect& destroyed_area, Geometry::Side incoming_side, EngineCommon::CollectionType& objects) {
	Geometry::Rect rects[4];
	const Geometry::Size min = EngineConfig::get().minObjectSize();
	const size_t count = item->getRect().splitCrossedRect(destroyed_area, rects, incoming_side);
	for(size_t i = 0; i < count; ++i) {
		if(! rects[i].canFit(min)) {
			// algirithm supposes to have ObjectSeparator in the end of objects that being processed
			// so here we registering the rects to expand after all items being hit and separated,
			// and actually expanding only in ObjectSeparator::live method
			registerRectForExpanding(rects[i]);
			continue;
		}
		ObjectInterface* obj = item->autoPoolObjectInterface(true); // cloning object
		obj->getRect() = rects[i];
		objects.addFirst( obj );
		items.addFirst( obj );
	}
};

void ObjectSeparator :: registerRectForExpanding(const Geometry::Rect& rect) {
	if(! rects_to_glue) {
		rects_to_glue = RectsList::create( RectsList() );
	}
	rects_to_glue->addFirst(rect);
};

void ObjectSeparator :: expandNeighbors(Geometry::Rect rect) {
	while(rect.width() && rect.height()) {
		ObjectInterface* best_obj = NULL;
		ssize best_square = 0;
		ssize best_additional = 0;
		Geometry::Side best_side_to_expand = Geometry::SIDE_NONE;

		for(CollectionType::LockIterator it(items); ! it.isDone(); ++it) {
			Geometry::Side side_to_expand = Geometry::SIDE_NONE;

			const Geometry::Rect& r = (*it)->getRect();
			if(rect.getXEnd() == r.x()) {
				if(rect.getYRange().includes( r.getYRange() )) {
					side_to_expand = Geometry::SIDE_LEFT;
				}
			} else if(rect.x() == r.getXEnd()) {
				if(rect.getYRange().includes( r.getYRange() )) {
					side_to_expand = Geometry::SIDE_RIGHT;
				}
			} else if(rect.getYEnd() == r.y()) {
				if(rect.getXRange().includes( r.getXRange() )) {
					side_to_expand = Geometry::SIDE_DOWN;
				}
			} else if(rect.y() == r.getYEnd()) {
				if(rect.getXRange().includes( r.getXRange() )) {
					side_to_expand = Geometry::SIDE_UP;
				}
			}

			if(side_to_expand != Geometry::SIDE_NONE) {
				const int x_dir = Geometry::XDirection(side_to_expand);
				const ssize intersect = x_dir ?
					(rect.getYRange() * r.getYRange()).length()
					: (rect.getXRange() * r.getXRange()).length();
				const ssize additional = x_dir ? rect.width() : rect.height();
				const ssize square = additional * intersect;
				if(square > best_square) {
					best_obj = *it;
					best_square = square;
					best_additional = additional;
					best_side_to_expand = side_to_expand;
				}
			}
		}
		if(best_obj) {
			// expanding the best possible object
			Geometry::Rect& r = best_obj->getRect();
			const int x_dir = Geometry::XDirection(best_side_to_expand);
			const int y_dir = Geometry::YDirection(best_side_to_expand);
			const Geometry::Size sz = x_dir ?
				Geometry::Size(r.width() + best_additional, r.height())
				: Geometry::Size(r.width(), r.height() + best_additional);
			r = r.getResizedRect(sz, -x_dir, -y_dir);

			// cut original rect to try to glue more
			Geometry::Side side_to_keep = Geometry::SIDE_NONE;
			if(x_dir) {
				side_to_keep = (r.y() - rect.y() > rect.getYEnd() - r.getYEnd()) ? Geometry::SIDE_DOWN : Geometry::SIDE_UP;
			} else {
				side_to_keep = (r.x() - rect.x() > rect.getXEnd() - r.getXEnd()) ? Geometry::SIDE_LEFT : Geometry::SIDE_RIGHT;
			}
			rect = rect.cut(r, side_to_keep);
		} else {
			return;
		}
	}
};

ObjectSeparator* ObjectSeparator :: getBackupClone() const {
	return backup_clone;
};

bool ObjectSeparator :: confirmBackupFrom(ObjectInterface* origin, size_t i_attempt) {
	// on 0th attempt saving cloned pointer to origin
	// on 1st attempt use clone's pointer by items connected to origin, to fix connection
	// on 2nd attempt lose pointer to clone in origin, then return true
	assert( dynamic_cast<ObjectSeparator*>(origin) != NULL && "Protocol error, incorrect origin for clone" );
	if(i_attempt == 0) {
		static_cast<ObjectSeparator*>(origin)->backup_clone = this;
	} else if(i_attempt == 2) {
		static_cast<ObjectSeparator*>(origin)->backup_clone = NULL;
	}
	return i_attempt >= 2;
};

bool ObjectSeparator :: draw(EngineCommon& env) const {
	return false;
};

bool ObjectSeparator :: live(Game::GameEngine& env) {
	if(rects_to_glue) {
		for(RectsList::Iterator it(*rects_to_glue); ! it.isDone(); ++it) {
			expandNeighbors(*it);
		}
		rects_to_glue->removeFromPool();
		rects_to_glue = NULL;
	}
	// return true to remove this object when list become empty
	return items.getTotalCount() == 0;
};

};

};
