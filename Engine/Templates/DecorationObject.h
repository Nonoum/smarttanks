#pragma once
#include "Object.h"
#include "../Game/GameEngine.h"
#include "../HitParams.h"
#include "../EngineConfig.h"
#include "ObjectSeparator.h"

namespace Engine {

namespace Templates {

/*
	Template of object that has image drawing (no animation).
	static 'init' method loads image from specified array.

	Usage of init for inherited classes A, B, C:
	Img32 images[10]; // load 10 images here
	uint16 total = 10;
	uint16 count = total;
	A::init(images + total - count, &count);
	B::init(images + total - count, &count);
	C::init(images + total - count, &count);
*/
template <class ChildClass, bool _breakable, bool _obstacle, bool _alpha_draw, unsigned char _level>
	class DecorationObject : public Object<ChildClass> {

	static Img32 img;
	ObjectSeparator* separator;
	typedef DecorationObject<ChildClass, _breakable, _obstacle, _alpha_draw, _level> ThisClass;
public:
	DecorationObject(Geometry::Rect rect = Geometry::Rect(), unsigned char level = _level)
		: Object<ChildClass>(rect, _breakable, _obstacle, _alpha_draw, level), separator(NULL) {
	};
	bool live(Game::GameEngine& env) {
		return false;
	};
	bool draw(EngineCommon& env) const {
		return this->drawImage(env, img);
	};
	static bool init(Img32* images, uint16* count_from_specified) {
		// takes one image
		if(*count_from_specified == 0) {
			return false;
		}
		images[0].swap(img);
		--(*count_from_specified);
		return true;
	};
	bool hit(HitParams& hit_params) { // will be generated for all child-classes for now, but in fact, implements only default separation logics (for walls)
		if(! this->isBreakable()) {
			return false; // get off, i'm untouchable
		}
		if(! this->answer(ObjectInterface::ASK_SEPARABLE) ) {
			return false; // expected an overload for it
		}
		if(hit_params.type == HitParams::DIRECT || hit_params.type == HitParams::CROSS) {
			return false; // miss direct hits, take ENVIRONMENT hits only
		}
		return separateByRect( hit_params.area, hit_params.side, hit_params.env.getObjects() );
	};
	bool confirmBackupFrom(ObjectInterface* origin, size_t i_attempt) { // overloading
		assert( dynamic_cast<ThisClass*>(origin) != NULL && "Protocol error, incorrect origin for clone" );
		if(separator) {
			if(i_attempt == 0) { // do nothing on 0th attempt
				return false;
			}
			// correct pointer on 1st attempt
			separator = separator->getBackupClone();
			assert( separator != NULL && "Protocol error, expected a valid prepared clone of separator" );
			separator->addItem(this);
		}
		return true;
	};
protected:
	static Geometry::Rect correctedRect(Geometry::Rect offered) { // returns rect with size of local image of object
		return offered.getResizedRect( Geometry::Size(img.width(), img.height()), -1, -1 ); // alignment = -1/-1 (min/min)
	};
private:
	bool separateByRect(const Geometry::Rect& destroyed_area, Geometry::Side incoming_side, EngineCommon::CollectionType& objects) {
		if(! this->getRect().crossing(destroyed_area)) {
			return false;
		}
		if(separator == NULL) {
			separator = static_cast<ObjectSeparator*>(ObjectSeparator().autoPoolObjectInterface(true));
			objects.addFirst( separator );
			separator->addItem(this);
		}
		separator->separateByRect(this, destroyed_area, incoming_side, objects);
		// current item is going to be destroyed, so removing it from separator
		separator->removeItem(this);
		return true;
	};
};

template <class ChildClass, bool _breakable, bool _obstacle, bool _alpha_draw, unsigned char _level>
	Img32 DecorationObject<ChildClass, _breakable, _obstacle, _alpha_draw,  _level> :: img;

}; // end Templates

}; // end Engine
