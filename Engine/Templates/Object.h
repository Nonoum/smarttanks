#pragma once
#include "../ObjectInterface.h"
#include "../EngineCommon.h"

namespace Engine {

namespace Templates {

template <class ChildClass> class Object : public ObjectInterface {
public:
	Object(const Geometry::Rect& rect, bool _breakable, bool _obstacle, bool _alpha, unsigned char _level)
		: ObjectInterface(rect, _breakable, _obstacle, _alpha, DrawingLevel(_level)) {
	};
	uint32 id(uint32 set_val) const {
		static uint32 _id = -1; // id of derived class
		if(_id == -1) {
			_id = set_val;
		}
		return _id;
	};
	const char* name(const char* set_val) const {
		static const char* _name = NULL; // name of derived class
		if(_name == NULL) {
			_name = set_val;
		}
		return _name;
	};
	ObjectInterface* autoPoolObjectInterface(bool clone = false) const {
		return clone ? autoPoolCloneObject() : autoPoolRawObject();
	};
protected:
	bool drawImage(EngineCommon& env, const Img32& img) const {
		const Geometry::Point pos = env.convertCoordinates(true, this->getPoint());
		// choose starting offsets in texture
		const ssize txt_offset_x = this->answer(ObjectInterface::ASK_SEPARABLE) ? this->x() : 0;
		const ssize txt_offset_y = this->answer(ObjectInterface::ASK_SEPARABLE) ? this->y() : 0;
		return env.getScreen().drawRect(pos.x(), pos.y(), this->width(), this->height(),
			img,
			txt_offset_x, txt_offset_y, this->isAlpha() ? Graphic32::D_ALPHA : Graphic32::D_SOLID);
	};
private:
	inline ChildClass* autoPoolRawObject(const ChildClass& init_val = ChildClass()) const {
		return AutoPoolGeneration::ObjectBase::create(init_val);
	};
	inline ChildClass* autoPoolCloneObject() const {
		const ChildClass& init_val = *static_cast<const ChildClass*>(this);
		return autoPoolRawObject( init_val );
	};
};

}; // end Templates

}; // end Engine
