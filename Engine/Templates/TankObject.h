#pragma once
#include "MovingObject.h"
#include "../Types/Bullet.h"
#include "../AnimationController.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Templates {

/*
	Template of tank object (shot and hit implementation)
*/
class ArmorCloudOwner;
typedef AnimationController<ArmorCloudOwner> ArmorCloudAnimation; // 'init' loads animation for it.

template <class ChildClass>
	class TankObject : public MovingObject<ChildClass, true, true, true, 10> {

	typedef MovingObject<ChildClass, true, true, true, 10> Parent;

	static const size_t RELOADING_TIME = 10;
	static const size_t BULLET_POWER = 41;

	Helpers::Timer reloading_timer;
	Helpers::Timer dying_timer;
	Helpers::Timer speeder_timer;
	size_t health;
	size_t armor;
	ArmorCloudAnimation armor_cloud;
public:
	TankObject(Geometry::Rect rect = Geometry::Rect(), Geometry::Side _side = Geometry::SIDE_LEFT)
			: Parent(Parent::correctedRect(rect, _side), _side) {

		health = 100;
		armor = 0;
	};
	size_t getHealth() const {
		return health;
	};
	size_t getArmor() const {
		return armor;
	};
	size_t getSpeederTime() const {
		return speeder_timer;
	};
	void addArmor(size_t count) {
		armor += count;
	};
	void addSpeederTime(size_t time) {
		speeder_timer.set( speeder_timer + time );
	};
	bool live(Game::GameEngine&) {
		if(speeder_timer.live()) {
			if(this->getSpeed()) {
				this->setSpeed(intendedSpeed());
			}
		}
		reloading_timer.live();
		if(armor) {
			armor_cloud.live();
		}
		return dying_timer.live();
	};
	bool hit(HitParams& hit_params) {
		if(hit_params.type != HitParams::ENVIRONMENT_HARD && hit_params.type != HitParams::DIRECT) {
			return false;
		}
		if(hit_params.player_cause_level && (! this->answer(ObjectInterface::ASK_PLAYER))) {
			const size_t lvl = hit_params.player_cause_level;
			Game::ScoreController& scorer = hit_params.env.getScorer();
			if(lvl == 1) {
				scorer.handleEvent(Game::ScoreController::S_DIRECT_HIT, 0);
			} else {
				scorer.handleEvent(Game::ScoreController::S_INDIRECT_HIT, lvl-1);
			}
		}
		++hit_params.n_injured;
		if(getArmor()) { // if tank has armor
			addArmor(-1); // consuming armor for hit
			return false; // out
		}
		if(! health) {
			// it's already dying, pass..
			return false;
		}
		const bool killed = hit_params.power >= health;
		health -= hit_params.power;
		if(killed) {
			health = 0;
			dying_timer.set(EngineConfig::get().tankDyingPeriod());
			++hit_params.n_killed;
		}
		return false; // don't remove object here, it will die slowly
	};
	Geometry::Rect getRectForBullet(Geometry::Side side_to_shoot) const {
		assert(side_to_shoot != Geometry::SIDE_NONE && "A valid side expected");
		Types::Bullet bullet(this->getRect(), side_to_shoot, BULLET_POWER); // current position - center of *this
		// correcting position
		const int x_dir = Geometry::XDirection(side_to_shoot);
		const int y_dir = Geometry::YDirection(side_to_shoot);
		ssize x_shift = 0, y_shift = 0;
		if(x_dir) {
			x_shift = (x_dir < 0) ? this->x() - bullet.getXEnd() : this->getXEnd() - bullet.x();
		} else { // y direction
			y_shift = (y_dir < 0) ? this->y() - bullet.getYEnd() : this->getYEnd() - bullet.y();
		}
		bullet.getPoint() = bullet.getPoint() + Geometry::Point(x_shift, y_shift);
		return bullet.getRect();
	};
	bool shotAttempt(Game::GameEngine& env) {
		// must be called in the end of 'live' method
		if(reloading_timer != 0 || health == 0) {
			return false;
		}
		Types::Bullet bullet(getRectForBullet(this->getDirection()), this->getDirection(), BULLET_POWER);
		bullet.setSpeed( bullet.getSpeed() + this->getSpeed() );
		prepareBullet(bullet);
		env.getObjects().addFirst( bullet.autoPoolObjectInterface(true) );
		reloading_timer.set(reloadingTime());
		return true;
	};
	bool draw(EngineCommon& env) const {
		bool result = false;
		if(getHealth() || (dying_timer & 1)) { // drawing flickering if it's dying
			result |= Parent::draw(env);
		}
		if(armor && armor_cloud.hasAnimation()) {
			const Geometry::Point pos = env.convertCoordinates(true, this->getPoint());
			result |= env.getScreen().drawRect(pos.x(), pos.y(), this->width(), this->height(), armor_cloud.getFrame(), 0, 0, Graphic32::D_ALPHA);
		}
		return result;
	};
	virtual void prepareBullet(Types::Bullet& bullet) {
	};
protected:
	HitParams::Flags getHitFlags() const {
		size_t flags = 0;
		if(getHealth()) { // dead tank can't consume armor
			flags |= HitParams::CONSUME_USEFUL;
		}
		return HitParams::Flags(flags);
	};
	bool isReloading() const {
		return reloading_timer != 0;
	};
	void consumeHitParams(HitParams& hit_params) {
		if(hit_params.n_armors) {
			addArmor(hit_params.n_armors);
		}
		if(hit_params.speeder_time) {
			addSpeederTime(hit_params.speeder_time);
			this->setSpeed(intendedSpeed());
		}
	};
	void setHealth(size_t _health) {
		health = _health;
	};
	virtual size_t intendedSpeed() const {
		return EngineConfig::get().tankSpeed() + (speeder_timer ? 1 : 0);
	};
	virtual size_t reloadingTime() const {
		return RELOADING_TIME;
	};
};

}; // end Templates

}; // end Engine
