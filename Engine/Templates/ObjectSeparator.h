#pragma once
#include "Object.h"
#include "../Game/GameEngine.h"
#include <assert.h>

namespace Engine {

namespace Templates {

/*
	Helper class for smart separation of objects on explosions.

	Stores list of items produced after initial separation of some item,
	gives own pointer to these items, on another separation uses list to modify
	neighbor items when removing one, to avoid missing of intermediate pieces.
*/
class ObjectSeparator : public Object<ObjectSeparator> {
	typedef AutoPoolUsing::List<ObjectInterface*> CollectionType;
	CollectionType items;

	struct RectsList : public AutoPoolGeneration::ObjectBase, public AutoPoolUsing::ListArray<Geometry::Rect> {
		RectsList() {};
		RectsList(const RectsList& other) {}; // empty copying
		RectsList& operator =(const RectsList& other) { return *this; }; // same here
	};
	RectsList* rects_to_glue;

	ObjectSeparator* backup_clone;
public:
	ObjectSeparator() : Object<ObjectSeparator>(Geometry::Rect(), false, false, false, 0), backup_clone(NULL), rects_to_glue(NULL) {};
	~ObjectSeparator();

	ObjectSeparator(const ObjectSeparator& other) : Object<ObjectSeparator>(Geometry::Rect(), false, false, false, 0), backup_clone(NULL), rects_to_glue(NULL) {
		assert( rects_to_glue == NULL && "Registered rects wasn't processed" );
		rects_to_glue = NULL; // for release build, if assert wasn't triggered
	}; // empty copying
	ObjectSeparator& operator =(const ObjectSeparator& other) { return *this; }; // same here

	void addItem(ObjectInterface* item);
	void removeItem(ObjectInterface* item);
	void separateByRect(ObjectInterface* item, const Geometry::Rect& destroyed_area, Geometry::Side incoming_side, EngineCommon::CollectionType& objects);

	bool draw(EngineCommon& env) const; // overloading
	bool live(Game::GameEngine& env); // ovrloading

	ObjectSeparator* getBackupClone() const;
	bool confirmBackupFrom(ObjectInterface* origin, size_t i_attempt); // overloading
private:
	void registerRectForExpanding(const Geometry::Rect& rect);
	void expandNeighbors(Geometry::Rect rect);
};

}; // end Templates

}; // end Engine
