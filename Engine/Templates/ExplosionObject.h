#pragma once
#include "AnimationObject.h"

namespace Engine {

namespace Templates {

/*
	Common logic of explosion.
*/
template <class ChildClass, unsigned char _level, HitParams::Type hit_type>
	class ExplosionObject : public AnimationObject<ChildClass, _level, true, false, false> {

	typedef AnimationObject<ChildClass, _level, true, false, false> Parent;
	size_t hit_power;
	bool hit_done;
	size_t player_cause_level;
public:
	ExplosionObject(Geometry::Rect rect = Geometry::Rect(), size_t power = -1)
				: Parent(Parent::correctedRect(rect)), hit_power(power) {

		hit_done = false;
		player_cause_level = 0;
	};
	bool live(Game::GameEngine& env) {
		bool result = Parent::live(env);
		if(! hit_done) { // hits environment after creation
			HitParams hit_params(hit_type, hit_power, Geometry::SIDE_NONE, env);
			hit_params.player_cause_level = player_cause_level ? player_cause_level + 1 : 0;
			hit_params.area = this->getRect();
			// walking and crushing
			for(Game::GameEngine::CollectionType::LockIterator it(env.getObjects()); ! it.isDone(); ) {
				if((*it)->crossing(this->getRect())) {
					if((*it)->hit(hit_params)) {
						(*it)->removeFromPool();
						it.remove();
						continue; // iterator made it's step in 'remove' method
					}
				}
				++it;
			}
			onExploading(env);
			hit_done = true;
		}
		return result;
	};
	virtual void onExploading(Game::GameEngine& env) { // empty implementation by default
	};
	void setPlayerCauseLevel(size_t p_level) {
		player_cause_level = p_level;
	};
};

}; // end Templates

}; // end Engine
