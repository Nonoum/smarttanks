#pragma once
#include "Object.h"
#include "../Game/GameEngine.h"
#include "../HitParams.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Templates {

/*
	Template of object that can move (in all directions).
*/
template <class ChildClass, bool _breakable, bool _obstacle, bool _alpha_draw, unsigned char _level>
	class MovingObject : public Object<ChildClass> {

	static Img32 imgs[4];
	Geometry::Side side; // side/direction
	size_t speed;
public:
	MovingObject(Geometry::Rect rect = Geometry::Rect(), Geometry::Side _side = Geometry::SIDE_LEFT, unsigned char level = _level)
			: Object<ChildClass>(rect, _breakable, _obstacle, _alpha_draw, level), side(_side) {

		speed = 0;
	};
	uint32 answer(ObjectInterface::Questions q) const {
		if(q == ObjectInterface::ASK_MOVABLE) {
			return 1;
		}
		return 0;
	};
	Geometry::Side getDirection() const {
		return side;
	};
	void setDirection(Geometry::Side direction) {
		side = direction;
		this->getRect() = correctedRect(this->getRect(), side);
	};
	size_t getSpeed() const {
		return speed;
	};
	void setSpeed(size_t _speed) {
		speed = _speed;
	};
	bool draw(EngineCommon& env) const {
		const Geometry::Side texture_index = Geometry::GetSideOrDefault(side, Geometry::SIDE_LEFT);
		return this->drawImage(env, imgs[texture_index]);
	};
	static bool init(Img32* images, uint16* count_from_specified) {
		// takes one image (LEFT-DIRECTION), rotates it to get images other directions
		if(*count_from_specified == 0) {
			return false;
		}
		images[0].swap(imgs[Geometry::SIDE_LEFT]);
		--(*count_from_specified);
		imgs[Geometry::SIDE_LEFT].rotate(1, imgs[Geometry::SIDE_UP]);
		imgs[Geometry::SIDE_LEFT].rotate(2, imgs[Geometry::SIDE_RIGHT]);
		imgs[Geometry::SIDE_LEFT].rotate(3, imgs[Geometry::SIDE_DOWN]);
		return true;
	};
	static Geometry::Size getSizeFor(Geometry::Side side) {
		side = Geometry::GetSideOrDefault(side, Geometry::SIDE_LEFT);
		return Geometry::Size(imgs[side].width(), imgs[side].height());
	};
protected:
	static Geometry::Rect correctedRect(Geometry::Rect offered, Geometry::Side side) { // returns rect with size of appropriate local image of object
		return offered.getResizedRect( getSizeFor(side), 0, 0 ); // alignment = 0/0 (center/center)
	};
	/*
		Internal struct to make moving logic common.
	*/
	struct MoveReturnRecord {
		const bool had_hit;
		const bool had_cross;
		const Geometry::Rect trail;
		MoveReturnRecord(bool hit, bool cross, Geometry::Rect trail_rect)
				: had_hit(hit), had_cross(cross), trail(trail_rect) {
		};
	};
	MoveReturnRecord move(EngineCommon& env, HitParams& hit_params) {
		const Geometry::Side side_to_keep = Geometry::Reverse(side);
		const int x_align = Geometry::XDirection(side_to_keep);
		const int y_align = Geometry::YDirection(side_to_keep);
		const ssize w_add = x_align ? speed : 0;
		const ssize h_add = y_align ? speed : 0;
		const Geometry::Size size( this->width() + w_add, this->height() + h_add );
		Geometry::Rect stretched = this->getRect().getResizedRect( size, x_align, y_align );
		bool error = false;
		bool had_hit = false;
		bool had_cross = false;
		//
		for(EngineCommon::CollectionType::LockIterator it(env.getObjects()); ! it.isDone(); ) {
			const Geometry::Rect obj_rect = (*it)->getRect();
			const bool obst = (*it)->isObstacle();
			const bool cross = obj_rect.crossing(stretched);
			bool removed = false;
			if(cross) { // objects are crossing, hit object
				had_cross = true;
				hit_params.area = (stretched & obj_rect).getResizedRect(Geometry::Size(1, 1), -x_align, -y_align); // center of crossing
				if( (*it)->hit(hit_params) ) {
					(*it)->removeFromPool();
					it.remove();
					removed = true;
				}
			}
			if(! removed) { // iterator have to be incremented manually
				++it;
			}
			// iterator is changed now
			if(cross && obst) { // object is crossing with obstacle. making cut of stretched Rect
				had_hit = true;
				Geometry::Rect cut = stretched.cut(obj_rect, side_to_keep);
				if(! cut.canFit( this->getSize() )) {
					error = true;
					break; // supposed to not happen ever
				}
				stretched = cut;
			}
		}
		if(! error) {
			this->getRect() = stretched.getResizedRect(this->getSize(), -x_align, -y_align);
		}
		const Geometry::Rect trail = stretched.cut(this->getRect(), side_to_keep);
		return MoveReturnRecord(had_hit, had_cross, trail);
	};
	// serialization implementation
	usize writeLocal(std::ostream& dst) const {
		uint32 buf = side;
		dst.write((char*)&buf, sizeof(buf));
		return sizeof(buf);
	};
	usize readLocal(std::istream& src) {
		uint32 buf = 0;
		src.read((char*)&buf, sizeof(buf));
		side = Geometry::Side(buf);
		return sizeof(buf);
	};
};

template <class ChildClass, bool _breakable, bool _obstacle, bool _alpha_draw, unsigned char _level>
	Img32 MovingObject<ChildClass, _breakable, _obstacle, _alpha_draw,  _level> :: imgs[4];

}; // end Templates

}; // end Engine
