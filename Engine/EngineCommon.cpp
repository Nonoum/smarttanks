#include "EngineCommon.h"
#include "EngineConfig.h"
#include "ResourceInitializer.h"
#include "EngineConfig.h"
#include <string.h> // memset
#include <stdexcept>

namespace Engine {

Geometry::Side KeyToSide(Controls key) {
	if(key == KEY_LEFT) return Geometry::SIDE_LEFT;
	if(key == KEY_RIGHT) return Geometry::SIDE_RIGHT;
	if(key == KEY_DOWN) return Geometry::SIDE_DOWN;
	if(key == KEY_UP) return Geometry::SIDE_UP;
	return Geometry::SIDE_NONE;
};

static const uint32 MAP_SIGNATURE = 0x02008874;

EngineCommon :: EngineCommon(bool _with_panel)
		: DisplayController(_with_panel), drawing_offset(0, 0) {

	ResourceInitializer init(ResourceInitializer::RES_FOR_SERIALIZED);
};

EngineCommon :: ~EngineCommon() {
	clearObjects(objects);
};

EngineCommon& EngineCommon :: getEngineCommon() {
	return *this;
};

EngineCommon::CollectionType& EngineCommon :: getObjects() {
	return objects;
};

void EngineCommon :: onFullUnlock() {
	DisplayController::onFullUnlock();
	if(file_read_request.isSet()) {
		readMap(file_read_request.param);
	}
	if(file_write_request.isSet()) {
		writeMap(file_write_request.param);
	}
};

void EngineCommon :: clearObjects(CollectionType& collection) {
	for(CollectionType::LockIterator it(collection); ! it.isDone(); ) {
		(*it)->removeFromPool(); // removing object from pool
		it.remove(); // removing pointer from collection
	}
};

void EngineCommon :: setGameSize(Geometry::Size size) {
	game_size = size;
};

Geometry::Size EngineCommon :: getGameSize() const {
	return game_size;
};

Geometry::Point EngineCommon :: convertCoordinates(bool object_to_screen, Geometry::Point coord) const {
	if(object_to_screen) {
		return coord - drawing_offset; // 'coord' is internal coordinates, converting to screen coordinates
	}
	return coord + drawing_offset; // 'coord' is screen coordinates, converting to internal coordinates
};

void EngineCommon :: adjustDrawingOffset(Geometry::Point adding) {
	drawing_offset += adding;
};

void EngineCommon :: resetDrawingOffset() {
	drawing_offset.setPoint(0, 0);
};

// file level functions

void EngineCommon :: readMap(std::string filename) {
	Helpers::AutoLocker auto_locker(*this, file_read_request);
	if(! auto_locker) {
		file_read_request.param = filename;
		return;
	}
	if(file_read_request.isSet()) {
		file_read_request.param.clear(); // cleaning
	}
	std::ifstream ifs(filename.c_str(), std::ios::binary);
	readMap( ifs );
};

void EngineCommon :: writeMap(std::string filename) {
	Helpers::AutoLocker auto_locker(*this, file_write_request);
	if(! auto_locker) {
		file_write_request.param = filename;
		return;
	}
	if(file_write_request.isSet()) {
		file_write_request.param.clear(); // cleaning
	}
	std::ofstream ofs(filename.c_str(), std::ios::binary);
	writeMap( ofs );
};

// stream level functions

void EngineCommon :: readMap(std::istream& src) {
	MapHeader hdr;
	readMapHeader(src, hdr);

	clearObjects(objects); // removing all previous objects
	CollectionType tmp; // temporary collection to reverse order of objects after reading
	for(size_t i = 0; i < hdr.obj_count; ++i) {
		ObjectInterface* obj = NULL;
		try {
			obj = object_transporter.read(src);
		} catch (std::exception& e) {
			clearObjects(tmp);
			throw e;
		}
		tmp.addFirst( obj );
	}
	for(CollectionType::LockIterator it(tmp); ! it.isDone(); ++it) {
		objects.addFirst( *it );
	}
	onMapLoaded();
};

void EngineCommon :: onMapLoaded() {
	resetDrawingOffset();
};

void EngineCommon :: writeMap(std::ostream& dst) {
	writeMapHeader(dst);

	for(CollectionType::LockIterator it(objects); ! it.isDone(); ++it) {
		object_transporter.write(dst, *it);
	}
};

// file header level functions

void EngineCommon :: readMapHeader(std::istream& src, MapHeader& output_header) {
	if(! src.read((char*)&output_header, sizeof(MapHeader))) {
		throw std::runtime_error(StringFor("cant_read_file"));
	}
	if(output_header.signature != MAP_SIGNATURE) {
		throw std::runtime_error(StringFor("not_tanks_map"));
	}
	if(output_header.engine_version > EngineConfig::get().engineVersion()) {
		throw std::runtime_error(StringFor("error_higher_version"));
	}
	game_size = Geometry::Size(output_header.gwidth, output_header.gheight);
};

void EngineCommon :: writeMapHeader(std::ostream& dst) const {
	const size_t n_locked = objects.calcLockedCount();
	assert((n_locked == 0) && "some objects are locked on writing map.");
	MapHeader header;
	memset(&header, 0, sizeof(MapHeader));
	header.signature = MAP_SIGNATURE;
	header.obj_count = uint32(objects.getTotalCount() - n_locked);
	header.gwidth = uint32(game_size.width());
	header.gheight = uint32(game_size.height());
	header.engine_version = EngineConfig::get().engineVersion();
	if(! dst.write((char*)&header, sizeof(header)) ) {
		throw std::runtime_error(StringFor("cant_write_file"));
	}
};

}; // end Engine
