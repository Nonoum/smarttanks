#include "Dynamite.h"
#include "LargeExplosion.h"

namespace Engine {

namespace Types {

Dynamite :: Dynamite(Geometry::Rect rect)
		: Parent(correctedRect(rect)) {
};

bool Dynamite :: hit(HitParams& hit_params) {
	if(hit_params.type == HitParams::CROSS) {
		return false;
	}
	LargeExplosion expl(getRect()); // centered by *this
	expl.setPlayerCauseLevel(hit_params.player_cause_level);
	hit_params.env.getObjects().addFirst( expl.autoPoolObjectInterface(true) );
	return true;
};

}; // end Types

}; // end Engine
