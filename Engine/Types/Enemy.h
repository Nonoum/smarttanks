#pragma once
#include "../Templates/TankObject.h"

namespace Engine {

namespace Types {

class Enemy : public Templates::TankObject<Enemy> {
	typedef Templates::TankObject<Enemy> Parent;
	bool registered;
	Helpers::Timer step_back_timer;
	Geometry::Side prev_ai_direction;
public:
	Enemy(Geometry::Rect rect = Geometry::Rect());
	bool live(Game::GameEngine& env);
};

}; // end Types

}; // end Engine
