#include "Quaker.h"
#include "../Game/GameEngine.h"
#include "../ProfileData.h"

namespace Engine {

namespace Types {

Quaker :: Quaker(double rotations, double radius, double dgrs_per_time, double max_percent)
		: Parent(Geometry::Rect(), false, false, false, 0), quake(rotations, radius, dgrs_per_time, max_percent) {
};

bool Quaker :: draw(EngineCommon& env) const {
	return false;
};

bool Quaker :: live(Game::GameEngine& env) {
	Geometry::Point& pt = env.specialDrawingOffset();
	ssize x = pt.x(), y = pt.y();
	const bool result = quake(&x, &y);
	pt.setPoint(x, y);
	return result;
};

void Quaker :: SupplyCalculatedQuaker(Game::GameEngine& env, const Geometry::Rect& epicenter, double power) {
	if(! ProfileData::get().areQuakersAllowed()) {
		return;
	}
	Game::PlayerControlParams params(KEY_GET_INFO, true);
	env.Game::PlayerController::notify(params);

	if(! params.health) {
		return; // player is dead, no quakers
	}
	const Geometry::Size scr_size = env.getRenderSize();
	const ssize estm_scr_size = (scr_size.width() + scr_size.height()) >> 1;
	const double scr_influence = double(estm_scr_size - 560) / 500.0; // screen size influence

	const ssize player_dist = params.placement.getPoint().absDist(epicenter.getPoint()) >> 1;
	double dist_influence = double(estm_scr_size - player_dist) / double(estm_scr_size * 2); // value is in (big negative ; 0.5]

	if(dist_influence * power < 0.03) {
		return; // almost no quake, quit
	}
	dist_influence *= dist_influence;

	const double rotations = 3.0 + dist_influence * 9.0;
	const double radius = 9.2 * power * dist_influence + scr_influence * 0.4;
	const double dgrs_per_time = 30.0 + scr_influence + dist_influence * 9.0;
	const double max_percent = 0.06;
	Types::Quaker q(rotations, radius, dgrs_per_time, max_percent);
	env.getObjects().addFirst( q.autoPoolObjectInterface(true) );
};

}; // end Types

}; // end Engine
