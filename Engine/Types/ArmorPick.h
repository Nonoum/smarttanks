#pragma once
#include "../Templates/AnimationObject.h"

namespace Engine {

namespace Types {

using Templates::AnimationObject;

class ArmorPick : public AnimationObject<ArmorPick, 15, false, false, false> {
	typedef AnimationObject<ArmorPick, 15, false, false, false> Parent;
public:
	ArmorPick(Geometry::Rect rect = Geometry::Rect());
	uint32 answer(Questions q) const;
	bool hit(HitParams& hit_params);
};

}; // end Types

}; // end Engine
