#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

class DarkPlateFloor : public Templates::DecorationObject<DarkPlateFloor, false, false, false, 1> {
	typedef Templates::DecorationObject<DarkPlateFloor, false, false, false, 1> Parent;
public:
	DarkPlateFloor(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
