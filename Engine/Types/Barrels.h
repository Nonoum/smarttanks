#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

class Barrels : public Templates::DecorationObject<Barrels, true, true, true, 15> {
	typedef Templates::DecorationObject<Barrels, true, true, true, 15> Parent;
public:
	Barrels(Geometry::Rect rect = Geometry::Rect());
	bool hit(HitParams& hit_params);
};

}; // end Types

}; // end Engine
