#pragma once
#include "../Templates/AnimationObject.h"

namespace Engine {

namespace Types {

using Templates::AnimationObject;

class SmallSmoke : public AnimationObject<SmallSmoke, 15, true, false, false> {
	typedef AnimationObject<SmallSmoke, 15, true, false, false> Parent;
public:
	SmallSmoke(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
