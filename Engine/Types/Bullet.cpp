#include "Bullet.h"
#include "SmallSmoke.h"
#include "SmallExplosion.h"
#include "SmallExplosion2.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Types {

static void Explode(Game::GameEngine::CollectionType& objects, const Geometry::Rect& area, size_t player_cause_level) {
	if(rand() & 1) {
		SmallExplosion expl(area);
		expl.setPlayerCauseLevel(player_cause_level);
		objects.addFirst( expl.autoPoolObjectInterface(true) );
	} else {
		SmallExplosion2 expl(area);
		expl.setPlayerCauseLevel(player_cause_level);
		objects.addFirst( expl.autoPoolObjectInterface(true) );
	}
};

Bullet :: Bullet(Geometry::Rect rect, Geometry::Side _side, size_t power)
		: Parent(correctedRect(rect, _side), _side), hit_power(power) {

	setSpeed(EngineConfig::get().bulletSpeed());
	caused_by_player = false;
};

size_t Bullet :: getPower() const {
	return hit_power;
};

void Bullet :: setPower(size_t power) {
	hit_power = power;
};

bool Bullet :: live(Game::GameEngine& env) {
	HitParams hit_params(HitParams::DIRECT, hit_power, Geometry::Reverse(getDirection()), env);
	hit_params.player_cause_level = caused_by_player ? 1 : 0;
	MoveReturnRecord result = move(env, hit_params);
	// creating smoke trail
	SmallSmoke smoke(result.trail);
	env.getObjects().addFirst( smoke.autoPoolObjectInterface(true) );
	// checking hit
	if(result.had_hit || hit_params.n_detonated) {
		Explode(env.getObjects(), hit_params.area, hit_params.player_cause_level);
		return true;
	}
	return false;
};

bool Bullet :: hit(HitParams& hit_params) {
	if(hit_params.type != HitParams::DIRECT) {
		return false;
	}
	Explode(hit_params.env.getObjects(), hit_params.area, hit_params.player_cause_level);
	++hit_params.n_detonated;
	return true;
};

void Bullet :: setCausedByPlayer() {
	caused_by_player = true;
};

}; // end Types

}; // end Engine
