#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

class StoneWall : public Templates::DecorationObject<StoneWall, false, true, false, 10> {
	typedef Templates::DecorationObject<StoneWall, false, true, false, 10> Parent;
public:
	StoneWall(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
