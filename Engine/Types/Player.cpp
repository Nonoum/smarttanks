#include "Player.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Types {

Player :: Player(Geometry::Rect rect)
		: Parent(rect) {

	connected = false;
	want_shoot = false;
	auto_move = true;
	shoot_repeat_canceled = true;
	achivs_speed = 0;
	achivs_reload_speed = 0;
	achivs_bullet_speed = 0;
	achivs_bullet_power = 0;
	achivs_loaded = false;
};

uint32 Player :: answer(Questions q) const {
	if(q == ASK_PLAYER) {
		return 1;
	}
	return Parent::answer(q);
};

bool Player :: live(Game::GameEngine& env) {
	bool result = Parent::live(env);
	if(! connected) {
		env.connect(this);
		if(! achivs_loaded) { // protecting from extra increasing of health on saving/restoring game state, because it's not the best place for initialization
			const Game::AchievementController& achivs = env.getAchivsController();
			achivs_speed = achivs.getBonus(Game::AchievementController::B_SPEED);
			achivs_reload_speed = achivs.getBonus(Game::AchievementController::B_RELOAD_SPEED);
			achivs_bullet_speed = achivs.getBonus(Game::AchievementController::B_BULLET_SPEED);
			achivs_bullet_power = achivs.getBonus(Game::AchievementController::B_BULLET_POWER);
			setHealth(getHealth() + achivs.getBonus(Game::AchievementController::B_HEALTH));
			achivs_loaded = true;
		}
		connected = true;
	}
	HitParams hit_params(HitParams::CROSS, 0, Geometry::Reverse(getDirection()), env);
	hit_params.flags = getHitFlags();
	MoveReturnRecord moving_result = move(env, hit_params);
	if(moving_result.had_hit) {
		setSpeed(0); // stop tank (only to have correct speed value for shooting)
	}
	if(want_shoot) {
		shotAttempt(env);
		want_shoot = false;
	}
	if(hit_params.n_armors) {
		env.getScorer().handleEvent(Game::ScoreController::S_ARMOR, hit_params.n_armors);
	}
	if(hit_params.speeder_time) {
		env.getScorer().handleEvent(Game::ScoreController::S_SPEEDER, hit_params.speeder_time);
	}
	consumeHitParams(hit_params);
	return result;
};

// handling users controls

void Player :: notificationHandler(Game::PlayerControlParams& params) {
	const Geometry::Side sd = KeyToSide(params.key);
	if(sd != Geometry::SIDE_NONE) {
		if(params.key_down) {
			setDirection(sd);
			setSpeed(intendedSpeed());
		} else if(! auto_move) {
			setSpeed(0);
		}
		return;
	}
	// non-moving keys
	if(! params.key_down) {
		if(params.key == KEY_SHOOT) {
			shoot_repeat_canceled = true;
		}
		return;
	}
	if(! shoot_repeat_canceled) {
		// this is made to not cancel shooting with pressed shoot-button after pressing other button (Windows workaround)
		want_shoot = true;
	}
	if(params.key == KEY_SHOOT) {
		shoot_repeat_canceled = false;
		want_shoot = true;
	} else if(params.key == KEY_GET_INFO) {
		params.health = getHealth();
		params.armor = getArmor();
		params.speeder = getSpeederTime();
		params.placement = getRect();
	} else if(params.key == KEY_SHIFT) {
		setSpeed(0);
	} else if(params.key == KEY_CONTROL) {
		auto_move ^= true;
	}
	return;
};

bool Player :: confirmBackupFrom(ObjectInterface* origin, size_t i_attempt) {
	connected = false;
	shoot_repeat_canceled = true;
	return true;
};

void Player :: prepareBullet(Types::Bullet& bullet) {
	bullet.setCausedByPlayer();
	bullet.setSpeed(bullet.getSpeed() + achivs_bullet_speed);
	bullet.setPower(bullet.getPower() + achivs_bullet_power);
};

size_t Player :: intendedSpeed() const {
	return achivs_speed + Parent::intendedSpeed();
};

size_t Player :: reloadingTime() const {
	return  Parent::reloadingTime() - achivs_reload_speed;
};

}; // end Types

}; // end Engine
