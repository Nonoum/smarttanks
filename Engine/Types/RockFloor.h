#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

class RockFloor : public Templates::DecorationObject<RockFloor, false, false, false, 1> {
	typedef Templates::DecorationObject<RockFloor, false, false, false, 1> Parent;
public:
	RockFloor(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
