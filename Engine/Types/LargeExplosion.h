#pragma once
#include "../Templates/ExplosionObject.h"
#include "Quaker.h"

namespace Engine {

namespace Types {

class LargeExplosion : public Templates::ExplosionObject<LargeExplosion, 20, HitParams::ENVIRONMENT_HARD> {
	typedef Templates::ExplosionObject<LargeExplosion, 20, HitParams::ENVIRONMENT_HARD> Parent;
public:
	LargeExplosion(Geometry::Rect rect = Geometry::Rect()) : Parent(rect, 22) {
	};
	void onExploading(Game::GameEngine& env) {
		Quaker::SupplyCalculatedQuaker(env, getRect(), 2);
	};
};

}; // end Types

}; // end Engine
