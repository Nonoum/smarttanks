#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

class Grape : public Templates::DecorationObject<Grape, false, false, true, 40> {
	typedef Templates::DecorationObject<Grape, false, false, true, 40> Parent;
public:
	Grape(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
