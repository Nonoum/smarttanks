#pragma once
#include "../Templates/MovingObject.h"

namespace Engine {

namespace Types {

using Templates::MovingObject;

class Bullet : public MovingObject<Bullet, true, false, true, 15> {
	typedef MovingObject<Bullet, true, false, true, 15> Parent;
	size_t hit_power;
	bool caused_by_player;
public:
	Bullet(Geometry::Rect rect = Geometry::Rect(), Geometry::Side _side = Geometry::SIDE_LEFT, size_t power = -1);
	size_t getPower() const;
	void setPower(size_t power);
	bool live(Game::GameEngine& env);
	bool hit(HitParams& hit_params);
	void setCausedByPlayer();
};

}; // end Types

}; // end Engine
