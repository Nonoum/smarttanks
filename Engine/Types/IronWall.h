#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

using Templates::DecorationObject;

class IronWall : public Templates::DecorationObject<IronWall, false, true, false, 10> {
	typedef Templates::DecorationObject<IronWall, false, true, false, 10> Parent;
public:
	IronWall(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
