#pragma once
#include "../Templates/AnimationObject.h"

namespace Engine {

namespace Types {

using Templates::AnimationObject;

class Speeder : public AnimationObject<Speeder, 15, false, false, false> {
	typedef AnimationObject<Speeder, 15, false, false, false> Parent;
public:
	Speeder(Geometry::Rect rect = Geometry::Rect());
	uint32 answer(Questions q) const;
	bool hit(HitParams& hit_params);
};

}; // end Types

}; // end Engine
