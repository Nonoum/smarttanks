#include "ArmorPick.h"
#include "../HitParams.h"

namespace Engine {

namespace Types {

static const size_t ARMORS_COUNT = 3;

ArmorPick :: ArmorPick(Geometry::Rect rect)
		: Parent(correctedRect(rect)) {
};

uint32 ArmorPick :: answer(Questions q) const {
	if(q == ASK_ARMOR) {
		return 1;
	}
	return 0;
};

bool ArmorPick :: hit(HitParams& hit_params) {
	if((hit_params.type != HitParams::CROSS) ||
		((hit_params.flags & HitParams::CONSUME_USEFUL) == 0)) {
		return false;
	}
	hit_params.n_armors += ARMORS_COUNT;
	return true;
};

}; // end Types

}; // end Engine
