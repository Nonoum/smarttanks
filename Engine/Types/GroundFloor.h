#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

class GroundFloor : public Templates::DecorationObject<GroundFloor, false, false, false, 0> {
	typedef Templates::DecorationObject<GroundFloor, false, false, false, 0> Parent;
public:
	GroundFloor(Geometry::Rect rect = Geometry::Rect()) : Parent(rect) {
	};
	uint32 answer(Questions q) const {
		if(q == ASK_SEPARABLE) {
			return 1;
		}
		return 0;
	};
};

}; // end Types

}; // end Engine
