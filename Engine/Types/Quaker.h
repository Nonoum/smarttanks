#pragma once
#include "../Templates/Object.h"

namespace Engine {

namespace Types {

class Quaker : public Templates::Object<Quaker> {
	typedef Templates::Object<Quaker> Parent;
	Geometry::Quaker quake;
public:
	Quaker(double rotations = 0, double radius = 0, double dgrs_per_time = 0, double max_percent = 0);
	bool draw(EngineCommon& env) const;
	bool live(Game::GameEngine& env);
	static void SupplyCalculatedQuaker(Game::GameEngine& env, const Geometry::Rect& epicenter, double power);
};

}; // end Types

}; // end Engine
