#include "Enemy.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Types {

static const size_t STEP_BACK_TIME_TACTS = 14;

Enemy :: Enemy(Geometry::Rect rect)
		: Parent(rect) {

	registered = false;
	prev_ai_direction = Geometry::SIDE_NONE;
	setSpeed(intendedSpeed());
};

bool Enemy :: live(Game::GameEngine& env) {
	if((! registered) && getHealth()) {
		env.registerEnemy();
		registered = true;
	} else if(registered && (! getHealth())) {
		env.unregisterEnemy();
		registered = false;
	}
	step_back_timer.live();

	const size_t nominal_speed = intendedSpeed();
	const bool result = Parent::live(env);
	HitParams hit_params(HitParams::CROSS, 0, Geometry::Reverse(getDirection()), env);
	hit_params.flags = getHitFlags();
	MoveReturnRecord moving_result = move(env, hit_params);
	if(moving_result.had_hit) {
		if((prev_ai_direction != Geometry::SIDE_NONE) && (! step_back_timer)) {
			// other tank faced, stopping seeking targets for a while
			step_back_timer.set(STEP_BACK_TIME_TACTS);
		}
		// by default, first setting random side to move
		setDirection( Geometry::Side(rand() & 0x03) );
		setSpeed(nominal_speed);
	}
	consumeHitParams(hit_params);
	// AI
	prev_ai_direction = Geometry::SIDE_NONE;
	Geometry::Rect bullet_rects[4];
	for(int s = Geometry::SIDE_ITERATOR; s < Geometry::SIDE_NONE; ++s) {
		bullet_rects[s] = getRectForBullet(Geometry::Side(s));
	}
	const AI::AIProcessor::ResultRecord ai_result = env.getAIProcessor().processAI(getRect(), nominal_speed, bullet_rects, isReloading(), step_back_timer == 0);
	if(ai_result.direction != Geometry::SIDE_NONE) {
		setDirection(ai_result.direction);
		setSpeed(ai_result.speed);
		prev_ai_direction = ai_result.direction;
	}
	if(ai_result.to_shot) {
		shotAttempt(env);
	}
	return result;
};

}; // end Types

}; // end Engine
