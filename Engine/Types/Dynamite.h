#pragma once
#include "../Templates/DecorationObject.h"

namespace Engine {

namespace Types {

using Templates::DecorationObject;

class Dynamite : public DecorationObject<Dynamite, true, true, true, 15> {
	typedef DecorationObject<Dynamite, true, true, true, 15> Parent;
public:
	Dynamite(Geometry::Rect rect = Geometry::Rect());
	bool hit(HitParams& hit_params);
};

}; // end Types

}; // end Engine
