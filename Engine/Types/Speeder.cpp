#include "Speeder.h"
#include "../HitParams.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Types {

static const size_t ACCELERATION_SECONDS = 15;

Speeder :: Speeder(Geometry::Rect rect)
		: Parent(correctedRect(rect)) {
};

uint32 Speeder :: answer(Questions q) const {
	if(q == ASK_SPEEDER) {
		return 1;
	}
	return 0;
};

bool Speeder :: hit(HitParams& hit_params) {
	if((hit_params.type != HitParams::CROSS) ||
		((hit_params.flags & HitParams::CONSUME_USEFUL) == 0)) {
		return false;
	}
	hit_params.speeder_time += ACCELERATION_SECONDS * EngineConfig::get().fps();
	return true;
};

}; // end Types

}; // end Engine
