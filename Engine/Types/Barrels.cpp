#include "Barrels.h"
#include "LargeExplosion.h"

namespace Engine {

namespace Types {

Barrels :: Barrels(Geometry::Rect rect)
		: Parent(correctedRect(rect)) {
};

bool Barrels :: hit(HitParams& hit_params) {
	if(hit_params.type == HitParams::CROSS) {
		return false;
	}
	const Geometry::Rect exps[] = {
		getRect().getResizedRect(Geometry::Size(width()/4, height()/4), -1, -1),
		getRect().getResizedRect(Geometry::Size(width()/4, height()/4), 1, -1),
		getRect().getResizedRect(Geometry::Size(width()/4, height()/4), 0, 1)
	};
	for(size_t i = 0; i < sizeof(exps)/sizeof(*exps); ++i) {
		LargeExplosion expl(exps[i]);
		expl.setPlayerCauseLevel(hit_params.player_cause_level);
		hit_params.env.getObjects().addFirst( expl.autoPoolObjectInterface(true) );
	}
	return true;
};

}; // end Types

}; // end Engine
