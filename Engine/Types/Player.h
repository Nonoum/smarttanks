#pragma once
#include "../Templates/TankObject.h"
#include "../Game/PlayerController.h"

namespace Engine {

namespace Types {

using Templates::TankObject;

class Player : public TankObject<Player>,
				public Game::PlayerConnector {

	typedef TankObject<Player> Parent;
	typedef Game::PlayerConnector Connector;

	bool connected;
	bool want_shoot;
	bool auto_move;
	bool shoot_repeat_canceled;
	size_t achivs_speed;
	size_t achivs_reload_speed;
	size_t achivs_bullet_speed;
	size_t achivs_bullet_power;
	bool achivs_loaded;
public:
	Player(Geometry::Rect rect = Geometry::Rect());
	uint32 answer(Questions q) const;
	bool live(Game::GameEngine& env);
	bool confirmBackupFrom(ObjectInterface* origin, size_t i_attempt); // overloading due to PlayerConnector
	void prepareBullet(Types::Bullet& bullet);
protected:
	size_t intendedSpeed() const;
	size_t reloadingTime() const;
	void notificationHandler(Game::PlayerControlParams& params);
};

}; // end Types

}; // end Engine
