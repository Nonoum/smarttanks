#pragma once
#include "../Templates/ExplosionObject.h"
#include "Quaker.h"

namespace Engine {

namespace Types {

using Templates::ExplosionObject;

class SmallExplosion2 : public ExplosionObject<SmallExplosion2, 20, HitParams::ENVIRONMENT_SOFT> {
	typedef ExplosionObject<SmallExplosion2, 20, HitParams::ENVIRONMENT_SOFT> Parent;
public:
	SmallExplosion2(Geometry::Rect rect = Geometry::Rect()) : Parent(rect, 1) {
	};
	void onExploading(Game::GameEngine& env) {
		Quaker::SupplyCalculatedQuaker(env, getRect(), 1.1);
	};
};

}; // end Types

}; // end Engine
