#include "AIEngine.h"
#include <assert.h>

namespace Engine {

namespace AI {

AIEngine :: AIEngine() {
	// optimization
	targets.reserve(128);
	last_ways.reserve(128);
};

void AIEngine :: addTarget(const Target& target) {
	targets.push_back(target);
};

void AIEngine :: clearResults() {
	last_ways.clear();
	way_tree.clear();
};

void AIEngine :: clearTargets() {
	targets.clear();
};

void AIEngine :: clear() {
	AITactData::clear();
	clearTargets();
};

const AIEngine::WaysType& AIEngine :: getWays() const {
	return last_ways;
};

size_t AIEngine :: searchWays(Geometry::Rect obj_area, size_t max_turns) {
	assert(way_tree.isEmpty() && "way tree must be clean here");
	const Geometry::Side directions[] = {
		Geometry::SIDE_LEFT,
		Geometry::SIDE_UP,
		Geometry::SIDE_RIGHT,
		Geometry::SIDE_DOWN
	};
	const size_t current_record_index = way_tree.push(obj_area, Geometry::SIDE_NONE);
	for(int i = 0; i < 4; ++i) {
		runRawRay(obj_area, directions[i], max_turns, obj_area.getSize(), current_record_index);
	}
	return searchWaysInWayTree(obj_area);
};

void AIEngine :: runRawRay(const Geometry::Rect& initial_area, Geometry::Side direction, size_t n_turns, const Geometry::Size& fit_size, size_t parent_record_index) {
	const int x_dir = Geometry::XDirection(direction);
	const int y_dir = Geometry::YDirection(direction);
	const ssize x_size = x_dir ? NNMLIB_SSIZE_MAX/2 : initial_area.width();
	const ssize y_size = y_dir ? NNMLIB_SSIZE_MAX/2 : initial_area.height();
	const Geometry::Size ray_size(x_size, y_size);
	const Geometry::Rect ray = initial_area.getResizedRect(ray_size, -x_dir, -y_dir);
	processRawRay(ray, direction, n_turns, fit_size, parent_record_index);
};

void AIEngine :: processRawRay(const Geometry::Rect& ray, Geometry::Side direction, size_t n_turns, const Geometry::Size& fit_size, size_t parent_record_index) {
	Geometry::Rect rays[3];
	const size_t split_count = splitRay(ray, direction, rays);
	if(split_count) {
		for(size_t i = 0; i < split_count; ++i) {
			if(rays[i].canFit(fit_size)) {
				processRawRay(rays[i], direction, n_turns, fit_size, parent_record_index);
			}
		}
		return;
	}
	// the ray is valid (without obstacles) - processing it
	const size_t current_record_index = way_tree.push(ray, direction, parent_record_index);
	// making new rotated *90 rays
	if(n_turns) {
		--n_turns;
		Geometry::Side dirs[2];
		dirs[0] = Geometry::XDirection(direction) ? Geometry::SIDE_DOWN : Geometry::SIDE_LEFT;
		dirs[1] = Geometry::Reverse(dirs[0]);
		for(size_t i = 0; i < 2; ++i) {
			runRawRay(ray, dirs[i], n_turns, fit_size, current_record_index);
		}
	}
};

size_t AIEngine :: searchWaysInWayTree(const Geometry::Size& fit_size) {
	last_ways.clear();
	for(TargetsType::const_iterator it = targets.begin(); it != targets.end(); ++it) {
		WayInfo winfo(it->type);
		if(way_tree.searchWayTo(*it, fit_size, winfo)) {
			last_ways.push_back(winfo);
		}
	}
	return last_ways.size();
};

Geometry::Side AIEngine :: sideForDirectReach(const Geometry::Rect path_origins[4], ssize limited_length, Target::Type type) const {
	for(TargetsType::const_iterator it = targets.begin(); it != targets.end(); ++it) {
		if(it->type != type) {
			continue;
		}
		for(int s = Geometry::SIDE_ITERATOR; s < Geometry::SIDE_NONE; ++s) {
			const Geometry::Side side = Geometry::Side(s);
			const int x_align = Geometry::XDirection(side);
			const int y_align = Geometry::YDirection(side);
			const Geometry::Rect& origin = path_origins[s];
			const Geometry::Size sz(x_align ? limited_length : origin.width(), y_align ? limited_length : origin.height());

			const Geometry::Rect stretched = path_origins[s].getResizedRect(sz, -x_align, -y_align);
			const Geometry::Rect cut = cutPath(stretched, side);
			if(cut.crossing(it->area)) {
				return side;
			}
		}
	}
	return Geometry::SIDE_NONE;
};

}; // end AI

}; // end Engine
