#pragma once
#include "AITactData.h"
#include <list>

namespace Engine {

namespace AI {

class WayTreeRecord {
public:
	typedef std::vector<size_t> IndexesType;
private:
	Geometry::Rect ray;
	Geometry::Side direction;
public:
	IndexesType* child_indexes;
public:
	WayTreeRecord(IndexesType* _reserved_indexes, const Geometry::Rect& _ray, Geometry::Side _direction);
	const Geometry::Rect& getRay() const;
	void addChild(size_t index);
	bool searchWayTo(const WayTreeRecord* root,
					const Target& target,
					WayInfo& output,
					const Geometry::Point& prev_start,
					Geometry::Side prev_dir,
					const Geometry::Size& fit_size,
					Geometry::Side first_path_side) const;
};

/*
	HARDCORE.
*/
class WayTree {
	typedef std::vector<WayTreeRecord> RecordsType;
	RecordsType records;
	typedef std::list<WayTreeRecord::IndexesType> IndexesReserve;
	IndexesReserve indexes_reserve; // without this cache algorithm would lead to catastrophical amount of memory allocations per second.
	IndexesReserve::iterator indexes_iterator;
	IndexesReserve::iterator bound_iterator;
public:
	WayTree();
	size_t push(const Geometry::Rect& ray, Geometry::Side direction, size_t parent_index = -1);
	bool searchWayTo(const Target& target, const Geometry::Size& fit_size, WayInfo& output) const;
	void clear();
	bool isEmpty() const;
private:
	void reserveIndexes(size_t count);
};

}; // end AI

}; // end Engine
