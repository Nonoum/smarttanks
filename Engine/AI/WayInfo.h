#pragma once
#include "Target.h"

namespace Engine {

namespace AI {

/*
	Contains information about way to target.
*/
struct WayInfo {
	Target::Type type;
	Geometry::Side side_to_move;
	size_t length;
	size_t first_path_length;

	WayInfo(Target::Type _type)
			: type(_type), side_to_move(Geometry::SIDE_NONE), length(0), first_path_length(0) {
	};
};

}; // end AI

}; // end Engine
