#pragma once
#include "AIEngine.h"
#include "../ObjectInterface.h" // for pre-declaration of GameEngine

namespace Engine {

namespace AI {

class AIProcessor {
	AIEngine engine;
public:
	enum AILevel {
		LEVEL_ITERATOR = 0,
		LEVEL_EASY = LEVEL_ITERATOR,
		LEVEL_NORMAL,
		LEVEL_HARD,
		LEVEL_VERY_HARD,
		LEVEL_COUNT
	};
	struct ResultRecord {
		Geometry::Side direction;
		bool to_shot;
		size_t speed;
		ResultRecord();
	};
	static const char* getLevelDescription(AILevel level);
	void setUpData(Game::GameEngine& env);
	ResultRecord processAI(const Geometry::Rect& position, size_t intended_speed, const Geometry::Rect bullet_rects[4], bool is_reloading, bool seek_targets);
};


}; // end AI

}; // end Engine
