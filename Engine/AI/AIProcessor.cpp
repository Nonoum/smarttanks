#include "AIProcessor.h"
#include "../Game/GameEngine.h"
#include "../EngineConfig.h"
#include "../ProfileData.h"
#include <assert.h>

namespace Engine {

namespace AI {

const char* AIProcessor :: getLevelDescription(AILevel level) {
	assert(level < LEVEL_COUNT && "Invalid enum value");
	switch (level) {
	case LEVEL_EASY:
		return StringFor("ai_level_easy");
	case LEVEL_NORMAL:
		return StringFor("ai_level_normal");
	case LEVEL_HARD:
		return StringFor("ai_level_hard");
	case LEVEL_VERY_HARD:
		return StringFor("ai_level_very_hard");
	default:
		break;
	};
	return "";
};

void AIProcessor :: setUpData(Game::GameEngine& env) {
	engine.clear();
	const AILevel current_level = ProfileData::get().aiLevel();
	if(current_level == LEVEL_EASY) {
		return;
	}
	for(Game::GameEngine::CollectionType::LockIterator it(env.getObjects()); ! it.isDone(); ++it) {
		if((*it)->answer(ObjectInterface::ASK_PLAYER)) {
			Target tgt(Target::PLAYER, (*it)->getRect());
			engine.addTarget(tgt);
		} else if((*it)->answer(ObjectInterface::ASK_ARMOR) || (*it)->answer(ObjectInterface::ASK_SPEEDER)) {
			if(current_level == LEVEL_VERY_HARD) {
				Target tgt(Target::USEFUL, (*it)->getRect());
				engine.addTarget(tgt);
			}
		} else if((*it)->isObstacle()) {
			if(! (*it)->answer(ObjectInterface::ASK_MOVABLE)) {
				engine.addObstacle( (*it)->getRect() );
			}
		}
	}
	engine.applyData();
};

AIProcessor::ResultRecord :: ResultRecord()
		: to_shot(false), direction(Geometry::SIDE_NONE), speed(0) {
};

struct WayComparator {
	bool operator ()(const WayInfo& max, const WayInfo& current) const {
		if(max.type != Target::PLAYER && current.type == Target::PLAYER) {
			return false;
		}
		if(max.type == Target::PLAYER && current.type != Target::PLAYER) {
			return true;
		}
		if(max.length == current.length) {
			return current.first_path_length < max.first_path_length;
		}
		return current.length < max.length;
	};
};

AIProcessor::ResultRecord AIProcessor :: processAI(const Geometry::Rect& position, size_t intended_speed, const Geometry::Rect bullet_rects[4], bool is_reloading, bool seek_targets) {
	const AILevel current_level = ProfileData::get().aiLevel();
	AIProcessor::ResultRecord result;
	result.speed = intended_speed;
	if(current_level == LEVEL_EASY) {
		if(rand() % 244 < 6) { // magic
			result.to_shot = true;
		}
		return result;
	}
	if(! is_reloading) {
		// can shoot now, check if can hit the target with direct shot
		const Geometry::Side sd = engine.sideForDirectReach(bullet_rects, EngineConfig::get().maxMapSize().width(), Target::PLAYER);
		if(sd != Geometry::SIDE_NONE) {
			result.direction = sd;
			result.to_shot = true;
			return result;
		}
	}
	if(current_level == LEVEL_NORMAL) {
		// enough here
		return result;
	}
	if(! seek_targets) {
		return result;
	}
	engine.clearResults();
	engine.searchWays(position, 1);
	const AIEngine::WaysType& ways = engine.getWays();
	AIEngine::WaysType::const_iterator way = std::max_element(ways.begin(), ways.end(), WayComparator());
	if(way != ways.end()) {
		result.direction = way->side_to_move;
		if(way->first_path_length && way->first_path_length < intended_speed) {
			result.speed = way->first_path_length;
		}
	}
	return result;
};

}; // end AI

}; // end Engine
