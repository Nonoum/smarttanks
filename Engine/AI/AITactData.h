#pragma once
#include "../imports.h"
#include "Target.h"
#include "WayInfo.h"
#include <vector>

namespace Engine {

namespace AI {

/*
	Contains cached data for AI logic.
	AITactData supposed to be initialized before calling AI logic:
		for(...)
			ai_tact_data.addObstacle(...);
		ai_tact_data.applyData();
*/
class AITactData {
protected:
	typedef std::vector<Geometry::Rect> RectsType;
private:
	RectsType rects[4]; // sorted collections for 4 directions
public:
	AITactData();
	void addObstacle(const Geometry::Rect& area);
	void applyData(); // prepares added data for using
	virtual void clear(); // cleans the data
protected:
	Geometry::Rect cutPath(const Geometry::Rect& path, Geometry::Side direction) const;
	size_t splitRay(const Geometry::Rect& ray, Geometry::Side direction, Geometry::Rect output[3]) const; // split the ray with nearest obstacle and create a new rays in (output). returns count of new rays
private:
	Geometry::Rect getFirstObstacle(const Geometry::Rect& path, Geometry::Side direction) const;
};

}; // end AI

}; // end Engine
