#include "AITactData.h"
#include <algorithm>
#include <assert.h>

namespace Engine {

namespace AI {

AITactData :: AITactData() {
	// optimization
	std::for_each(rects, rects+4, std::bind2nd(std::mem_fun_ref(&RectsType::reserve), 256));
};

void AITactData :: addObstacle(const Geometry::Rect& area) {
	// static_cast is needed to distinguish push_back of C++98 and push_back of C++11 for rvalue.
	std::for_each(rects, rects+4, std::bind2nd(std::mem_fun_ref(static_cast<void (RectsType::*)(const Geometry::Rect&)>(&RectsType::push_back)), area));
};

void AITactData :: clear() {
	std::for_each(rects, rects+4, std::mem_fun_ref(&RectsType::clear));
};

// functors ---------------------------------------------------------------------

inline bool Lefter(const Geometry::Rect& r1, const Geometry::Rect& r2) {
	return r1.x() < r2.x();
};

inline bool Downer(const Geometry::Rect& r1, const Geometry::Rect& r2) {
	return r1.y() < r2.y();
};

inline bool Righter(const Geometry::Rect& r1, const Geometry::Rect& r2) {
	return r1.getXEnd() > r2.getXEnd();
};

inline bool Upper(const Geometry::Rect& r1, const Geometry::Rect& r2) {
	return r1.getYEnd() > r2.getYEnd();
};

typedef bool (*Functor)(const Geometry::Rect&, const Geometry::Rect&);

class FunctorsInitializer {
	static Functor functors[4];
	FunctorsInitializer() {
		functors[Geometry::SIDE_LEFT] = Lefter;
		functors[Geometry::SIDE_UP] = Upper;
		functors[Geometry::SIDE_RIGHT] = Righter;
		functors[Geometry::SIDE_DOWN] = Downer;
	};
	friend Functor& GetFunctor(Geometry::Side direction);
};

Functor FunctorsInitializer :: functors[4];

inline Functor& GetFunctor(Geometry::Side direction) {
	assert((direction != Geometry::SIDE_NONE) && "expected valid side here");
	static FunctorsInitializer initer;
	return FunctorsInitializer::functors[direction];
};

// end functors -----------------------------------------------------------------

void AITactData :: applyData() {
	for(int i = 0; i < 4; ++i) {
		RectsType& c = rects[i];
		std::sort(c.begin(), c.end(), GetFunctor(Geometry::Side(i)));
	}
};

/*
	Function-helper for the following one.
*/
template <int x_align, int y_align> size_t AppendRayIfSizeIsPositive(const Geometry::Rect& ray, Geometry::Rect& output, ssize size) {
	if(size <= 0)
		return 0;
	output = ray.getResizedRect(Geometry::Size(x_align ? size : ray.width(), y_align ? size : ray.height()), x_align, y_align);
	return 1;
};

/*
	Specific function for splitting Ray rect with obstacle, returns count of sub-rays after splitting.
	one ray is full-wide ray of original (ray) until meeting (splitter), others is side rays
	with length of original (ray) placed around (splitter).
*/
static size_t SplitRay(const Geometry::Rect& ray, Geometry::Side direction, const Geometry::Rect& splitter, Geometry::Rect output[3]) {
	assert(ray.crossing(splitter) && "Ray was supposed to cross a splitter here");
	assert(direction != Geometry::SIDE_NONE && "A valid direction expected on splitting ray");
	const int x_dir = Geometry::XDirection(direction);
	const int y_dir = Geometry::YDirection(direction);
	ssize distance = 0;
	Geometry::Size main_sub_ray_size(0, 0);
	Geometry::Rect side_sub_rays[2];
	size_t side_rays_count = 0;
	if(x_dir) { // X direction
		if(direction == Geometry::SIDE_LEFT) {
			distance = ray.getXEnd() - splitter.getXEnd();
		} else { // SIDE_RIGHT, 100%
			distance = splitter.x() - ray.x();
		}
		main_sub_ray_size.setSize(distance, ray.height()); // full-wide ray
		// side sub-rays
		side_rays_count += AppendRayIfSizeIsPositive<0, -1>(ray, side_sub_rays[side_rays_count], splitter.y() - ray.y());
		side_rays_count += AppendRayIfSizeIsPositive<0, 1>(ray, side_sub_rays[side_rays_count], ray.getYEnd() - splitter.getYEnd());
	} else { // Y direction
		if(direction == Geometry::SIDE_DOWN) {
			distance = ray.getYEnd() - splitter.getYEnd();
		} else { // SIDE_UP, 100%
			distance = splitter.y() - ray.y();
		}
		main_sub_ray_size.setSize(ray.width(), distance); // full-wide ray
		// side sub-rays
		side_rays_count += AppendRayIfSizeIsPositive<-1, 0>(ray, side_sub_rays[side_rays_count], splitter.x() - ray.x());
		side_rays_count += AppendRayIfSizeIsPositive<1, 0>(ray, side_sub_rays[side_rays_count], ray.getXEnd() - splitter.getXEnd());
	}
	if(distance <= 0)
		return 0; // impossible/rare case
	output[0] = ray.getResizedRect(main_sub_ray_size, -x_dir, -y_dir);
	size_t result = 1;
	for(size_t i = 0; i < side_rays_count; ++i) {
		output[result] = side_sub_rays[i];
		++result;
	}
	return result;
};

size_t AITactData :: splitRay(const Geometry::Rect& ray, Geometry::Side direction, Geometry::Rect output[3]) const {
	const Geometry::Rect obstacle = getFirstObstacle(ray, direction);
	if(! obstacle) {
		return 0;
	}
	return SplitRay(ray, direction, obstacle, output);
};

Geometry::Rect AITactData :: cutPath(const Geometry::Rect& path, Geometry::Side direction) const {
	const Geometry::Rect obstacle = getFirstObstacle(path, direction);
	if(! obstacle) {
		return path;
	}
	return path.cut(obstacle, Geometry::Reverse(direction));
};

Geometry::Rect AITactData :: getFirstObstacle(const Geometry::Rect& path, Geometry::Side direction) const {
	typedef RectsType::const_iterator Iter;
	const Geometry::Side reversed = Geometry::Reverse(direction);
	const RectsType& c = rects[reversed];
	const std::pair<Iter, Iter> range = std::equal_range(c.begin(), c.end(), path, GetFunctor(reversed));
	// range.first is a bound iterator of possible obstacles
	const Iter found = std::find_if(range.first, c.end(), std::bind2nd(std::mem_fun_ref(&Geometry::Rect::crossing), path));
	return found != c.end() ? *found : Geometry::Rect();
};

}; // end AI

}; // end Engine
