#pragma once
#include "../imports.h"

namespace Engine {

namespace AI {

/*
	Target is anything that wanted to be reached.
*/
struct Target {
	enum Type {
		PLAYER,
		USEFUL,
	};
	Type type;
	Geometry::Rect area;
	Target(Type _type, Geometry::Rect _area) : type(_type), area(_area) {
	};
};

}; // end AI

}; // end Engine
