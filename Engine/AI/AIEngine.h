#pragma once
#include "AITactData.h"
#include "WayTree.h"

namespace Engine {

namespace AI {

class AIEngine : public AITactData {
public:
	typedef std::vector<WayInfo> WaysType;
private:
	typedef std::vector<Target> TargetsType;
	WaysType last_ways;
	TargetsType targets;
	WayTree way_tree;
public:
	AIEngine();
	void addTarget(const Target& target);
	void clearResults(); // clears the cache of results
	void clearTargets(); // clears the list of targets
	void clear(); // clears the tact data - targets and obstacles
	const WaysType& getWays() const;
	size_t searchWays(Geometry::Rect obj_area, size_t max_turns); // returns count of targets, for which were found ways
	Geometry::Side sideForDirectReach(const Geometry::Rect path_origins[4], ssize limited_length, Target::Type type) const;
private:
	void runRawRay(const Geometry::Rect& initial_area, Geometry::Side direction, size_t n_turns, const Geometry::Size& fit_size, size_t parent_record_index); // renders WayTreeRecord collection
	void processRawRay(const Geometry::Rect& ray, Geometry::Side direction, size_t n_turns, const Geometry::Size& fit_size, size_t parent_record_index);
	size_t searchWaysInWayTree(const Geometry::Size& fit_size); // uses rendered WayTree to find optimal way for every target
};

}; // end AI

}; // end Engine
