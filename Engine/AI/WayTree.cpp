#include "WayTree.h"
#include <assert.h>

namespace Engine {

namespace AI {

WayTreeRecord :: WayTreeRecord(IndexesType* _reserved_indexes, const Geometry::Rect& _ray, Geometry::Side _direction)
		: child_indexes(_reserved_indexes), ray(_ray), direction(_direction) {

	child_indexes->clear(); // cleaning previous data because it's reserved vector
};

const Geometry::Rect& WayTreeRecord :: getRay() const {
	return ray;
};

void WayTreeRecord :: addChild(size_t index) {
	child_indexes->push_back(index);
};

bool WayTreeRecord :: searchWayTo(const WayTreeRecord* root, const Target& target, WayInfo& output, const Geometry::Point& prev_start,
		Geometry::Side prev_dir, const Geometry::Size& fit_size, Geometry::Side first_path_side) const {

	size_t prev_block_length = 0; // length [prev_ray curr_ray] (has different logic of calculation depending of crossing target by current ray)
	ssize x_start = prev_start.x(), y_start = prev_start.y(); // positions of current ray to calculate length from
	const int x_dir = Geometry::XDirection(direction);
	const int y_dir = Geometry::YDirection(direction);
	const bool target_reached = ray.crossing(target.area);
	const Geometry::Point subpath_limits(ray.width() - fit_size.width(), ray.height() - fit_size.height());
	if(first_path_side == Geometry::SIDE_NONE) {
		first_path_side = direction; // so (first_path_side) is always correct, except for 0th level where is undetermined
	}

	if(prev_dir != Geometry::SIDE_NONE) {
		if(x_dir) {
			if(prev_dir == Geometry::SIDE_DOWN) {
				y_start = ray.getYEnd() - fit_size.height();
				prev_block_length = prev_start.y() - y_start;
			} else { // SIDE_UP, 100%
				y_start = ray.y();
				prev_block_length = y_start - prev_start.y();
			}
		} else if(y_dir) {
			if(prev_dir == Geometry::SIDE_RIGHT) {
				x_start = ray.x();
				prev_block_length = x_start - prev_start.x();
			} else { // SIDE_LEFT, 100%
				x_start = ray.getXEnd() - fit_size.width();
				prev_block_length = prev_start.x() - x_start;
			}
		}
	}

	output.first_path_length += prev_block_length;

	const Geometry::Point this_start(x_start, y_start);
	if(target_reached) {
		const Geometry::Point diff = target.area.getPoint() - this_start;
		const Geometry::Point abs_diff = diff.abs();
		output.length += abs_diff.x() + abs_diff.y() + prev_block_length;
		output.side_to_move = direction;
		if(first_path_side != Geometry::SIDE_NONE) { // not base
			const int first_x_dir = Geometry::XDirection(first_path_side);
			const int first_y_dir = Geometry::YDirection(first_path_side);
			if(first_x_dir * diff.x() > 0) {
				output.first_path_length += Helpers::min2(abs_diff.x(), subpath_limits.x());
			} else if(first_y_dir * diff.y() > 0) {
				output.first_path_length += Helpers::min2(abs_diff.y(), subpath_limits.y());
			}
		}
		return true;
	}
	size_t n_found = 0; // count of sub-ways to target found
	size_t min_value = -1; // minimum value of sub-ways lengths
	size_t min_first_path = -1;
	Geometry::Side best_direction = Geometry::SIDE_NONE;
	// saving otput to restore from modifications of cycle
	const WayInfo saved_output = output;

	if(prev_dir != Geometry::SIDE_NONE) {
		// currently, further using of this variable should be unreachable
		// reseting this value to avoid incorrect calculations of 'first_path_length'
		first_path_side = Geometry::SIDE_NONE;
	}

	for(size_t i = 0; i < child_indexes->size(); ++i) {
		if(root[(*child_indexes)[i]].searchWayTo(root, target, output, this_start, direction, fit_size, first_path_side)) {
			size_t len = output.length - saved_output.length;
			const Geometry::Side dir = output.side_to_move;
			if(! output.first_path_length) {
				// skip this
				output = saved_output;
				continue;
			}
			if((len < min_value) || (len == min_value && output.first_path_length < min_first_path)) { // minimum total length, or equal but better first path
				min_value = len;
				min_first_path = output.first_path_length;
				best_direction = dir;
			}
			++n_found;
		}
		output = saved_output;
	}
	if(! n_found) {
		return false;
	}
	output.length += min_value + prev_block_length;
	output.first_path_length = min_first_path;
	output.side_to_move = (direction == Geometry::SIDE_NONE ? best_direction : direction);
	return true;
};

// way tree

static const size_t RECORDS_RESERVE_PORTION = 64;
static const size_t CHILDS_RESERVE_PORTION = 32;

WayTree :: WayTree() {
	records.reserve(RECORDS_RESERVE_PORTION);
	reserveIndexes(RECORDS_RESERVE_PORTION);
	clear();
};

size_t WayTree :: push(const Geometry::Rect& ray, Geometry::Side direction, size_t parent_index) {
	const size_t curr_index = records.size();
	if(indexes_iterator == bound_iterator) {
		reserveIndexes(RECORDS_RESERVE_PORTION);
	}
	++indexes_iterator; // moving iterator
	records.push_back( WayTreeRecord(&*indexes_iterator, ray, direction) );
	if(parent_index != -1) {
		records[parent_index].addChild(curr_index);
	}
	return curr_index;
};

bool WayTree :: searchWayTo(const Target& target, const Geometry::Size& fit_size, WayInfo& output) const {
	assert((! isEmpty()) && "WayTree supposed to not be empty at moment of search");
	const Geometry::Point start(records[0].getRay().getPoint());
	return records[0].searchWayTo(&records[0], target, output, start, Geometry::SIDE_NONE, fit_size, Geometry::SIDE_NONE);
};

void WayTree :: clear() {
	records.clear();
	indexes_iterator = indexes_reserve.begin();
};

bool WayTree :: isEmpty() const {
	return records.size() == 0;
};

void WayTree :: reserveIndexes(size_t count) {
	for(size_t i = 0; i < count; ++i) {
		indexes_reserve.insert(indexes_reserve.end(), WayTreeRecord::IndexesType());
		IndexesReserve::iterator it = indexes_reserve.end();
		--it;
		(*it).reserve(CHILDS_RESERVE_PORTION);
	}
	bound_iterator = indexes_reserve.end();
	--bound_iterator;
};

}; // end AI

}; // end Engine
