#include "Strings.h"
#include <string>
#include <map>
#include <assert.h>

namespace Engine {

class StringsStorage {
	typedef std::map<std::string, const char*> CollectionType;
	CollectionType strs;

	StringsStorage() {
		strs["constructor_caption"] = "Smart Tanks Map Creator";
		strs["game_caption"] = "Smart Tanks";
		strs["map_params_caption"] = "Map parameters";
		strs["object_params_caption"] = "Object parameters";
		strs["types_list_caption"] = "Object types";

		strs["load_map"] = "Load map";
		strs["save_map"] = "Save map";
		strs["create_map"] = "Create map";
		strs["show_types_list"] = "Show types list";
		strs["about_button"] = "About";
		strs["help_button"] = "Help";

		strs["approve_map_creation"] = "create map";
		strs["approve_object_parameters"] = "Apply parameters";

		strs["about_caption"] = "About";

		strs["help_caption"] = "Help";

		strs["tanks_map_phrase"] = "Smart Tanks Map";

		strs["author"] = "Evgeniy ' Nonoum ' Evstratov";

		strs["ai_level_easy"] = " Easy";
		strs["ai_level_normal"] = " Normal";
		strs["ai_level_hard"] = " Hard";
		strs["ai_level_very_hard"] = " Critical";

		strs["error_reading_map"] = "Error during reading map..";
		strs["cant_read_file"] = "Can't read file.";
		strs["cant_write_file"] = "Can't write file.";
		strs["error_higher_version"] = "Map version is above game engine version.";
		strs["not_tanks_map"] = "Not a smart tanks map file.";
		strs["error_init_resource"] = "Error during initializing resource.";
		strs["error_init_timer"] = "Error during initializing timer.";
		strs["error_caption"] = "Error";
	};
public:
	static const StringsStorage& get() {
		static StringsStorage storage;
		return storage;
	};
	const char* stringFor(const char* key) const {
		static std::string mem_cache_string;

		mem_cache_string = key;
		const CollectionType::const_iterator iter = strs.find(mem_cache_string);
		if(iter == strs.end()) {
			assert(false && "String not found..");
			return "";
		}
		return iter->second;
	};
};

const char* StringFor(const char* key) {
	return StringsStorage::get().stringFor(key);
};

}; // end Engine
