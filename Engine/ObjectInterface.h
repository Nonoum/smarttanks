#pragma once
#include "imports.h"
#include "DrawingLevel.h"
#include <string>

namespace Engine {

// forward declarations
class EngineCommon;
struct HitParams;

namespace Game {
class GameEngine;
};

/*
	Common interface of game objects.
	Supposed to allocate objects in auto-pool memory.
*/
class ObjectInterface : public AutoPoolGeneration::ObjectBase, public Geometry::Rect {
	bool breakable;
	bool obstacle;
	bool alpha_draw;
	DrawingLevel level;
protected:
	ObjectInterface();
public:
	enum Questions {
		ASK_PLAYER,
		ASK_ARMOR,
		ASK_SEPARABLE, // (resizeable)
		ASK_MOVABLE,
		ASK_SPEEDER,
	};
public:
	ObjectInterface(const Geometry::Rect& rect, bool _breakable, bool _obstacle, bool _alpha, const DrawingLevel& _level);
	virtual ~ObjectInterface();
	// user virtual methods
	virtual bool live(Game::GameEngine&) = 0; // true if object have to be removed
	virtual bool draw(EngineCommon&) const = 0;
	// implemented virtual methods
	virtual uint32 answer(Questions q) const; // custom logic
	virtual bool hit(HitParams& hit_params); // true if object need to be removed
	// template-implemented virtual methods
	virtual uint32 id(uint32 set_val = -1) const = 0; // defined in class 'Object'
	virtual const char* name(const char* set_val = NULL) const = 0; // defined in class 'Object'
	virtual ObjectInterface* autoPoolObjectInterface(bool clone = false) const = 0; // defined in class 'Object'
	// completed methods
	bool isBreakable() const {
		return breakable;
	};
	bool isObstacle() const {
		return obstacle;
	};
	bool isAlpha() const {
		return alpha_draw;
	};
	const DrawingLevel& getLevel() const {
		return level;
	};
	usize write(std::ostream&) const; // writes binary stream of object without type ID
	usize read(std::istream&); // reads binary stream of object without type ID
	std::string get() const; // returns a string with parameters of object
	bool set(std::string); // sets object parameters from string, and returns true if succeeds
	void drawCorpse(EngineCommon& env, uint32 color) const; // draws corpse conturs
	// implemented virtual methods
	virtual usize writeLocal(std::ostream&) const; // empty in base
	virtual usize readLocal(std::istream&); // empty in base
	virtual std::string getLocal() const; // empty in base
	virtual bool setLocal(std::string& src); // true if succeeds

	virtual bool confirmBackupFrom(ObjectInterface* origin, size_t i_attempt); // true if backup creation is completed, false if more attempts is required
};

}; // end Engine
