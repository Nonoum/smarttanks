#include "ResourceInitializer.h"
#include "AnimationController.h" // for using of loading batch of images
#include "imports.h"

// decoration object types
#include "Types/GroundFloor.h"
#include "Types/DesertFloor.h"
#include "Types/RockFloor.h"
#include "Types/BrickWall.h"
#include "Types/StoneWall.h"
#include "Types/Grape.h"
#include "Types/Dynamite.h"
#include "Types/Barrels.h"
#include "Types/YellowBrickWall.h"
#include "Types/DesertGrassFloor.h"
#include "Types/DarkPlateFloor.h"
#include "Types/IronWall.h"

// moving object types
#include "Types/Player.h"
#include "Types/Bullet.h"
#include "Types/Enemy.h"

// animation objects
#include "Types/SmallExplosion.h"
#include "Types/LargeExplosion.h"
#include "Types/ArmorPick.h"
#include "Types/SmallSmoke.h"
#include "Types/Speeder.h"
#include "Types/SmallExplosion2.h"

// others
#include "Game/InfoPanelController.h"
#include "ProfileData.h"

#include <assert.h>
#include <stdexcept>

namespace Engine {

bool ResourceInitializer :: initialized[RES_COUNT] = {}; // all-false

using Helpers::Filesystem::Join;

#ifdef _DEBUG
static std::string common_resources_dir = Join(Join(Join("..", ".."), ".."), "Output");
static std::string library_dir = common_resources_dir;
static std::string game_resources_dir = library_dir;
static std::string creator_resources_dir = library_dir;
static std::string stuff_dir = Join(library_dir, "maps");
#else
static std::string common_resources_dir("..");
static std::string library_dir = common_resources_dir;
static std::string game_resources_dir = Join(library_dir, "game");
static std::string creator_resources_dir = Join(library_dir, "creator");
static std::string stuff_dir = Join(library_dir, "stuff");
#endif


//-----------------------------------------------------------------

static void RegisterImageBySwapping(const char* image_name, Img32& img) {
	Graphic32::GUI::ImageStorage* istorage = new Graphic32::GUI::ImageStorage(image_name);
	istorage->get().swap(img);
	Graphic32::GUI::GUIObject::images.addDifferent(istorage);
};

static bool RegisterImage(const char* image_name, const std::string& filename, bool is_tga) {
	Graphic32::Img32 i;
	const char* name = filename.c_str();
	const bool success = (is_tga ? i.readTGA( name ) : i.readCimg( name )) != 0;
	if(! success) {
		return false;
	}
	RegisterImageBySwapping(image_name, i);
	return true;
};

static bool RegisterTgaImage(const char* image_name, const std::string& filename) {
	return RegisterImage(image_name, filename, true);
};

static bool RegisterCimgImage(const char* image_name, const std::string& filename) {
	return RegisterImage(image_name, filename, false);
};


static bool LoadResForSerialized() {
	Img32* images = NULL;
	uint16 count = 0;
	if(! ImageBatchLoader::loadFromCimgs( Join(common_resources_dir, "resources.cimgs").c_str(), images, count )) {
		return false;
	}
	Helpers::CustomAutoPtr<Img32> imagesptr(images); // making this ptr auto-releasable

	uint16 count_left = count;
	bool result = true;
	bool (*const fs[])(Img32*, uint16*) = {&Types::GroundFloor::init,
										&Types::DesertFloor::init,
										&Types::RockFloor::init,
										&Types::BrickWall::init,
										&Types::StoneWall::init,
										&Types::Grape::init,
										&Types::Dynamite::init,
										&Types::Barrels::init,
										&Types::Player::init,
										&Types::Enemy::init,
										&Types::Bullet::init,
										&Types::YellowBrickWall::init,
										&Types::DesertGrassFloor::init,
										&Types::DarkPlateFloor::init,
										&Types::IronWall::init};
	for(int i = 0; i < sizeof(fs)/sizeof(*fs); ++i) {
		result &= fs[i](images + count - count_left, &count_left);
	}

	result &= Types::ArmorPick::init( Join(common_resources_dir, "armor.cimgs").c_str() );
	result &= Types::Speeder::init( Join(common_resources_dir, "speeder.cimgs").c_str() );
	return result;
};

static bool LoadResForGameOnly() {
	bool result = true;
	result &= Types::SmallExplosion::init( Join(game_resources_dir, "bfire.cimgs").c_str() );
	result &= Types::LargeExplosion::init( Join(game_resources_dir, "efire.cimgs").c_str() );
	result &= Types::SmallSmoke::init( Join(game_resources_dir, "ssmoke.cimgs").c_str() );
	result &= Templates::ArmorCloudAnimation::init( Join(game_resources_dir, "cloud.cimgs").c_str() );
	result &= Game::InfoPanelController::init( Join(game_resources_dir, "panel_resources.cimgs").c_str() );
	result &= Types::SmallExplosion2::init( Join(game_resources_dir, "bfire2.cimgs").c_str() );
	return result;
};

static bool LoadLibrary() {
	bool res = true;
	res &= Graphic32::TextPrinter::initFonts( Join(library_dir, "nnmlib1.1_fonts.cimgs").c_str() );
	res &= Graphic32::GUI::GUIObject::initGUI( Join(library_dir, "nnmlib1.1_gui.cimgs").c_str() );
	return res;
};

static bool LoadGUIImages() {
	bool res = RegisterTgaImage("window-background", Join(creator_resources_dir, "background.tga"));
	res &= RegisterTgaImage("text-background", Join(creator_resources_dir, "text_background.tga"));
	return res;
};

static bool LoadProfileData() {
	return ProfileData::init( Join(common_resources_dir, "profile.dat").c_str(), stuff_dir );
};

static bool LoadGameGUIImages() {
	bool res = RegisterCimgImage("game-menu-background", Join(game_resources_dir, "game_menu_background.cimg"));

	Img32* images = NULL;
	uint16 count = 0;
	if(! ImageBatchLoader::loadFromCimgs( Join(game_resources_dir, "game_gui.cimgs").c_str(), images, count )) {
		return false;
	}
	Helpers::CustomAutoPtr<Img32> imagesptr(images); // making this ptr auto-releasable
	if(count < 6) {
		return false;
	}
	RegisterImageBySwapping("game-settings-button", images[0]);
	RegisterImageBySwapping("game-about-button", images[1]);
	RegisterImageBySwapping("game-back-button", images[2]);
	RegisterImageBySwapping("game-exit-button", images[3]);
	RegisterImageBySwapping("game-help-button", images[4]);
	RegisterImageBySwapping("game-text-background", images[5]);
	return res;
};

static bool LoadCommonGUIImages() {
	bool res = RegisterCimgImage("simple-background", Join(common_resources_dir, "simple_background.cimg"));
	return res;
};

static bool (*functions[])() = {
	LoadResForSerialized,
	LoadResForGameOnly,
	LoadLibrary,
	LoadGUIImages,
	LoadProfileData,
	LoadGameGUIImages,
	LoadCommonGUIImages
};

ResourceInitializer :: ResourceInitializer(Resources required, bool forced) {
	assert(required < RES_COUNT && "invalid enum value");
	if(initialized[required] && (! forced)) {
		return;
	}
	if( functions[required]() ) {
		initialized[required] = true;
	} else {
		throw std::runtime_error(StringFor("error_init_resource"));
	}
};

}; // end Engine
