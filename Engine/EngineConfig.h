#pragma once
#include "imports.h"

namespace Engine {

class EngineConfig {
	// colors
	uint32 corpses_color; // color of corpses of objects to display in map constructor
	uint32 corpses_selected_color; // color of corpse of selected object
	uint32 text_color; // color of text of objects -:-
	uint32 grid_color; // color of grid in map maker

	// sizes
	ssize info_panel_height; // height of bottom information panel in the game
	ssize constructor_big_step; // big moving step in constructor
	ssize grid_step; // step for grid in map maker
	ssize constructor_gui_height; // height of GUI part of constructor window (bottom panel)
	Geometry::Size min_window_size; // minimum size of engine window
	Geometry::Size min_object_size; // minimum size of object after separating
	// min_map_size supposed to be equal to min_window_size
	Geometry::Size max_map_size; // maximum size for engine map
	ssize safe_zone_percent; // safe zone (as 100*percent of each map size) - nimimum displayed zone size around player

	// others
	Graphic32::FontInfo small_font;
	Graphic32::FontInfo large_font;

	//
	uint32 engine_version; // distinct version of engine to check on loading maps
	const char* string_version; // version in full string representation

	// physical/behavior parameters
	size_t tank_dying_period;
	size_t tank_speed;
	size_t bullet_speed;
	size_t frames_per_second;

	//------------------------------------------------------
	EngineConfig();
	void checkConfig();
public:
	static const EngineConfig& get();

	inline const Graphic32::FontInfo& sFont() const { return small_font; };
	inline const Graphic32::FontInfo& lFont() const { return large_font; };

	inline uint32 textColor() const { return text_color; };
	inline uint32 corpsesColor() const { return corpses_color; };
	inline uint32 sCorpsesColor() const { return corpses_selected_color; };
	inline uint32 gridColor() const { return grid_color; };

	inline ssize gridStep() const { return grid_step; };
	inline ssize infoPanelHeight() const { return info_panel_height; };
	inline ssize constructorGUIHeight() const { return constructor_gui_height; };
	inline ssize constructorBigStep() const { return constructor_big_step; };

	inline Geometry::Size minWindowSize() const { return min_window_size; };
	inline Geometry::Size minObjectSize() const { return min_object_size; };
	inline Geometry::Size maxMapSize() const { return max_map_size; };

	inline ssize safeZonePercent() const { return safe_zone_percent; };

	inline uint32 engineVersion() const { return engine_version; };
	inline const char* stringVersion() const { return string_version; };

	inline size_t tankDyingPeriod() const { return tank_dying_period; };
	inline size_t tankSpeed() const { return tank_speed; };
	inline size_t bulletSpeed() const { return bullet_speed; };
	inline size_t fps() const { return frames_per_second; };
};

}; // end Engine
