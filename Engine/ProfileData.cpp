#include "ProfileData.h"

namespace Engine {

std::string ProfileData :: maps_dir;
std::string ProfileData :: file_name;

ProfileData :: ProfileData() {
	reset();
};

ProfileData :: ~ProfileData() {
	if(file_name.length() > 0) {
		try {
			std::ofstream ofs(file_name.c_str());
			write( ofs );
		} catch (std::exception&) {
		}
	}
};

void ProfileData :: reset() {
	screen_size.setSize(800, 600);
	maps_list.clear();
	ai_level = AI::AIProcessor::LEVEL_HARD;
	allow_quakers = true;
};

ProfileData& ProfileData :: get() {
	static ProfileData profile;
	return profile;
};

bool ProfileData :: init(const char* fname, std::string& dir) {
	file_name = fname;
	maps_dir = dir;

	try {
		std::ifstream ifs(fname);
		get().read( ifs );
	} catch (std::exception&) {
		return false;
	}
	return true;
};

const std::vector<std::string>& ProfileData :: maps() const {
	return ProfileData::maps_list;
};

std::string ProfileData :: mapPath(size_t map_index_in_list) const {
	return Helpers::Filesystem::Join(maps_dir, maps_list[map_index_in_list].c_str());
};

// helpers for reading fields of profile data

bool SizeReader(const char* s, ProfileData& pd) {
	Geometry::Size sz(&s);
	if(! s) {
		return false;
	}
	pd.screenSize() = sz;
	return true;
};

bool LevelReader(const char* s, ProfileData& pd) {
	size_t level = atoi(s);
	if(level >= AI::AIProcessor::LEVEL_COUNT) {
		return false;
	}
	pd.aiLevel() = AI::AIProcessor::AILevel(level);
	return true;
};

bool QuakersOptionReader(const char* s, ProfileData& pd) {
	size_t q_allowed = atoi(s);
	if(q_allowed > 1) {
		return false;
	}
	pd.areQuakersAllowed() = q_allowed ? true : false;
	return true;
};

bool MapsControllerReader(const char* s, ProfileData& pd) {
	pd.mapsControllerData() = s;
	return true;
};

bool AchivsControllerReader(const char* s, ProfileData& pd) {
	pd.achivsControllerData() = s;
	return true;
};

typedef bool (*ReaderFunction)(const char* s, ProfileData& pd);
static ReaderFunction R_FUNCTIONS[] = {SizeReader, LevelReader, QuakersOptionReader, MapsControllerReader, AchivsControllerReader};
static size_t R_FUNCTIONS_CNT = sizeof(R_FUNCTIONS) / sizeof(*R_FUNCTIONS);

// end helpers zone

void ProfileData :: read(std::istream& is) {
	reset();
	std::vector<std::string> strs;
	std::string str;
	// parsing whole file
	while(getline(is, str)) {
		strs.push_back(str);
	}
	// reading maps list, that ends with an empty line
	size_t i = 0;
	for(; i < strs.size() && strs[i].length() > 0; ++i) {
		maps_list.push_back(strs[i]);
	}
	// reading other properties
	for(size_t f_idx = 0; f_idx < R_FUNCTIONS_CNT; ++f_idx) {
		if(strs.size() <= i + 1) {
			return;
		}
		const char* s = strs[++i].c_str();
		if(! R_FUNCTIONS[f_idx](s, *this)) {
			return;
		}
	}
};

void ProfileData :: write(std::ostream& os) {
	for(size_t i = 0; i < maps_list.size(); ++i) {
		os << maps_list[i] << std::endl;
	}
	os << std::endl;
	os << screen_size << std::endl;
	os << ai_level << std::endl;
	os << int(allow_quakers ? 1 : 0) << std::endl;
	os << maps_controller_data << std::endl;
	os << achivs_controller_data << std::endl;
};

}; // end Engine
