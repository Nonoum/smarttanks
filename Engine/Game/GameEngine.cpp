#include "GameEngine.h"
#include "../ResourceInitializer.h"
#include "../EngineConfig.h"
#include "../ProfileData.h"

#ifdef _DEBUG
#include <sstream>
#endif

namespace Engine {

namespace Game {

GameEngine :: GameEngine()
		: EngineCommon(true), current_state(getObjects()) {

	ResourceInitializer init(ResourceInitializer::RES_FOR_GAME_ONLY);
	paused = false;
	maps_controller.set( ProfileData::get().mapsControllerData() );
	if(maps_controller.getTotalScore()) {
		// protection from case when only maps_controller data is broken
		// if so, don't load achivs to have it's initial state
		achivs_controller.set( ProfileData::get().achivsControllerData() );
		achivs_controller.setScore( maps_controller.getTotalScore() );
	}
};

GameEngine :: ~GameEngine() {
	ProfileData::get().mapsControllerData() = maps_controller.get();
	ProfileData::get().achivsControllerData() = achivs_controller.get();
};

GameEngine& GameEngine :: getGameEngine() {
	return *this;
};

// state params

GameEngine::StateParams :: StateParams() : collection_ptr(&collection) {
	reset();
};

GameEngine::StateParams :: StateParams(CollectionType& c) : collection_ptr(&c) {
	reset();
};

void GameEngine::StateParams :: reset() {
	n_enemies = 0;
	special_drawing_offset.setPoint(0, 0);
	scorer.handleEvent(ScoreController::S_ENABLE, 1);
	scorer.handleEvent(ScoreController::S_RESET, 0);
};

GameEngine::StateParams :: ~StateParams() {
	if(&collection == collection_ptr) {
		GameEngine::clearObjects(collection);
	}
};

void GameEngine::StateParams :: cloneTo(StateParams& other, GameEngine& lock_object, Helpers::Request<void>& request) {
	Helpers::AutoLocker auto_locker(lock_object, request);
	if(! auto_locker) {
		return;
	}
	clearObjects(*other.collection_ptr); // cleaning destination first
	typedef std::pair<ObjectInterface*, ObjectInterface*> Pair;
	typedef AutoPoolUsing::List<Pair> TempListType;
	TempListType tmp;
	// making clones of objects
	for(CollectionType::LockIterator it(*collection_ptr); ! it.isDone(); ++it) {
		ObjectInterface* cloned = (*it)->autoPoolObjectInterface(true);
		tmp.addFirst( std::make_pair(*it, cloned) );
		other.collection_ptr->addFirst( cloned );
	}
	size_t n_undone = tmp.getTotalCount();
	size_t i_attempt = 0;
	const size_t enough_attempts_cnt = 10; // 10 is magic that should always be enough
	// correcting infrastructure of cloned list (internal connections between objects, e.t.c)
	while(n_undone > 0 && i_attempt < enough_attempts_cnt) {
		for(TempListType::LockIterator it(tmp); ! it.isDone(); ) {
			if((*it).second->confirmBackupFrom((*it).first, i_attempt)) {
				it.remove();
				--n_undone;
			} else {
				++it;
			}
		}
		++i_attempt;
	}
	assert( n_undone == 0 && "Infrastructure wasn't copied properly" );

	other.special_drawing_offset = special_drawing_offset;
	other.n_enemies = n_enemies;
	other.scorer = scorer;
};

// backup methods

void GameEngine :: createBackup() {
	current_state.cloneTo(saved_state, *this, creating_backup_request);
};

void GameEngine :: applyBackup() {
	saved_state.cloneTo(current_state, *this, applying_backup_request);
};

AI::AIProcessor& GameEngine :: getAIProcessor() {
	return ai_processor;
};

MapsController& GameEngine :: getMapsController() {
	return maps_controller;
};

AchievementController& GameEngine :: getAchivsController() {
	return achivs_controller;
};

// user input methods

bool GameEngine :: handleInput(Controls key, bool key_down, Geometry::Point pos/*unused now*/) {
	PlayerControlParams params(key, key_down);
	return PlayerController::handleInput( params );
};

bool GameEngine :: internalHandler(PlayerControlParams& params) {
	switch (params.key) {
	case KEY_PAUSE:
		paused ^= params.key_down;
		break;
	case KEY_RESTART:
		applyBackup();
		render(true);
		break;
	default:
		return false;
	};
	return true;
};

// geometry

void GameEngine :: correctDrawingOffset() {
	const Geometry::Size screen_size(getScreen().width(), getScreen().height());
	const Geometry::Size game_on_screen_size = screen_size.getMinimums(getGameSize());
	if(screen_size.canFit(getGameSize())) {
		return;
	}
	const Geometry::Point cur_offs = convertCoordinates(false, Geometry::Point(0, 0));
	// taking player's placement
	PlayerControlParams params(KEY_GET_INFO, true);
	PlayerController::notify(params);
	// calculating offsets
	if( params.health ) { // only if we have alive player
		// calculating safe zone size
		const ssize w = screen_size.width() * EngineConfig::get().safeZonePercent() / 100;
		const ssize h = screen_size.height() * EngineConfig::get().safeZonePercent() / 100;
		// applying safe zone
		const Geometry::Size sz(w*2 + params.placement.width(), h*2 + params.placement.height());
		const Geometry::Rect safe_bounds = params.placement.getResizedRect(sz, 0, 0);
		// check placement
		const Geometry::Rect visible_rect(cur_offs, game_on_screen_size);
		if(! safe_bounds.insideOf(visible_rect)) {
			const ssize diffs[] = {
				safe_bounds.x() - visible_rect.x(),
				safe_bounds.y() - visible_rect.y(),
				visible_rect.getXEnd() - safe_bounds.getXEnd(),
				visible_rect.getYEnd() - safe_bounds.getYEnd()
			};
			ssize x_compensation = 0, y_compensation = 0;
			if(diffs[0] < 0) {
				x_compensation += Helpers::max2(diffs[0], -cur_offs.x());
			}
			if(diffs[1] < 0) {
				y_compensation += Helpers::max2(diffs[1], -cur_offs.y());
			}
			if(diffs[2] < 0) {
				x_compensation -= Helpers::max2(diffs[2], cur_offs.x() + game_on_screen_size.width() - getGameSize().width());
			}
			if(diffs[3] < 0) {
				y_compensation -= Helpers::max2(diffs[3], cur_offs.y() + game_on_screen_size.height() - getGameSize().height());
			}
			adjustDrawingOffset( Geometry::Point(x_compensation, y_compensation) );
		}
	}
};

Geometry::Point& GameEngine :: specialDrawingOffset() {
	return current_state.special_drawing_offset;
};

// common

bool GameEngine :: render(bool forced) {
	Helpers::AutoLocker auto_locker(*this, render_request);
	if(! auto_locker) {
		render_request.param = forced;
		return false;
	}
	correctDrawingOffset(); // correcting drawing offset before render of screen

	adjustDrawingOffset(current_state.special_drawing_offset); // adjusted special offset

	DrawingLevel::DrawingLevels levels;
	const Geometry::Point offset = convertCoordinates(false, Geometry::Point(0, 0));
	const Geometry::Rect related_output(offset.x(),
										offset.y(),
										getScreen().width(),
										getScreen().height());
	// collecting objects that will be visible on screen for current settings
	typedef AutoPoolUsing::ListArray<ObjectInterface*> TempListType;
	TempListType list_to_draw;
	for(CollectionType::LockIterator it(getObjects()); ! it.isDone(); ++it) {
		if( (*it)->getRect().crossing(related_output) ) {
			list_to_draw.addFirst(*it); // adding in temporary reduced collection
			levels.include((*it)->getLevel()); // marking level as using
		}
	}
	// drawing objects by increasing level
	for(levels.beginIterate(); ! levels.isDone(); ++levels) {
		for(TempListType::Iterator it(list_to_draw); ! it.isDone(); ++it) {
			if(levels.match( (*it)->getLevel() )) { // object is on current level
				(*it)->draw( getGameEngine() );
			}
		}
	}
	adjustDrawingOffset(-current_state.special_drawing_offset); // recovering offset by substracting special offset
#ifdef _DEBUG
	{ // drawing debug-info
		static Graphic32::TextPrinter printer;
		if(! printer.getCHeight()) {
			printer.create(EngineConfig::get().sFont());
		}
		std::ostringstream oss;
		size_t nrows = 0;
		oss << "Total objects count: " << getObjects().getTotalCount(); ++nrows;
		printer.print(getScreen(), 0, printer.getCHeight() * (nrows-1), ~0, oss.str().c_str(), NULL);
	}
#endif
	// sending callback for screen
	EngineCallbackParams cback_params(&getScreen(), getGlobalScreenPos());
	sendCallback( cback_params );
	// refreshing panel if needed and sending callback
	if(renderPanel(forced)) {
		EngineCallbackParams params(&getPanel(), getGlobalPanelPos());
		sendCallback( params );
	}
	return true;
};

bool GameEngine :: live() {
	Helpers::AutoLocker auto_locker(*this, live_request);
	if(! auto_locker) {
		return false;
	}
	if(paused) {
		return false;
	}
	// preparing AI data
	ai_processor.setUpData(*this);
	// processing environment
	for(CollectionType::LockIterator it(getObjects()); ! it.isDone(); ) {
		if( (*it)->live( getGameEngine() ) ) { // object need to be removed
			(*it)->removeFromPool(); // destructing object
			it.remove(); // removing object from collection (iterator moves to next)
		} else {
			++it;
		}
	}
	if(! PlayerController::connectionsCount()) {
		// lost
		current_state.scorer.handleEvent(ScoreController::S_ENABLE, 1);
		current_state.scorer.handleEvent(ScoreController::S_RESET, 0);
		current_state.scorer.handleEvent(ScoreController::S_ENABLE, 0);
	} else if(! current_state.n_enemies) {
		// won
		PlayerControlParams params(KEY_GET_INFO, true);
		PlayerController::notify(params);
		current_state.scorer.handleEvent(ScoreController::S_APPEND, params.health);
		current_state.scorer.handleEvent(ScoreController::S_ENABLE, 0);
		if(maps_controller.submitMapScore(current_state.scorer)) {
			achivs_controller.setScore(maps_controller.getTotalScore());
		}
	}
	panel_controller.onTimer();
	// rendering scene
	render(false);
	return true;
};

void GameEngine :: registerEnemy() {
	++current_state.n_enemies;
};

void GameEngine :: unregisterEnemy() {
	--current_state.n_enemies;
};

void GameEngine :: unpause() {
	paused = false;
};

ScoreController& GameEngine :: getScorer() {
	return current_state.scorer;
};

bool GameEngine :: renderPanel(bool forced) {
	InfoPanelController::State state;
	// taking info
	PlayerControlParams params(KEY_GET_INFO, true);
	PlayerController::notify(params);
	// writing info to state
	state.health = params.health;
	state.armor = params.armor;
	state.speeder = params.speeder;
	state.n_enemies = current_state.n_enemies;
	state.score = current_state.scorer;
	return panel_controller.draw(getPanel(), state, forced);
};

void GameEngine :: onMapLoaded() {
	EngineCommon::onMapLoaded();
	current_state.reset();
	createBackup();
};

// requests handling

void GameEngine :: onFullUnlock() {
	EngineCommon::onFullUnlock();
	// live request is more prioritized
	if(live_request.isSet()) {
		live();
	}
	if(render_request.isSet()) {
		render(render_request.param);
	}
	if(creating_backup_request.isSet()) {
		createBackup();
	}
	if(applying_backup_request.isSet()) {
		applyBackup();
	}
};

}; // end Game

}; // end Engine
