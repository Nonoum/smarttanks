#include "ScoreController.h"
#include "../ProfileData.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Game {

ScoreController :: ScoreController() {
	score = 0;
	scoring_on = true;
};

void ScoreController :: handleEvent(ScoreEvent ev, size_t param) {
	if(ev == S_ENABLE) {
		scoring_on = param ? true : false;
		return;
	}
	if(! scoring_on) {
		return;
	}
	const size_t mult = getMultipler();
	switch (ev) {
	case S_ARMOR:
		score += 3 * param * mult;
		break;
	case S_SPEEDER:
		score += param * mult / (EngineConfig::get().fps() * 7);
		break;
	case S_DIRECT_HIT:
		score += 4 * mult;
		break;
	case S_INDIRECT_HIT:
		score += (15 + param) * mult;
		break;
	case S_APPEND:
		score += param * mult;
		break;
	case S_RESET:
		score = 0;
		break;
	default:
		break;
	};
};

size_t ScoreController :: getMultipler() {
	switch (ProfileData::get().aiLevel()) {
	case AI::AIProcessor::LEVEL_EASY: return 1;
	case AI::AIProcessor::LEVEL_NORMAL: return 3;
	case AI::AIProcessor::LEVEL_HARD: return 7;
	case AI::AIProcessor::LEVEL_VERY_HARD: return 11;
	};
	return 0;
};

}; // end Game

}; // end Engine
