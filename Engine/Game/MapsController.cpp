#include "MapsController.h"
#include "../ProfileData.h"
#include <numeric>
#include <sstream>
#include <assert.h>

namespace Engine {

namespace Game {

static const char* CRYPT_PHRASE = "scores";

MapsController :: MapsController() {
	icurrent = 0;
	total_score = 0;
	scores.assign(ProfileData::get().maps().size(), 0);
	updateDisplayNames();
};

void MapsController :: setCurrentMapIndex(size_t index) {
	assert(index < scores.size());
	icurrent = index;
};

bool MapsController :: submitMapScore(size_t score) {
	if(scores[icurrent] >= score) {
		return false;
	}
	scores[icurrent] = score;
	total_score = std::accumulate(scores.begin(), scores.end(), 0);
	updateDisplayNames();
	return true;
};

size_t MapsController :: getTotalScore() const {
	return total_score;
};

const std::string& MapsController :: getMapDisplayName(size_t map_index) const {
	assert(map_index < display_names.size());
	return display_names[map_index];
};

void MapsController :: updateDisplayNames() {
	display_names.clear();
	std::string tmp_str;
	std::ostringstream oss;
	const std::vector<std::string>& fnames = ProfileData::get().maps();
	for(size_t i = 0; i < fnames.size(); ++i) {
		tmp_str = fnames[i];
		const size_t dot_pos = tmp_str.rfind('.');
		if(dot_pos != -1) {
			tmp_str.erase(dot_pos);
		}
		if(! scores[i]) {
			display_names.push_back(tmp_str);
		} else {
			oss << tmp_str << " (" << scores[i] << " points)";
			display_names.push_back(oss.str());
			oss.str(""); // clear the buffer
		}
	}
};

std::string MapsController :: get() const {
	const size_t sz = scores.size() + 2;
	Helpers::CustomAutoPtr<uint32> raw(new uint32[sz]);
	raw.get()[0] = scores.size();
	raw.get()[1] = 0; // reserved
	for(size_t i = 0; i < scores.size(); ++i) {
		raw.get()[i+2] = scores[i];
	}
	return Helpers::SimpleEncrypt(raw.get(), sz*sizeof(uint32), CRYPT_PHRASE);
};

bool MapsController :: set(const std::string& str) {
	size_t length = 0;
	Helpers::CustomAutoPtr<uint32> raw( (uint32*)Helpers::SimpleDecrypt(str, length, CRYPT_PHRASE) );
	if(raw.get() == NULL) {
		return false;
	}
	if(raw.get()[0] > scores.size()) {
		return false;
	}
	const size_t limit = Helpers::min2(size_t(raw.get()[0]), size_t(scores.size()));
	std::fill(scores.begin(), scores.end(), 0);
	std::copy(raw.get()+2, raw.get()+2+limit, scores.begin());
	total_score = std::accumulate(scores.begin(), scores.end(), 0);
	updateDisplayNames();
	return true;
};

}; // end Game

}; // end Engine
