#pragma once
#include "../imports.h"
#include <vector>

namespace Engine {

namespace Game {

class MapsController {
	size_t icurrent;
	typedef std::vector<size_t> ScoresType;
	ScoresType scores;
	size_t total_score;
	typedef std::vector<std::string> NamesType;
	NamesType display_names;
public:
	MapsController();
	void setCurrentMapIndex(size_t index);
	bool submitMapScore(size_t score);
	size_t getTotalScore() const;
	const std::string& getMapDisplayName(size_t map_index) const;
	//
	std::string get() const;
	bool set(const std::string& str);
private:
	void updateDisplayNames();
};

}; // end Game

}; // end Engine
