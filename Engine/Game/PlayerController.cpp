#include "PlayerController.h"

namespace Engine {

namespace Game {

PlayerControlParams :: PlayerControlParams(Controls _key, bool _key_down)
		: key(_key), key_down(_key_down) {

	health = 0;
	armor = 0;
	speeder = 0;
	placement = Geometry::Rect(0, 0, 0, 0);
};

//------------------------------------------------------

void PlayerController :: notificationHandler(PlayerControlParams&) {
};

bool PlayerController :: handleInput(PlayerControlParams params) {
	bool result = internalHandler(params);
	notify(params);
	return result;
};

}; // end Game

}; // end Engine
