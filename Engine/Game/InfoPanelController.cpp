#include "InfoPanelController.h"
#include "../AnimationController.h"
#include "../Types/ArmorPick.h"

namespace Engine {

namespace Game {

/*
	Returns width of drawed area (in pixels).
	(numbers) is image with line of numbers (0-9) with equal width
*/
static size_t DrawNumber(Img32& dst, Img32& numbers, ssize x, ssize y, size_t value) {
	if((! dst) || (! numbers)) {
		return 0;
	}
	// parsing value
	char parts[24];
	size_t cnt = 0;
	do {
		parts[cnt] = value % 10;
		value /= 10;
		++cnt;
	} while(value);
	// drawing parts
	const ssize char_width = numbers.width() / 10;
	for(size_t i = 0; i < cnt; ++i) {
		const ssize offset = parts[cnt-1-i] * char_width;
		dst.drawRect(x, y, char_width, numbers.height(), numbers, offset, 0, Graphic32::D_ALPHA);
		x += char_width;
	}
	return cnt * char_width;
};

/*
	Draws (src) to (dst) on horizontal area [x:xend) with (shift) offset from start.
	If image doesn't fit - it's being splitted in two parts and 2nd part is being drawed at beginning (circular line).
	Returns (shift) value corrected by module of [x:xend) distance.
*/
static ssize DrawImageCircular(Img32& dst, ssize x, ssize y, ssize xend, const Img32& src, ssize shift) {
	const ssize dist = xend - x;
	ssize length = src.width();
	if(shift >= dist) {
		shift -= dist;
	}
	if(length > xend - shift - x) {
		length = xend - shift - x;
	}
	dst.drawRect(x + shift, y, length, src.height(), src, 0, 0, Graphic32::D_ALPHA);
	if(length != src.width()) { // separated
		dst.drawRect(x, y, src.width() - length, src.height(), src, length, 0, Graphic32::D_ALPHA);
	}
	return shift;
};

// state

InfoPanelController::State :: State() {
	health = 0;
	armor = 0;
	speeder = 0;
	n_enemies = 0;
	score = 0;
};

bool InfoPanelController::State :: operator ==(const State& other) const {
	return health == other.health && armor == other.armor && n_enemies == other.n_enemies && speeder == other.speeder && score == other.score;
};

// info panel controller

InfoPanelController :: InfoPanelController() {
	done_once = false;
	message_shift = 0;
};

bool InfoPanelController :: init(const char* cimgs_resources_filename) {
	Img32* images = NULL;
	uint16 count = 0;
	if(! ImageBatchLoader::loadFromCimgs( cimgs_resources_filename, images, count )) {
		return false;
	}
	const bool result = (count >= 8);
	if(result) {
		images[0].swap(background_img);
		images[1].swap(life_img);
		images[2].swap(armor_img);
		images[3].swap(numbers_img);
		images[4].swap(game_over_img);
		images[5].swap(you_won_img);
		images[6].swap(speeder_img);
		images[7].swap(score_img);
	}
	delete [] images; // cleaning memory
	return result;
};

bool InfoPanelController :: draw(Img32& panel, const State& state, bool forced) {
	if((done_once) && (! forced) && (state == last_state)) {
		return false;
	}
	// TODO: possible to do local optimization with partial redrawing in this method
	panel.drawRect(0, 0, panel.width(), panel.height(), background_img);
	const ssize xmargin = 20;
	const ssize space = 26;
	const ssize ispace = 6;
	ssize x = xmargin;
	ssize y = 3;
	// health part
	panel.drawRect(x, y, life_img.width(), life_img.height(), life_img, 0, 0, Graphic32::D_ALPHA);
	x += life_img.width() + ispace;
	x += DrawNumber(panel, numbers_img, x, y, state.health) + space;
	// armor part
	panel.drawRect(x, y, armor_img.width(), armor_img.height(), armor_img, 0, 0, Graphic32::D_ALPHA);
	x += armor_img.width() + ispace;
	x += DrawNumber(panel, numbers_img, x, y, state.armor) + space;
	// speeder part
	if(state.speeder) {
		panel.drawRect(x, y, speeder_img.width(), speeder_img.height(), speeder_img, 0, 0, Graphic32::D_ALPHA);
		x += speeder_img.width() + ispace;
		x += DrawNumber(panel, numbers_img, x, y, state.speeder) + space;
	}
	// score part
	panel.drawRect(x, y, score_img.width(), score_img.height(), score_img, 0, 0, Graphic32::D_ALPHA);
	x += score_img.width() + ispace;
	x += DrawNumber(panel, numbers_img, x, y, state.score) + space;
	// msg part
	if(! state.health) {
		message_shift = DrawImageCircular(panel, x, y, panel.width() - xmargin, game_over_img, message_shift);
	} else if(! state.n_enemies) {
		message_shift = DrawImageCircular(panel, x, y, panel.width() - xmargin, you_won_img, message_shift);
	}
	last_state = state;
	return true;
};

void InfoPanelController :: onTimer() {
	if((! last_state.health) || (! last_state.n_enemies)) {
		++message_shift;
	} else {
		message_shift = 0;
	}
};

Img32 InfoPanelController :: background_img;
Img32 InfoPanelController :: life_img;
Img32 InfoPanelController :: armor_img;
Img32 InfoPanelController :: numbers_img;
Img32 InfoPanelController :: game_over_img;
Img32 InfoPanelController :: you_won_img;
Img32 InfoPanelController :: speeder_img;
Img32 InfoPanelController :: score_img;

}; // end Game

}; // end Engine
