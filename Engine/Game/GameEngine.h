#pragma once
#include "../EngineCommon.h"
#include "PlayerController.h"
#include "InfoPanelController.h"
#include "../AI/AIProcessor.h"
#include "ScoreController.h"
#include "MapsController.h"
#include "AchievementController.h"

namespace Engine {

namespace Game {

/*
	Completed game engine implementation.
*/
class GameEngine : public EngineCommon,
					public PlayerController {
	Helpers::Request<bool> render_request;
	Helpers::Request<void> live_request;
	Helpers::Request<void> creating_backup_request;
	Helpers::Request<void> applying_backup_request;

	struct StateParams : private NonCopyable {
		CollectionType* const collection_ptr;
		Geometry::Point special_drawing_offset; // offset for quakers, this is temporarily adjusted on drawing
		size_t n_enemies;
		ScoreController scorer;

		StateParams(); // for using personal collection
		StateParams(CollectionType& c); // for pointing to other collection
		~StateParams();
		void reset();
		void cloneTo(StateParams& other, GameEngine& lock_object, Helpers::Request<void>& request);
	private:
		CollectionType collection;
	};

	StateParams current_state, saved_state;

	InfoPanelController panel_controller;

	AI::AIProcessor ai_processor;

	MapsController maps_controller;

	AchievementController achivs_controller;

	bool paused;
public:
	GameEngine();
	~GameEngine();

	GameEngine& getGameEngine();
	bool render(bool forced);
	bool live();
	bool handleInput(Controls key, bool key_down, Geometry::Point pos);
	void createBackup(); // cleaning 'backup_objects' and cloning from EngineCommon::objects
	void applyBackup(); // cloning 'backup_objects' into EngineCommon::objects
	Geometry::Point& specialDrawingOffset();
	AI::AIProcessor& getAIProcessor();
	MapsController& getMapsController();
	AchievementController& getAchivsController();
	void registerEnemy();
	void unregisterEnemy();
	void unpause();
	ScoreController& getScorer();
protected:
	void onFullUnlock(); // overloading
	bool internalHandler(PlayerControlParams& params); // overloading
	void onMapLoaded();
private:
	bool renderPanel(bool forced);
	void correctDrawingOffset(); // corrects drawing offset to display player in allowed part of screen
};

}; // end Game

}; // end Engine
