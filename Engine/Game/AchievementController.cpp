#include "AchievementController.h"
#include <algorithm>
#include <sstream>
#include <assert.h>

namespace Engine {

namespace Game {

static const char* CRYPT_PHRASE = "achivs";

AchievementController :: AchievementController() {
	score = 0;
	balance = 0;

	std::fill(current_index, current_index + B_TYPES_COUNT, 0);
	std::fill(current_summ, current_summ + B_TYPES_COUNT, 0);

	const Achievement healths[] = {{10, 800}, {20, 1600}, {30, 2350}, {50, 4100}, {89, 8150}, {100, 12000}, {100, 18000}};
	bonuses[B_HEALTH].assign(healths, healths+7);

	const Achievement speeds[] = {{1, 9000}};
	bonuses[B_SPEED].assign(speeds, speeds+1);

	const Achievement rel_speeds[] = {{1, 1000}, {1, 1500}, {1, 2000}};
	bonuses[B_RELOAD_SPEED].assign(rel_speeds, rel_speeds+3);

	const Achievement bul_speeds[] = {{1, 2500}, {1, 2750}, {1, 3000}};
	bonuses[B_BULLET_SPEED].assign(bul_speeds, bul_speeds+3);

	const Achievement bul_powers[] = {{5, 900}, {7, 1500}, {10, 1700}, {15, 1900}, {22, 6000}};
	bonuses[B_BULLET_POWER].assign(bul_powers, bul_powers+5);

	fillTexts();
};

size_t AchievementController :: getBalance() const {
	return balance;
};

void AchievementController :: setScore(size_t _score) {
	score = _score;
	calc();
};

size_t AchievementController :: getBonus(BonusType b) const {
	assert(b < B_TYPES_COUNT);
	return current_summ[b];
};

size_t AchievementController :: getMaxBonus(BonusType b) const {
	assert(b < B_TYPES_COUNT);
	size_t sum = 0;
	for(size_t j = 0; j < bonuses[b].size(); ++j) {
		sum += bonuses[b][j].value;
	}
	return sum;
};

bool AchievementController :: processUpgrade(BonusType b, bool add) {
	assert(b < B_TYPES_COUNT);
	if(add) {
		if(bonuses[b].size() == current_index[b]) {
			return false;
		}
		Achievement& ach = bonuses[b][current_index[b]];
		if(ach.cost <= balance) {
			++(current_index[b]);
			calc();
			return true;
		}
	} else {
		if(! current_index[b]) {
			return false;
		}
		--(current_index[b]);
		calc();
		return true;
	}
	return false;
};

void AchievementController :: calc() {
	balance = score;
	for(size_t i = 0; i < B_TYPES_COUNT; ++i) {
		current_summ[i] = 0;
		for(size_t j = 0; j < current_index[i]; ++j) {
			current_summ[i] += bonuses[i][j].value;
			balance -= bonuses[i][j].cost;
		}
	}
};

void AchievementController :: fillTexts() {
	std::ostringstream oss;
	for(size_t i = 0; i < B_TYPES_COUNT; ++i) {
		for(AchivsType::iterator it = bonuses[i].begin(); it != bonuses[i].end(); ++it) {
			oss << "+" << it->value << " (" << it->cost << " p)";
			it->text_buy = oss.str();
			oss.str(""); // clear the buffer
			oss << "-" << it->value << " (" << it->cost << " p)";
			it->text_sell = oss.str();
			oss.str(""); // clear the buffer
		}
	}
};

const std::string& AchievementController :: getCurrentText(BonusType b, bool for_adding) const {
	static const std::string empty;
	if(for_adding) {
		if(current_index[b] != bonuses[b].size()) {
			return bonuses[b][current_index[b]].text_buy;
		}
	} else {
		if(current_index[b] != 0) {
			return bonuses[b][current_index[b]-1].text_sell;
		}
	}
	return empty;
};

bool AchievementController :: isOptionPresent(BonusType b, bool add) const {
	assert(b < B_TYPES_COUNT);
	if(add) {
		return current_index[b] != bonuses[b].size();
	}
	return current_index[b] != 0;
};

bool AchievementController :: isUpgradeAvailable(BonusType b) const {
	if(bonuses[b].size() == current_index[b]) {
		return false;
	}
	return bonuses[b][current_index[b]].cost <= balance;
};

const char* AchievementController :: getAchievementName(BonusType b) {
	switch (b) {
	case B_SPEED: return "Speed";
	case B_HEALTH: return "Health";
	case B_RELOAD_SPEED: return "Reloading speed";
	case B_BULLET_SPEED: return "Bullet speed";
	case B_BULLET_POWER: return "Bullet power";
	default: break;
	};
	return "";
};

std::string AchievementController :: get() const {
	const size_t sz = B_TYPES_COUNT + 2;
	Helpers::CustomAutoPtr<uint32> raw(new uint32[sz]);
	raw.get()[0] = B_TYPES_COUNT;
	raw.get()[1] = 0; // reserved
	for(size_t i = 0; i < B_TYPES_COUNT; ++i) {
		raw.get()[i+2] = current_index[i];
	}
	return Helpers::SimpleEncrypt(raw.get(), sz*sizeof(uint32), CRYPT_PHRASE);
};

bool AchievementController :: set(const std::string& str) {
	size_t length = 0;
	Helpers::CustomAutoPtr<uint32> raw( (uint32*)Helpers::SimpleDecrypt(str, length, CRYPT_PHRASE) );
	if(raw.get() == NULL) {
		return false;
	}
	if(raw.get()[0] > B_TYPES_COUNT) {
		return false;
	}
	const size_t limit = Helpers::min2(size_t(raw.get()[0]), size_t(B_TYPES_COUNT));
	std::fill(current_index, current_index + B_TYPES_COUNT, 0);
	std::copy(raw.get()+2, raw.get()+2+limit, current_index);
	calc();
	return true;
};

}; // end Game

}; // end Engine
