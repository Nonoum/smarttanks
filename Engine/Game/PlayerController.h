#pragma once
#include "../imports.h"
#include "../Controls.h"

namespace Engine {

namespace Game {

struct PlayerControlParams {
	const Controls key;
	const bool key_down;
	size_t health;
	size_t armor;
	size_t speeder;
	Geometry::Rect placement;
	PlayerControlParams(Controls _key, bool _key_down);
};

typedef AutoPoolUsing::Connector<PlayerControlParams&> PlayerConnector;

class PlayerController : public PlayerConnector {
public:
	bool handleInput(PlayerControlParams params);
protected:
	void notificationHandler(PlayerControlParams&); // handler for notifications from opposite side
	virtual bool internalHandler(PlayerControlParams& params) = 0; // handler that will be called in begin of 'handleInput'
};

}; // end Game

}; // end Engine
