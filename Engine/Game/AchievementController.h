#pragma once
#include "../imports.h"
#include <iostream>

namespace Engine {

namespace Game {

class AchievementController {
public:
	enum BonusType {
		B_HEALTH,
		B_SPEED,
		B_RELOAD_SPEED,
		B_BULLET_SPEED,
		B_BULLET_POWER,
		B_TYPES_COUNT
	};
private:
	size_t score, balance;
	struct Achievement {
		size_t value;
		size_t cost;
		std::string text_buy;
		std::string text_sell;
	};
	typedef std::vector<Achievement> AchivsType;
	AchivsType bonuses[B_TYPES_COUNT];
	size_t current_index[B_TYPES_COUNT];
	size_t current_summ[B_TYPES_COUNT];
public:
	AchievementController();
	size_t getBalance() const;
	void setScore(size_t _score);
	size_t getBonus(BonusType b) const;
	size_t getMaxBonus(BonusType b) const;
	bool processUpgrade(BonusType b, bool add);
	const std::string& getCurrentText(BonusType b, bool for_adding) const;
	bool isOptionPresent(BonusType b, bool add) const;
	bool isUpgradeAvailable(BonusType b) const;
	static const char* getAchievementName(BonusType b);
	//
	std::string get() const;
	bool set(const std::string& str);
private:
	void calc();
	void fillTexts();
};

}; // end Game

}; // end Engine
