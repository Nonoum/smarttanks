#pragma once
#include "../imports.h"

namespace Engine {

namespace Game {

/*
	Provides:
	- initialization for resources, required for info panel;
	- drawing specified state to panel;
*/
class InfoPanelController {
public:
	struct State {
		size_t health;
		size_t armor;
		size_t speeder;
		size_t n_enemies;
		size_t score;
		State();
		bool operator ==(const State& other) const;
	};
private:
	bool done_once;
	State last_state;
	ssize message_shift;
	// statics
	static Img32 background_img;
	static Img32 life_img;
	static Img32 armor_img;
	static Img32 numbers_img;
	static Img32 game_over_img;
	static Img32 you_won_img;
	static Img32 speeder_img;
	static Img32 score_img;
public:
	InfoPanelController();
	static bool init(const char* cimgs_resources_filename);
	bool draw(Img32& panel, const State& state, bool forced);
	void onTimer();
};

}; // end Game

}; // end Engine
