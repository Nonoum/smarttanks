#pragma once
#include "../imports.h"

namespace Engine {

namespace Game {

class ScoreController {
	size_t score;
	bool scoring_on;
public:
	enum ScoreEvent {
		S_ARMOR,
		S_SPEEDER,
		S_DIRECT_HIT,
		S_INDIRECT_HIT,
		S_APPEND,
		S_RESET,
		S_ENABLE,
	};
	ScoreController();
	void handleEvent(ScoreEvent ev, size_t param);
	operator size_t() const { return score; };
private:
	static size_t getMultipler();
};

}; // end Game

}; // end Engine
