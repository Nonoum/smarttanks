#include "AnimationController.h"
#include <assert.h>

namespace Engine {

bool ImageBatchLoader :: loadFromCimgs(const char* filename, Img32*& dst, uint16& dst_length) {
	assert(filename != NULL && "A valid file name expected");
	if(dst) { // already loaded
		return true;
	}
	dst_length = 0;
	const uint16 real_count = Img32().readCimgBatch(filename, 0);
	if(! real_count) {
		return false; // can't read file or empty animation..
	}
	Helpers::CustomAutoPtr<Img32> imgsptr(new Img32[real_count]);
	if(imgsptr.get()->readCimgBatch(filename, real_count) == real_count) {
		dst_length = real_count;
		dst = imgsptr.release();
		return true; // ok
	}
	return false; // failed
};

}; // end Engine
