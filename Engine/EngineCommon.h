#pragma once
#include "imports.h"
#include "ObjectInterface.h"
#include "DisplayController.h"
#include "ObjectTransporter.h"
#include "Controls.h"
#include <string>

namespace Engine {

// enum convertor
Geometry::Side KeyToSide(Controls key);

/*
	Common engine part.
*/
// no secondary using after destructing
class EngineCommon : public DisplayController {
public:
	typedef AutoPoolUsing::List<ObjectInterface*> CollectionType;
private:
	CollectionType objects;
	Geometry::Size game_size; // size of game field
	Geometry::Point drawing_offset; // offset for rendering screen (local)
	//-----------------------------------
	struct MapHeader {
		uint32 signature;
		uint32 obj_count;
		uint32 gwidth;
		uint32 gheight;
		uint32 engine_version;
		uint32 reserved[27];
	}; // file header struct
	Helpers::Request<std::string> file_read_request;
	Helpers::Request<std::string> file_write_request;
protected:
	const ObjectTransporter object_transporter; // single serializer
public:
	EngineCommon(bool _with_panel);
	virtual ~EngineCommon();
	// accessors
	EngineCommon& getEngineCommon();
	CollectionType& getObjects();
	Geometry::Size getGameSize() const;
	// screen offset interface
	Geometry::Point convertCoordinates(bool object_to_screen, Geometry::Point coord) const;
	void adjustDrawingOffset(Geometry::Point adding);
	// serializers
	void readMap(std::string filename);
	void writeMap(std::string filename);
private:
	// if there is LockIterators created for (objects), then readMap/writeMap will work incorrectly.
	void readMap(std::istream& src);
	void writeMap(std::ostream& dst);
	void readMapHeader(std::istream& src, MapHeader& output_header);
	void writeMapHeader(std::ostream& dst) const;
protected:
	static void clearObjects(CollectionType& collection); // cleans the collection
	void setGameSize(Geometry::Size size);
	void onFullUnlock(); // overloading
	void resetDrawingOffset();
	virtual void onMapLoaded(); // being called after successful loading map
};

}; // end Engine
