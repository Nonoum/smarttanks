#include "DrawingLevel.h"

namespace Engine {

DrawingLevel :: DrawingLevel() : value(0) {
};

DrawingLevel :: DrawingLevel(unsigned char _value) : value(_value) {
};

DrawingLevel :: operator unsigned char() const {
	return value;
};

usize DrawingLevel :: write(std::ostream& dst) const {
	dst.write((char*)&value, sizeof(value));
	return sizeof(value);
};

usize DrawingLevel :: read(std::istream& src) {
	src.read((char*)&value, sizeof(value));
	return sizeof(value);
};

std::ostream& operator <<(std::ostream& dst, const DrawingLevel& dlevel) {
	return dst << unsigned(dlevel.value);
};

std::istream& operator >>(std::istream& src, DrawingLevel& dlevel) {
	unsigned int buf;
	src >> buf;
	dlevel.value = buf;
	if(buf > 255) { // too much
		src.setstate(std::ios::badbit);
	}
	return src;
};

// Nested

DrawingLevel::DrawingLevels :: DrawingLevels() : current(0), moved(false) {
};

void DrawingLevel::DrawingLevels :: include(const DrawingLevel& dlevel) {
	bits.set(dlevel);
};

void DrawingLevel::DrawingLevels :: operator ++() {
	if(! moved) { // first step
		moved = true;
		if(bits.test(current)) {
			return; // no need to move to next value
		}
	}
	do {
		if(current >= 255) { // done
			current = 256;
			break; // out
		}
		current = current + 1; // to next
	} while (! bits.test(current)); // while level not found
};

bool DrawingLevel::DrawingLevels :: match(const DrawingLevel& dlevel) const {
	return current == dlevel;
};

bool DrawingLevel::DrawingLevels :: isDone() const {
	return current > 255;
};

void DrawingLevel::DrawingLevels :: beginIterate() {
	if(moved) {
		current = 0;
		moved = false;
	}
	++(*this);
};

}; // end Engine
