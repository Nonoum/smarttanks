#include "ObjectInterface.h"
#include "EngineCommon.h"
#include <sstream>
#include <assert.h>

namespace Engine {

ObjectInterface :: ObjectInterface() {
};

ObjectInterface :: ObjectInterface(const Geometry::Rect& rect, bool _breakable, bool _obstacle, bool _alpha, const DrawingLevel& _level)
		: Geometry::Rect(rect), breakable(_breakable), obstacle(_obstacle), alpha_draw(_alpha), level(_level) {
};

ObjectInterface :: ~ObjectInterface() {
};

usize ObjectInterface :: writeLocal(std::ostream&) const {
	return 0;
};

usize ObjectInterface :: readLocal(std::istream&) {
	return 0;
};

std::string ObjectInterface :: getLocal() const {
	return std::string("");
};

bool ObjectInterface :: setLocal(std::string& src) {
	return true;
};

uint32 ObjectInterface :: answer(Questions q) const {
	return 0;
};

bool ObjectInterface :: hit(HitParams& h) {
	return false;
};

usize ObjectInterface :: write(std::ostream& dst) const {
	usize size = getRect().write(dst); // writing rect, then other base parameters
	char buf[8] = {0}; // reserving (5 additional bytes now)
	buf[0] = breakable;
	buf[1] = obstacle;
	buf[2] = alpha_draw;
	dst.write(buf, sizeof(buf));
	size += sizeof(buf);
	size += level.write(dst);
	return size + writeLocal(dst); // writing other object data (virtual part)
};

usize ObjectInterface :: read(std::istream& src) {
	usize size = getRect().read(src); // reading rect, then other base parameters
	char buf[8] = {0}; // reserving (5 additional bytes now)
	src.read(buf, sizeof(buf));
	breakable = buf[0] != 0;
	obstacle = buf[1] != 0;
	alpha_draw = buf[2] != 0;
	size += sizeof(buf);
	size += level.read(src);
	return size + readLocal(src); // reading other object data (virtual part)
};

std::string ObjectInterface :: get() const {
	std::ostringstream oss;
	oss << "breakable: " << breakable << std::endl;
	oss << "obstacle: " << obstacle << std::endl;
	oss << "alpha_draw: " << alpha_draw << std::endl;
	oss << "drawing_level: " << level << std::endl;
	return oss.str() + getLocal(); // virtual part
};

bool ObjectInterface :: set(std::string src) {
	std::istringstream iss(src);
	std::string param_name;
	iss >> param_name >> breakable;
	iss >> param_name >> obstacle;
	iss >> param_name >> alpha_draw;
	iss >> param_name >> level;
	if(! iss) {
		return false;
	}
	src.erase(static_cast<std::string::size_type>(iss.tellg()));
	return setLocal(src); // virtual part
};

void ObjectInterface :: drawCorpse(EngineCommon& env, uint32 color) const {
	Geometry::Point pos = env.convertCoordinates(true, getPoint()); // position on screen
	env.getScreen().drawRectContur(pos.x(), pos.y(), width()-1, height()-1, color);
};

bool ObjectInterface :: confirmBackupFrom(ObjectInterface* origin, size_t i_attempt) {
	assert( origin != NULL && "A valid object pointer expected" );
	return true;
};

}; // end Engine
