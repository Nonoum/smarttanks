#pragma once
#include "imports.h"

namespace Engine {

/*
	Parameters and base for engine drawing callback.
*/
struct EngineCallbackParams {
	const Img32* const image;
	const Geometry::Point position;
	EngineCallbackParams(const Img32* img, Geometry::Point pos);
};

struct EngineCallbackBase {
	virtual bool action(EngineCallbackParams&) = 0;
	virtual ~EngineCallbackBase() {};
};

/*
	Controller of engine screens.
	'render_request' have to be processed in inherited class, cause 'render' is pure virtual.
*/
class DisplayController : protected Helpers::ObjectLocker,
							public Helpers::CallbackStorage<EngineCallbackBase, EngineCallbackParams&> {
	Img32 screen; // engine screen
	Img32 panel; // game info panel
	Geometry::Size render_size;
	const bool has_panel;
	Geometry::Point screen_pos; // local position of engine screen in pixels on user area
	Geometry::Point panel_pos; // local position of game info panel -:-
	Geometry::Point output_window_position; // position of total screen on user window
	//-----------------------------------
	Helpers::Request<Geometry::Size> resizing_request;
protected:
	DisplayController(bool _with_panel);
	~DisplayController();
	inline Img32& getPanel() {
		return panel;
	};
	void onFullUnlock();
	Geometry::Point getGlobalScreenPos() const;
	Geometry::Point getGlobalPanelPos() const;
public:
	inline Img32& getScreen() {
		return screen;
	};
	bool setRenderSize(Geometry::Size size); // sets rendering size of total screen, if size is different from current
	void setOutputPosition(Geometry::Point pos);
	Geometry::Size getRenderSize() const;
	Geometry::Point getOutputPosition() const;
	virtual bool render(bool forced) = 0;
};

}; // end Engine
