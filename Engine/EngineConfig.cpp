#include "EngineConfig.h"
#include <assert.h>

namespace Engine {

const EngineConfig& EngineConfig :: get() {
	static EngineConfig config;
	return config;
};

EngineConfig :: EngineConfig()
		: large_font(11, 20, 120),
		small_font(7, 12, 120) {

	corpses_color = 0x0000FFFF;
	corpses_selected_color = 0x00FFFF00;
	text_color = 0x00FF0000;
	grid_color = 0x00404040;

	grid_step = 50;
	info_panel_height = 36;
	constructor_gui_height = 48;
	constructor_big_step = 24;

	min_window_size = Geometry::Size(320, 240);
	min_object_size = Geometry::Size(4, 4);
	max_map_size = Geometry::Size(1000000, 1000000);

	safe_zone_percent = 38;

	engine_version = 0x00000002;
	string_version = "2.0";

	tank_dying_period = 10;
	tank_speed = 3;
	bullet_speed = 5;

	frames_per_second = 30;

	checkConfig();
};

void EngineConfig :: checkConfig() {
	assert(max_map_size.canFit(min_window_size) && "Invalid config sizes");
};

}; // end Engine
