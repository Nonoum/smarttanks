#pragma once

// libraries
#include "K:/NnmLib/head.h"

#ifdef _DEBUG
#pragma comment(lib, "K:/NnmLib/debug.lib")
#else
#pragma comment(lib, "K:/NnmLib/release.lib")
#endif

using namespace LibTypes;
using Graphic32::Img32;

// code optimizer
#define DECLARE_TYPICAL_CALLBACK(cb_name, cb_base, cb_init, cb_param)	\
	class cb_name : public cb_base {									\
		cb_init client;													\
	public:																\
		cb_name(cb_init _client) : client(_client) {					\
		};																\
		bool action(cb_param params);									\
	};

#include "Strings.h"
