#pragma once
#include "imports.h"
#include <iostream>
#include <bitset>

namespace Engine {

class DrawingLevel {
	unsigned char value;
public:
	DrawingLevel();
	explicit DrawingLevel(unsigned char _value);
	operator unsigned char() const;
	usize write(std::ostream& dst) const; // returns count of bytes written
	usize read(std::istream& src); // returns count of bytes read
	friend std::ostream& operator <<(std::ostream& dst, const DrawingLevel& dlevel);
	friend std::istream& operator >>(std::istream& src, DrawingLevel& dlevel);
	// Nested --------------------------------------------------
	class DrawingLevels {
		std::bitset<256> bits;
		size_t current;
		bool moved;
	public:
		DrawingLevels();
		void include(const DrawingLevel& dlevel); // includes dlevel in set
		void operator ++(); // moves to the next value
		bool match(const DrawingLevel& dlevel) const; // checks if dlevel is the current level
		bool isDone() const; // checks if last level in set was passed
		void beginIterate(); // call before cycle
	};
};

}; // end Engine
