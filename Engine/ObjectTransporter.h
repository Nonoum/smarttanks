#pragma once
#include "ObjectInterface.h"
#include <map>
#include <vector>

namespace Engine {

/*
	Class collects object types by pointers of 'ObjectInterface' to object instances.
	Supplies read/write methods for transporting with files.
*/

class ObjectInterface;

class ObjectTransporter : private NonCopyable {
protected:
	typedef std::map<uint32, ObjectInterface*> CollectionType;
	CollectionType types; // map of conformity of types with id's

	void addType(uint32 id, ObjectInterface* object, const char* name);
public:
	ObjectTransporter();
	~ObjectTransporter();
	void bringAutoPoolCopies(std::vector<ObjectInterface*>& dst) const; // adds copies of objects to (dst) without cleaning prematurely
	usize write(std::ostream& dst, const ObjectInterface* object) const; // returns count of bytes written
	ObjectInterface* read(std::istream& src) const; // returns pointer to object created in auto pool chain
};

}; // end Engine
