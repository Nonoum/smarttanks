#pragma once
#include "imports.h"

namespace Engine {

/*
	Provides loading of animation from CIMGS file;
	moved out of template to .cpp to avoid excess code generation.
*/
struct ImageBatchLoader {
	static bool loadFromCimgs(const char* filename, Img32*& dst, uint16& dst_length);
};

/*
	Provides static animation for specified owner class,
	and interface for reproducing animation.
*/
template <class Owner> class AnimationController : private ImageBatchLoader {
	struct FramesHolder {
		Img32* frames;
		FramesHolder() : frames(NULL) {};
		~FramesHolder() { delete [] frames; frames = NULL; };
	};
	static uint16 length;
	static FramesHolder holder;
	uint16 icurrent;
public:
	AnimationController() : icurrent(0) {
	};
	static bool init(const char* cimgs_filename) {
		return loadFromCimgs(cimgs_filename, holder.frames, length);
	};
	static bool hasAnimation() {
		return holder.frames != NULL;
	};
	const Img32& getFrame() const {
		return holder.frames[icurrent];
	};
	bool live() { // returns true if animation is just done it's cycle
		if( ++icurrent >= length ) {
			icurrent = 0;
			return true;
		}
		return false;
	};
	static Geometry::Size getFramesSize() {
		if(! hasAnimation()) {
			return Geometry::Size(0, 0);
		}
		return Geometry::Size( holder.frames[0].width(), holder.frames[0].height() );
	};
};

template <class Owner> uint16 AnimationController<Owner> :: length = 0;
template <class Owner> typename AnimationController<Owner>::FramesHolder AnimationController<Owner> :: holder;

}; // end Engine
