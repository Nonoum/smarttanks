#pragma once
#include "imports.h"
#include "AI/AIProcessor.h"
#include <vector>
#include <fstream>

namespace Engine {

class ProfileData {
	std::vector<std::string> maps_list;
	Geometry::Size screen_size;
	AI::AIProcessor::AILevel ai_level;
	bool allow_quakers;
	std::string maps_controller_data;
	std::string achivs_controller_data;

	// statics
	static std::string maps_dir;
	static std::string file_name;

	ProfileData();
	~ProfileData();
	void reset();
public:
	static bool init(const char* fname, std::string& dir);
	static ProfileData& get();

	const std::vector<std::string>& maps() const;
	std::string mapPath(size_t map_index_in_list) const;
	Geometry::Size& screenSize() { return screen_size; };
	AI::AIProcessor::AILevel& aiLevel() { return ai_level; };
	bool& areQuakersAllowed() { return allow_quakers; };
	std::string& mapsControllerData() { return maps_controller_data; };
	std::string& achivsControllerData() { return achivs_controller_data; };

private:
	void read(std::istream& is);
	void write(std::ostream& os);
};

}; // end Engine
