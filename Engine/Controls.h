#pragma once

namespace Engine {

enum Controls {
	KEY_ITERATOR = 0,
	// constructor only
	KEY_MOUSE_LEFT = KEY_ITERATOR,
	KEY_MOUSE_RIGHT,
	KEY_MOUSE_MOVE,
	KEY_MOUSE_DOUBLE,
	KEY_DELETE,
	// common
	KEY_DOWN,
	KEY_UP,
	KEY_LEFT,
	KEY_RIGHT,
	KEY_PAUSE,
	KEY_CONTROL,
	KEY_SHIFT,
	KEY_ESC,
	KEY_RESTART,
	KEY_SHOOT,
	KEY_GET_INFO,
	//-----------//
	KEY_NKEYS // all keys must be lower than this value (as unsigned)
};

}; // end Engine
