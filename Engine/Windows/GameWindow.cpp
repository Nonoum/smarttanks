#include "GameWindow.h"
#include "../ResourceInitializer.h"
#include "../ProfileData.h"
#include "../EngineConfig.h"
#include "SimpleWindows.h"
#include "../AI/AIProcessor.h"
#include <sstream>

namespace Engine {

namespace Windows {

// UI window constants
static const ssize MARGIN = 10;
static const uint32 TEXT_COLOR = 0x00FFFF80;
static const uint32 DYNAMIC_TEXT_COLORS[] = {TEXT_COLOR, 0x00FF4080, 0x00FF8000};
static const uint32 STATIC_TEXT_COLORS[] = {TEXT_COLOR, TEXT_COLOR, TEXT_COLOR};
static const uint32 BUTTON_ALPHA_COLORS[] = {0xD0000000, 0x80000000, 0xFF000000};

static const uint32 AVAILABLE_UPGRADES_COLORS[] = {0x00007820, 0x00008830, 0x00008820};
static const uint32 UNAVAILABLE_UPGRADES_COLORS[] = {0x00002848, 0x00002848, 0x00002848};
static const uint32 DOWNGRADES_COLORS[] = {0x00500010, 0x00501000, 0x00600000};

// local helper class to work with resilutions list
class ResolutionLBController {
	static Geometry::Size sizes[];
	static size_t count;
public:
	static void fill(GUI::GUIListBox* listbox) {
		if(! listbox) {
			assert(false && "Got a NULL pointer instead of GUIListbox*");
			return;
		}
		for(size_t i = 0; i < count; ++i) {
			std::ostringstream oss;
			oss << sizes[i].width() << " x " << sizes[i].height();
			listbox->pushString( oss.str().c_str() );
		}
	};
	static Geometry::Size getSize(size_t index) {
		return index < count ? sizes[index] : Geometry::Size();
	};
};
Geometry::Size ResolutionLBController :: sizes[] = {Geometry::Size(640, 480),
													Geometry::Size(800, 600),
													Geometry::Size(960, 600),
													Geometry::Size(1024, 768),
													};
size_t ResolutionLBController :: count = sizeof(sizes)/sizeof(*sizes);


GameWindow :: GameWindow()
		: WindowInterface(StringFor("game_caption")),
		gui_wnd(Geometry::Point(), GUI::GUIObject::DRAW_IMAGE_SOLID) {

	ResourceInitializer init_gui_images(ResourceInitializer::RES_GAME_GUI_IMAGES);

	gui_wnd.setCachedImage("game-menu-background");

	gui_wnd.setCallback( new GUICallback(*this) );
	engine_wnd.setCallback( new EngineCallback(*this) );
	state = STATE_MAIN_MENU;
};

GameWindow :: ~GameWindow() {
};

void GameWindow :: timerProc(size_t milliseconds) {
	if(state == STATE_GAME) {
		engine_wnd.live();
	}
};

bool GameWindow :: handleInput(Controls key, bool key_down, Geometry::Point pos) {
	if(state == STATE_GAME) {
		if(key == KEY_ESC && key_down) {
			engine_wnd.unpause();
			state = STATE_MAIN_MENU;
			setRenderSize(getRenderSize(), true);
			return true;
		} else {
			return engine_wnd.handleInput(key, key_down, pos);
		}
	} else if(state == STATE_SETTINGS_MENU && key == KEY_ESC) {
		changeSubMenu(STATE_MAIN_MENU);
		forcedRender();
		return true;
	}
	return false;
};

bool GameWindow :: handleInput(char c, bool key_down) {
	if(state != STATE_GAME) {
		return gui_wnd.handleInput(c, key_down);
	}
	return false;
};

bool GameWindow :: handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) {
	if(state != STATE_GAME) {
		bool result = gui_wnd.handleInput(key, key_down, pos);

		if(wnd_need_resize.isSet()) {
			wnd_need_resize.toggleOff();
			processChosenResolution(wnd_need_resize.param);
		}
		if(wnd_need_submenu_change.isSet()) {
			wnd_need_submenu_change.toggleOff();
			changeSubMenu(wnd_need_submenu_change.param);
		}
		return result;
	}
	return false;
};

bool GameWindow :: forcedRender() {
	if(state == STATE_GAME) {
		return engine_wnd.render(true);
	}
	return gui_wnd.render(true);
};

void GameWindow :: onClose() {
	setRenderSize(Geometry::Size()); // clean window
};

bool GameWindow :: resizeHandler(Geometry::Size size) {
	if(state != STATE_GAME) {
		engine_wnd.setRenderSize(size);
		return resizeOnMenuMode(size);
	} else {
		gui_wnd.setRenderSize(size);
		return engine_wnd.setRenderSize(size);
	}
	return false;
};

bool GameWindow :: resizeOnMenuMode(Geometry::Size size) {
	gui_wnd.clearObjects(false); // cleaning previous objects
	bool result = gui_wnd.setRenderSize(size);
	if(! size) {
		return result;
	}
	if(state == STATE_MAIN_MENU) {
		initMainMenuItems(size);
	} else if(state == STATE_SETTINGS_MENU) {
		initSettingsMenuItems(size);
	}
	// common items
	// exit button
	const Geometry::Rect r_exit = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN, 44, 72, Geometry::ALIGN_END, Geometry::ALIGN_BEGIN);
	GUI::GUIButton* exit_btn = new GUI::GUIButton(r_exit, GUI::GUIObject::DRAW_IMAGE_ALPHA, BTN_EXIT, "game-exit-button");
	gui_wnd.registerObject(exit_btn);
	GUI::SetColors(exit_btn, BUTTON_ALPHA_COLORS);
	exit_btn->setCallback( new ButtonCallback(*this) );
	return result;
};

void GameWindow :: initMainMenuItems(Geometry::Size size) {
	const Graphic32::FontInfo& lfont = EngineConfig::get().lFont();
	// maps list
	ssize h = lfont.height;
	ssize w = 120;
	const Geometry::Rect r_maps = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
	GUI::GUITextZone* maps_text = new GUI::GUITextZone(r_maps, lfont, GUI::GUIObject::DRAW_NONE);
	gui_wnd.registerObject(maps_text);
	maps_text->setText("Start game:");
	GUI::SetTextColors(maps_text, STATIC_TEXT_COLORS);

	w = 328;
	h = size.height() - MARGIN * 2 - lfont.height;
	const Geometry::Rect r_maps_list = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN + lfont.height, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
	GUI::GUIListBox* listbox = new GUI::GUIListBox(r_maps_list, lfont, GUI::GUIObject::DRAW_IMAGE_ALPHA);
	gui_wnd.registerObject(listbox);

	listbox->setCachedImage("game-text-background");
	GUI::SetTextColors(listbox, DYNAMIC_TEXT_COLORS);

	const uint32 alpha_colors[] = {0x70000000, 0x70000000, 0x70000000};
	GUI::SetColors(listbox, alpha_colors);

	listbox->setCallback( new MapsListBoxCallback(*this) );

	const size_t maps_cnt = ProfileData::get().maps().size();
	for(size_t i = 0; i < maps_cnt; ++i) {
		listbox->pushString(engine_wnd.getMapsController().getMapDisplayName(i).c_str());
	}

	// settings button
	ssize xpos = size.width() - MARGIN - 72;
	ssize ypos = size.height() - MARGIN - 54;
	const Geometry::Rect r_settings(xpos, ypos, 0, 0);
	GUI::GUIButton* settings_btn = new GUI::GUIButton(r_settings, GUI::GUIObject::DRAW_IMAGE_ALPHA, BTN_SETTINGS, "game-settings-button");
	gui_wnd.registerObject(settings_btn);
	GUI::SetTextColors(settings_btn, DYNAMIC_TEXT_COLORS);
	GUI::SetColors(settings_btn, BUTTON_ALPHA_COLORS);
	settings_btn->setCallback( new ButtonCallback(*this) );

	// help button
	ypos -= 72;
	const Geometry::Rect r_help(xpos, ypos, 0, 0);
	GUI::GUIButton* help_btn = new GUI::GUIButton(r_help, GUI::GUIObject::DRAW_IMAGE_ALPHA, BTN_HELP, "game-help-button");
	gui_wnd.registerObject(help_btn);
	GUI::SetTextColors(help_btn, DYNAMIC_TEXT_COLORS);
	GUI::SetColors(help_btn, BUTTON_ALPHA_COLORS);
	help_btn->setCallback( new ButtonCallback(*this) );

	// about button
	ypos -= 72;
	const Geometry::Rect r_about(xpos, ypos, 0, 0);
	GUI::GUIButton* about_btn = new GUI::GUIButton(r_about, GUI::GUIObject::DRAW_IMAGE_ALPHA, BTN_ABOUT, "game-about-button");
	gui_wnd.registerObject(about_btn);
	GUI::SetTextColors(about_btn, DYNAMIC_TEXT_COLORS);
	GUI::SetColors(about_btn, BUTTON_ALPHA_COLORS);
	about_btn->setCallback( new ButtonCallback(*this) );
};

void GameWindow :: initSettingsMenuItems(Geometry::Size size) {
	const Graphic32::FontInfo& lfont = EngineConfig::get().lFont();
	// resolutions list
	ssize w = 116;
	ssize h = lfont.height;
	const Geometry::Rect r_resolution = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN, w, h, Geometry::ALIGN_END, Geometry::ALIGN_END);
	GUI::GUITextZone* resolutions_text = new GUI::GUITextZone(r_resolution, lfont, GUI::GUIObject::DRAW_NONE);
	gui_wnd.registerObject(resolutions_text);
	resolutions_text->setText("Resolution:");
	GUI::SetTextColors(resolutions_text, STATIC_TEXT_COLORS);

	h = lfont.height * 4 + 4;
	const Geometry::Rect r_listbox = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN + r_resolution.height(), w, h, Geometry::ALIGN_END, Geometry::ALIGN_END);
	GUI::GUIListBox* res_listbox = new GUI::GUIListBox(r_listbox, lfont, GUI::GUIObject::DRAW_IMAGE_SOLID);
	gui_wnd.registerObject(res_listbox);
	GUI::SetTextColors(res_listbox, DYNAMIC_TEXT_COLORS);
	ResolutionLBController::fill(res_listbox);
	res_listbox->setCallback( new ResolutionsListBoxCallback(*this) );

	// AI complexity levels
	w = 140;
	h = lfont.height;
	// caption
	const Geometry::Rect r_radios_caption = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
	GUI::GUITextZone* radios_text = new GUI::GUITextZone(r_radios_caption, lfont, GUI::GUIObject::DRAW_NONE);
	gui_wnd.registerObject(radios_text);
	radios_text->setText("Enemy skill:");
	GUI::SetTextColors(radios_text, STATIC_TEXT_COLORS);
	// radios
	h = 120;
	const Geometry::Rect r_radios = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN + r_radios_caption.height(), w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
	GUI::GUIRadioGroup* radios = new GUI::GUIRadioGroup(r_radios, lfont, AI::AIProcessor::LEVEL_COUNT, GUI::GUIObject::DRAW_NONE, BTN_RADIOS);
	gui_wnd.registerObject(radios);
	GUI::SetTextColors(radios, STATIC_TEXT_COLORS);
	GUI::SetColors(radios, DYNAMIC_TEXT_COLORS);
	radios->setContur(true, TEXT_COLOR);
	for(int i = AI::AIProcessor::LEVEL_ITERATOR; i < AI::AIProcessor::LEVEL_COUNT; ++i) {
		radios->setText(i, AI::AIProcessor::getLevelDescription( AI::AIProcessor::AILevel(i) ));
	}
	radios->setActiveItem( ProfileData::get().aiLevel() );
	radios->setCallback( new ButtonCallback(*this) );

	// quakers option
	h = (h-2) / AI::AIProcessor::LEVEL_COUNT;
	w = 248;
	const Geometry::Rect r_quakers = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN + 160, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
	GUI::GUICheckBox* quakers_btn = new GUI::GUICheckBox(r_quakers, lfont, GUI::GUIObject::DRAW_NONE, BTN_QUAKERS);
	gui_wnd.registerObject(quakers_btn);
	GUI::SetTextColors(quakers_btn, STATIC_TEXT_COLORS);
	GUI::SetColors(quakers_btn, DYNAMIC_TEXT_COLORS);
	quakers_btn->setText(" Quake on explosion");
	quakers_btn->setContur(true, TEXT_COLOR);
	quakers_btn->setChecked(ProfileData::get().areQuakersAllowed());
	quakers_btn->setCallback( new ButtonCallback(*this) );

	// achievements part
	std::ostringstream oss;
	Game::AchievementController& achivs = engine_wnd.getAchivsController();
	h = lfont.height;
	w = 340;
	const Geometry::Rect r_scores = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN + 210, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
	GUI::GUITextZone* scores_text = new GUI::GUITextZone(r_scores, lfont, GUI::GUIObject::DRAW_NONE);
	gui_wnd.registerObject(scores_text);
	oss << "Score: " << engine_wnd.getMapsController().getTotalScore() << "   Balance: " << achivs.getBalance();
	scores_text->setText(oss.str().c_str());
	GUI::SetTextColors(scores_text, STATIC_TEXT_COLORS);
	for(size_t i = 0; i < Game::AchievementController::B_TYPES_COUNT; ++i) {
		const Game::AchievementController::BonusType b_type = Game::AchievementController::BonusType(i);
		w = 172;
		const ssize y_offset = 244;
		const ssize local_height = 30;
		h = lfont.height;
		const Geometry::Rect r_name = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN + y_offset + i * local_height, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
		GUI::GUITextZone* name_text = new GUI::GUITextZone(r_name, lfont, GUI::GUIObject::DRAW_NONE, 0, NULL, Geometry::ALIGN_END);
		gui_wnd.registerObject(name_text);
		name_text->setText(achivs.getAchievementName(b_type));
		GUI::SetTextColors(name_text, STATIC_TEXT_COLORS);
		// progress bar
		ssize x = w + 10;
		w = 60;
		h = local_height - 4;
		const Geometry::Rect r_bar = gui_wnd.getRect().getPlacedRect(MARGIN + x, MARGIN + y_offset - 2 + i * local_height + 2, w, h-4, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
		GUI::GUIProgressBar* bar = new GUI::GUIProgressBar(r_bar, achivs.getMaxBonus(b_type), achivs.getBonus(b_type), 0xFF00A860, 0xFF002848);
		gui_wnd.registerObject(bar);
		x += w + 10;
		// + button
		const size_t button_id = BTN_ACHIEVEMENTS + i*2;
		w = 154;
		if(achivs.isOptionPresent(b_type, true)) {
			const Geometry::Rect r_add = gui_wnd.getRect().getPlacedRect(MARGIN + x, MARGIN + y_offset - 2 + i * local_height, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
			GUI::GUIButton* add_btn = new GUI::GUIButton(r_add, lfont, GUI::GUIObject::DRAW_ROUNDED_RECT_MASK, button_id);
			gui_wnd.registerObject(add_btn);
			GUI::SetTextColors(add_btn, STATIC_TEXT_COLORS);
			GUI::SetColors(add_btn, achivs.isUpgradeAvailable(b_type) ? AVAILABLE_UPGRADES_COLORS : UNAVAILABLE_UPGRADES_COLORS);
			add_btn->setText( achivs.getCurrentText(b_type, true).c_str() );
			add_btn->setCallback( new ButtonCallback(*this) );
			if(! achivs.isUpgradeAvailable(b_type)) {
				add_btn->activate(false);
			}
		}
		// - button
		x += w + 10;
		if(achivs.isOptionPresent(b_type, false)) {
			const Geometry::Rect r_sub = gui_wnd.getRect().getPlacedRect(MARGIN + x, MARGIN + y_offset - 2 + i * local_height, w, h, Geometry::ALIGN_BEGIN, Geometry::ALIGN_END);
			GUI::GUIButton* sub_btn = new GUI::GUIButton(r_sub, lfont, GUI::GUIObject::DRAW_ROUNDED_RECT_MASK, button_id+1);
			gui_wnd.registerObject(sub_btn);
			GUI::SetTextColors(sub_btn, DYNAMIC_TEXT_COLORS);
			GUI::SetColors(sub_btn, DOWNGRADES_COLORS);
			sub_btn->setText( achivs.getCurrentText(b_type, false).c_str() );
			sub_btn->setCallback( new ButtonCallback(*this) );
		}
	}

	// back to main menu
	const Geometry::Rect r_back = gui_wnd.getRect().getPlacedRect(MARGIN, MARGIN, 0, 0, Geometry::ALIGN_BEGIN, Geometry::ALIGN_BEGIN);
	GUI::GUIButton* back_btn = new GUI::GUIButton(r_back, GUI::GUIObject::DRAW_IMAGE_ALPHA, BTN_BACK_TO_MAIN_MENU, "game-back-button");
	gui_wnd.registerObject(back_btn);
	GUI::SetColors(back_btn, BUTTON_ALPHA_COLORS);
	back_btn->setCallback( new ButtonCallback(*this) );
};

void GameWindow :: changeSubMenu(State new_state) {
	assert(state != STATE_GAME && "This method changes mode only between different menus");
	state = new_state;
	setRenderSize(getRenderSize(), true);
};

void GameWindow :: processChosenMap(size_t index_in_list) {
	if(index_in_list == -1) {
		return;
	}
	const std::string& fname = ProfileData::get().mapPath(index_in_list);
	engine_wnd.getMapsController().setCurrentMapIndex(index_in_list);

	try {
		engine_wnd.readMap(fname.c_str());
		state = STATE_GAME;
	} catch (std::exception& e) {
		ShowMessage(StringFor("error_reading_map"), e.what());
	}
};

void GameWindow :: processChosenResolution(Geometry::Size size) {
	setRenderSize(size);
	SupplyWindow(this);
	ProfileData::get().screenSize() = size;
};

// nested ------------------------------------------------------------------------------

bool GameWindow::MapsListBoxCallback :: action(GUI::CallbackParams& params) {
	if((params.action == GUI::KEY_MOUSE_LEFT) && (! params.key_down) && params.hit_area) {
		if(params.length != 0) {
			client.processChosenMap(params.index);
		}
	}
	return false;
};

bool GameWindow::ResolutionsListBoxCallback :: action(GUI::CallbackParams& params) {
	if((params.action == GUI::KEY_MOUSE_LEFT) && (! params.key_down) && params.hit_area) {
		if(params.length != 0) {
			client.wnd_need_resize.param = ResolutionLBController::getSize(params.index);
			client.wnd_need_resize.toggleOn();
		}
	}
	return false;
};

bool GameWindow::ButtonCallback :: action(GUI::CallbackParams& params) {
	const size_t wnd_id = params.caller->getID();
	if(wnd_id == GameWindow::BTN_RADIOS) {
		if((params.action == GUI::KEY_MOUSE_LEFT) && params.length && params.hit_area) {
			assert(params.index < AI::AIProcessor::LEVEL_COUNT && "Invalid AI level index from UI level");
			ProfileData::get().aiLevel() = AI::AIProcessor::AILevel(params.index);
		}
		return false;
	} else if(wnd_id == GameWindow::BTN_QUAKERS) {
		if((params.action == GUI::KEY_MOUSE_LEFT) && params.length && params.hit_area) {
			if(params.length) {
				ProfileData::get().areQuakersAllowed() = params.state ? true : false;
			}
		}
		return false;
	} else if((params.action == GUI::KEY_MOUSE_LEFT) && hitCompleted(params)) {
		if(wnd_id == GameWindow::BTN_ABOUT) {
			ShowAboutWindow();
		} else if(wnd_id == GameWindow::BTN_HELP) {
			ShowHelpWindow();
		} else if(wnd_id == GameWindow::BTN_SETTINGS) {
			client.wnd_need_submenu_change.param = GameWindow::STATE_SETTINGS_MENU;
			client.wnd_need_submenu_change.toggleOn();
		} else if(wnd_id == GameWindow::BTN_BACK_TO_MAIN_MENU) {
			client.wnd_need_submenu_change.param = GameWindow::STATE_MAIN_MENU;
			client.wnd_need_submenu_change.toggleOn();
		} else if(wnd_id == GameWindow::BTN_EXIT) {
			RequestQuit(true);
		} else if(wnd_id >= GameWindow::BTN_ACHIEVEMENTS) {
			const size_t index = (wnd_id - GameWindow::BTN_ACHIEVEMENTS) / 2;
			const bool to_add = (wnd_id - GameWindow::BTN_ACHIEVEMENTS) % 2 == 0;
			Game::AchievementController& achivs = client.engine_wnd.getAchivsController();
			if(achivs.processUpgrade(Game::AchievementController::BonusType(index), to_add)) {
				client.wnd_need_submenu_change.param = GameWindow::STATE_SETTINGS_MENU;
				client.wnd_need_submenu_change.toggleOn();
			}
		}
	}
	return false;
};

}; // end Windows

}; // end Engine
