#pragma once
#include "WindowInterface.h"
#include "../Game/GameEngine.h"

namespace Engine {

namespace Windows {

class GameWindow : public WindowInterface {
	enum State {
		STATE_MAIN_MENU,
		STATE_SETTINGS_MENU,
		STATE_GAME
	} state;
	Game::GameEngine engine_wnd; // game subwindow
	GUI::GUIWindow gui_wnd; // menu subwindow
	Helpers::Request<Geometry::Size> wnd_need_resize; // request for resizing window, handled differently
	Helpers::Request<State> wnd_need_submenu_change; // request for changing submenu, handled differently
public:
	GameWindow();
	~GameWindow();

	void timerProc(size_t milliseconds); // overloading
	bool handleInput(Controls key, bool key_down, Geometry::Point pos); // overloading
	bool handleInput(char c, bool key_down); // overloading
	bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos); // overloading
	bool forcedRender(); // overloading
	void onClose(); // overloading
protected:
	bool resizeHandler(Geometry::Size size); // overloading
private:
	bool resizeOnMenuMode(Geometry::Size size);
	void initMainMenuItems(Geometry::Size size);
	void initSettingsMenuItems(Geometry::Size size);
	void changeSubMenu(State new_state); // changes one menu state to another

	void processChosenMap(size_t index_in_list);
	void processChosenResolution(Geometry::Size size);
	// callback to process list with maps
	DECLARE_TYPICAL_CALLBACK(MapsListBoxCallback, GUI::CallbackInterface, GameWindow&, GUI::CallbackParams&)
	DECLARE_TYPICAL_CALLBACK(ResolutionsListBoxCallback, GUI::CallbackInterface, GameWindow&, GUI::CallbackParams&)
	enum Button {
		BTN_SETTINGS,
		BTN_QUAKERS,
		BTN_BACK_TO_MAIN_MENU,
		BTN_RADIOS,
		BTN_ABOUT,
		BTN_EXIT,
		BTN_HELP,
		BTN_ACHIEVEMENTS = 1000
	};
	DECLARE_TYPICAL_CALLBACK(ButtonCallback, GUI::CallbackInterface, GameWindow&, GUI::CallbackParams&)
};

}; // end Windows

}; // end Engine
