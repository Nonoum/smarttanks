#pragma once
#include "WindowInterface.h"

namespace Engine {

namespace Windows {

struct ListMessageParams {
	size_t index_clicked;
	const char* string_to_push;
	bool to_list;
	//------------------------------------------------------
	ListMessageParams(const char* str); // message to List
	ListMessageParams(size_t index_string_clicked); // message from list
};

typedef AutoPoolUsing::Connector<ListMessageParams&> ListConnector;

class TypesListWindow : public WindowInterface,
						public ListConnector {
	GUI::GUIWindow wnd;
	GUI::GUIListBox* listbox; // will be cleaned by (wnd)
	std::vector<std::string> strings; // local storage for saving between/before allocating ('setRenderSize')
public:
	TypesListWindow();
	~TypesListWindow();

	bool handleInput(Controls key, bool key_down, Geometry::Point pos); // overloading
	bool handleInput(char c, bool key_down); // overloading
	bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos); // overloading
	bool forcedRender(); // overloading
protected:
	bool resizeHandler(Geometry::Size size); // overloading
	void notificationHandler(ListMessageParams& params); // overloading
private:
	// clicking callback for (listbox) : sending index of selected row
	DECLARE_TYPICAL_CALLBACK(LocalListBoxCallback, GUI::CallbackInterface, TypesListWindow&, GUI::CallbackParams&)
};

}; // end Windows

}; // end Engine
