#include "SimpleWindows.h"
#include "../EngineConfig.h"
#include <sstream>

namespace Engine {

namespace Windows {

class HelpWindow : public WindowInterface {
	GUI::GUIWindow gui_wnd; // menu sub-window

	HelpWindow() : WindowInterface(StringFor("help_caption")), gui_wnd(Geometry::Point(), GUI::GUIObject::DRAW_IMAGE_SOLID) {
		gui_wnd.setCallback( new GUICallback(*this) );
		gui_wnd.setCachedImage("simple-background");
	};
	static const std::string& getHelpText() {
		static std::string text;
		if(! text.length()) {
			std::ostringstream oss;
			oss << "Ingame controls:\n";
			oss << "- Arrows - move tank\n";
			oss << "- Space - shot\n";
			oss << "- Escape - exit to menu\n";
			oss << "- R - restart level\n";
			oss << "- Shift - stop tank\n";
			oss << "- Ctrl - switch manual/auto moving\n";
			oss << "- Pause - pause/unpause the game\n";
			text = oss.str();
		}
		return text;
	};
public:
	static WindowInterface* get() {
		static HelpWindow wnd;
		return &wnd;
	};
	bool handleInput(Controls key, bool key_down, Geometry::Point pos) { // overloading
		return false;
	};
	bool handleInput(char c, bool key_down) { // overloading
		return gui_wnd.handleInput(c, key_down);
	};
	bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) { // overloading
		return gui_wnd.handleInput(key, key_down, pos);
	};
	bool forcedRender() { // overloading
		return gui_wnd.render(true);
	};
	void onClose() {
		setRenderSize(Geometry::Size()); // clean window
	};
protected:
	bool resizeHandler(Geometry::Size size) { // overloading
		gui_wnd.clearObjects(false); // cleaning previous objects
		bool result = gui_wnd.setRenderSize(size);

		if(! size) {
			return result;
		}

		const ssize margin = 10;
		const Graphic32::FontInfo& lfont = EngineConfig::get().lFont();

		Geometry::Rect r1(margin, margin, size.width() - margin*2, size.height() - margin*2);
		GUI::GUITextZone* text = new GUI::GUITextZone(r1, lfont, GUI::GUIObject::DRAW_NONE);
		gui_wnd.registerObject(text);
		text->setText(getHelpText().c_str());
		const uint32 text_colors[] = {0x0000FFFF, 0x0000FFFF, 0x0000FFFF};
		GUI::SetTextColors(text, text_colors);
		return result;
	};
};

WindowInterface* ShowHelpWindow() {
	WindowInterface* wnd = HelpWindow::get();
	wnd->setRenderSize(Geometry::Size(420, 180));
	SupplyWindow(wnd);
	return wnd;
};

void CloseHelpWindow() {
	HelpWindow::get()->close();
};

}; // end Windows

}; // end Engine
