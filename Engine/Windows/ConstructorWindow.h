#pragma once
#include "WindowInterface.h"
#include "TypesListWindow.h"
#include "MapParamsWindow.h"
#include "ObjectParamsWindow.h"
#include "../Constructor/MapMaker.h"

namespace Engine {

namespace Windows {

class ConstructorWindow : public WindowInterface,
							public ListConnector,
							public MapParamsConnector,
							public ObjectParamsConnector {
	//------------------------------------------------------
	// local subwindows
	Constructor::MapMaker maker_wnd; // map-maker subwindow
	GUI::GUIWindow gui_wnd;
	// window interfaces
	TypesListWindow list_wnd; // subwindow object for list of object types
	MapParamsWindow map_params_wnd; // subwindow object for map parameters
	ObjectParamsWindow object_params_wnd; // subwindow object for game object parameters
	// other members
	Helpers::Request<ChosenMapParams> map_params_request;
	bool map_initialized;
public:
	ConstructorWindow();
	~ConstructorWindow();

	bool handleInput(Controls key, bool key_down, Geometry::Point pos); // overloading
	bool handleInput(char c, bool key_down); // overloading
	bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos); // overloading
	bool forcedRender(); // overloading
protected:
	void notificationHandler(ListMessageParams&); // overloading for connector to list_wnd
	void notificationHandler(ChosenMapParams&); // overloading for connector to map_params_wnd
	void notificationHandler(ObjectDialogParams&); // overloading for connector to object_params_wnd
	bool resizeHandler(Geometry::Size size); // overloading
	void onFullUnlock(); // overloading
private:
	void fillTypesListWindow();
	// nested, callback for handling selection
	DECLARE_TYPICAL_CALLBACK(SelectionCallback, Constructor::SelectionCallbackBase, ConstructorWindow&, Constructor::SelectionCallbackParams&)
	// button callbacks. all in one
	enum Button {
		BTN_LOAD,
		BTN_SAVE,
		BTN_CREATE,
		BTN_SHOWLIST,
		BTN_ABOUT
	};
	DECLARE_TYPICAL_CALLBACK(ButtonCallback, GUI::CallbackInterface, ConstructorWindow&, GUI::CallbackParams&)
};

}; // end Windows

}; // end Engine
