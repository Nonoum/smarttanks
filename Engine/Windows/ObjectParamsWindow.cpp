#include "ObjectParamsWindow.h"
#include "../EngineConfig.h"
#include "../ObjectInterface.h"
#include "../ResourceInitializer.h"

namespace Engine {

namespace Windows {

ObjectParamsWindow :: ObjectParamsWindow()
		: WindowInterface(StringFor("object_params_caption")),
		gui_wnd(Geometry::Point(), GUI::GUIObject::DRAW_IMAGE_SOLID),
		local_object_clone(NULL), edit(NULL) {

	Engine::ResourceInitializer init_gui_images(Engine::ResourceInitializer::RES_GUI_IMAGES);

	gui_wnd.setOutputPosition( Geometry::Point(0, 0) );
	gui_wnd.setCallback( new GUICallback(*this) );
	gui_wnd.setCachedImage("window-background");
};

ObjectParamsWindow :: ~ObjectParamsWindow() {
	setParams(NULL);
	edit = NULL; // losing pointer
};

bool ObjectParamsWindow :: handleInput(Controls key, bool key_down, Geometry::Point pos) { // empty
	return false;
};

bool ObjectParamsWindow :: handleInput(char c, bool key_down) {
	return gui_wnd.handleInput(c, key_down);
};

bool ObjectParamsWindow :: handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) {
	return gui_wnd.handleInput(key, key_down, pos);
};

bool ObjectParamsWindow :: forcedRender() {
	return gui_wnd.render(true);
};

bool ObjectParamsWindow :: resizeHandler(Geometry::Size size) {
	bool result = gui_wnd.setRenderSize(size);
	gui_wnd.clearObjects(false); // cleaning previous objects

	if(! size) {
		return result;
	}

	ssize item_y = 10;
	ssize item_x = 10;
	ssize item_width = 260;
	ssize item_height = 20;
	ssize item_y_disp = 20;
	{
		Geometry::Rect rect(item_x, item_y, item_width, item_height);
		GUI::GUIButton* apply = CreateConfiguredGUIButton(rect);
		apply->setText(StringFor("approve_object_parameters"));

		apply->setCallback( new ApplyButtonCallback(*this) );
		gui_wnd.registerObject( apply );
		item_y += item_height + item_y_disp;
	}
	{
		Geometry::Rect rect(item_x, item_y, 260, 300);
		edit = CreateConfiguredGUIEdit(rect);
		setText(); // filling 'edit' subwindow if parameters was already set
		gui_wnd.registerObject( edit );
	}
	return result;
};

void ObjectParamsWindow :: notificationHandler(ObjectDialogParams&) { // empty
};

void ObjectParamsWindow :: setParams(ObjectInterface* cloned_autopool_object) {
	if(local_object_clone) { // have local copy
		local_object_clone->removeFromPool(); // cleaning
	}
	local_object_clone = cloned_autopool_object;
	setText();
};

void ObjectParamsWindow :: setText() {
	if(! edit) {
		return;
	}
	if(local_object_clone) {
		edit->setText( local_object_clone->get().c_str() );
	} else {
		edit->setText("");
	}
};

bool ObjectParamsWindow :: doneCatcher() {
	if((! edit) || (! local_object_clone)) {
		return false;
	}
	std::string input = edit->getText();
	if(! local_object_clone->set(input)) {
		return false;
	}
	ObjectDialogParams params;
	params.str = input;
	notify( params );
	return true;
};

// nested

bool ObjectParamsWindow::ApplyButtonCallback :: action(GUI::CallbackParams& params) {
	if(params.action == GUI::KEY_MOUSE_LEFT && (! params.key_down) && params.hit_area) {
		if(client.doneCatcher()) { // parameters are correct and has been sent
			client.close();
		}
	}
	return false;
};

}; // end Windows

}; // end Engine
