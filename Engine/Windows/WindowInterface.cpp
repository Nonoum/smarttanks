#include "WindowInterface.h"

namespace Engine {

namespace Windows {

WindowCallbackParams :: WindowCallbackParams(const Graphic32::Img32* _screen, size_t _len, size_t _ind, Geometry::Rect _area, Geometry::Point _pos, Geometry::Size _total_window_size)
		: screen(_screen), length(_len), index(_ind), area(_area), pos(_pos), cmd(WND_DRAW), total_window_size(_total_window_size) {
};

WindowCallbackParams :: WindowCallbackParams(int exit_code)
		: cmd(WND_EXIT), index(exit_code) {
};

//------------------------------------------------------

WindowInterface :: WindowInterface(const char* static_wnd_name)
		: window_name(static_wnd_name), render_size(0, 0) {

	if(! window_name) {
		window_name = "";
	}
};

WindowInterface :: ~WindowInterface() {
	WindowCallbackParams params(0);
	sendCallback( params ); // exit window
};

void WindowInterface :: timerProc(size_t milliseconds) {
};

const char* WindowInterface :: getWindowName() const {
	return window_name;
};

bool WindowInterface :: setRenderSize(Geometry::Size size, bool forced) {
	Helpers::AutoLocker auto_locker(*this, resizing_request);
	if(! auto_locker) {
		resizing_request.param = size;
		return false;
	}
	if(render_size == size && (! forced)) {
		return false;
	}
	render_size = size;
	bool result = resizeHandler(size);
	forcedRender(); // calling render explicitly here
	return result;
};

Geometry::Size WindowInterface :: getRenderSize() const {
	return render_size;
};

void WindowInterface :: onFullUnlock() {
	if(resizing_request.isSet()) {
		setRenderSize(resizing_request.param);
	}
};

void WindowInterface :: close() {
	WindowCallbackParams params(0); // exit code
	sendCallback( params );
};

void WindowInterface :: onClose() {
};

//------------------------------------------------------
// nested - callbacks

bool WindowInterface::GUICallback :: action(GUI::CallbackParams& params) {
	if(params.action == GUI::KEY_RENDER_ACTION) {
		WindowCallbackParams _params(params.screen, params.length, params.index, params.area,
			params.caller->getRect().getPoint(), client.getRenderSize()); // drawing
		return client.sendCallback(_params);
	}
	return false;
};

bool WindowInterface::EngineCallback :: action(EngineCallbackParams& params) {
	const ssize w = params.image->width();
	const ssize h = params.image->height();
	Geometry::Rect area(0, 0, w, h); // area to refresh - full image
	WindowCallbackParams _params(params.image, 1, 0, area, params.position, client.getRenderSize()); // drawing
	return client.sendCallback(_params);
};

}; // end Windows

}; // end Engine
