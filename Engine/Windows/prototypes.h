#pragma once
#include "../imports.h"

namespace Engine {

namespace Windows {

using namespace Graphic32;

class WindowInterface;

// platform-independent common stuff

// GUI styles for Constructor declared here
GUI::GUIButton* CreateConfiguredGUIButton(Geometry::Rect rect, size_t id = 0);
GUI::GUIEdit* CreateConfiguredGUIEdit(Geometry::Rect rect);

// TODO: consider moving quit-related functions somewhere
void RequestQuit(bool quit);
bool IsQuitRequested();

// platform-dependent stuff

/*
	Function for creating window by OS for specified client.
	Implement it for target OS.
*/
bool SupplyWindow(WindowInterface* client);
/*
	Functions for taking filenames for reading and writing (respectively).
	Implement it for target OS.
*/
std::string ROpenFileDialog(const char *szExtensions, const char *szFormatInfo);
std::string WOpenFileDialog(const char *szExtensions, const char *szFormatInfo);
/*
	Function for displaying notifications and errors.
	Implement it for target OS.
*/
void ShowMessage(const char* caption, const char* message);

/*
	Performs quit for application.
	Implement it for target OS.
*/
void QuitApplication();

}; // end Windows

}; // end Engine
