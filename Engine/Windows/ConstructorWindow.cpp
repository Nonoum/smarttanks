#include "ConstructorWindow.h"
#include "SimpleWindows.h"
#include "../ResourceInitializer.h"
#include <stdexcept>

namespace Engine {

namespace Windows {

ConstructorWindow :: ConstructorWindow()
		: WindowInterface(StringFor("constructor_caption")),
		gui_wnd(Geometry::Point(), GUI::GUIObject::DRAW_IMAGE_SOLID) {

	Engine::ResourceInitializer init_gui_images(Engine::ResourceInitializer::RES_GUI_IMAGES);

	gui_wnd.setCachedImage("window-background");
	// default positions
	gui_wnd.setOutputPosition( Geometry::Point(0, 0) ); // will be corrected in 'resizeHandler'
	maker_wnd.setOutputPosition( Geometry::Point(0, 0) ); // same here
	// callbacks
	gui_wnd.setCallback( new GUICallback(*this) );
	maker_wnd.DisplayController::setCallback( new EngineCallback(*this) ); // workaround for stupid compiler
	maker_wnd.Constructor::SelectionCallbackStorage::setCallback( new SelectionCallback(*this) ); // same here
	// connections for notifications
	list_wnd.connect(this);
	map_params_wnd.connect(this);
	object_params_wnd.connect(this);
	// preparing types list
	fillTypesListWindow();
	// others
	map_initialized = false;
};

ConstructorWindow :: ~ConstructorWindow() {
};

void ConstructorWindow :: fillTypesListWindow() {
	std::vector<std::string> types;
	maker_wnd.getTypesNames( types );
	for(size_t i = 0; i < types.size(); ++i) {
		ListMessageParams params( types[i].c_str() );
		ListConnector::notify( params );
	}
};

bool ConstructorWindow :: handleInput(Controls key, bool key_down, Geometry::Point pos) {
	switch (key) {
	case KEY_MOUSE_LEFT:
	case KEY_MOUSE_RIGHT:
	case KEY_MOUSE_MOVE:
	case KEY_MOUSE_DOUBLE:
		{
			const Geometry::Rect wnd_rect(maker_wnd.getOutputPosition(), maker_wnd.getRenderSize());
			if(! (wnd_rect & pos)) {
				return false;
			}
		}
		maker_wnd.setCurrentCursorPos(pos);
		break;
	default:
		break;
	};
	return maker_wnd.handleInput(key, key_down);
};

bool ConstructorWindow :: handleInput(char c, bool key_down) {
	return gui_wnd.handleInput(c, key_down);
};

bool ConstructorWindow :: handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) {
	return gui_wnd.handleInput(key, key_down, pos);
};

bool ConstructorWindow :: forcedRender() {
	bool result = maker_wnd.render(true);
	result |= gui_wnd.render(true);
	return result;
};

bool ConstructorWindow :: resizeHandler(Geometry::Size size) {
	ssize gui_height = EngineConfig::get().constructorGUIHeight();
	if(! size.canFit( EngineConfig::get().minWindowSize() )) {
		throw std::runtime_error("ConstructorWindow::resizeHandler error : too small window size requested");
	}
	Geometry::Size gui_wnd_size, maker_wnd_size;
	// preparing sizes of sub-windows
	gui_wnd_size.setSize(size.width(), gui_height);
	maker_wnd_size.setSize(size.width(), size.height() - gui_wnd_size.height());
	// setting sizes of subwindows
	bool result = gui_wnd.setRenderSize( gui_wnd_size );
	result |= maker_wnd.setRenderSize( maker_wnd_size );
	// setting positions of subwindows
	gui_wnd.setOutputPosition( Geometry::Point(0, 0) ); // setting gui subwindow position here
	maker_wnd.setOutputPosition( Geometry::Point(0, gui_height) ); // setting engine subwindow position here
	// preparing gui subwindow
	gui_wnd.clearObjects(false); // cleaning previous objects here

	if(! size) {
		return result;
	}

	ssize buttons_height = gui_height * 2 / 3;
	ssize buttons_y = (gui_height - buttons_height) / 2;
	ssize button_x = 10;
	ssize button_width = 102;
	ssize button_x_disp = 12;
	{
		Geometry::Rect rect(button_x, buttons_y, button_width, buttons_height);
		GUI::GUIButton* load_map = CreateConfiguredGUIButton(rect, BTN_LOAD);
		load_map->setText(StringFor("load_map"));

		load_map->setCallback( new ButtonCallback(*this) );
		gui_wnd.registerObject( load_map );
		button_x += button_width + button_x_disp; // moving X to next
	}
	{
		Geometry::Rect rect(button_x, buttons_y, button_width, buttons_height);
		GUI::GUIButton* save_map = CreateConfiguredGUIButton(rect, BTN_SAVE);
		save_map->setText(StringFor("save_map"));

		save_map->setCallback( new ButtonCallback(*this) );
		gui_wnd.registerObject( save_map );
		button_x += button_width + button_x_disp; // moving X to next
	}
	{
		button_width += 18;
		Geometry::Rect rect(button_x, buttons_y, button_width, buttons_height);
		GUI::GUIButton* create_map = CreateConfiguredGUIButton(rect, BTN_CREATE);
		create_map->setText(StringFor("create_map"));

		create_map->setCallback( new ButtonCallback(*this) );
		gui_wnd.registerObject( create_map );
		button_x += button_width + button_x_disp; // moving X to next
	}
	{
		button_width += 56;
		Geometry::Rect rect(button_x, buttons_y, button_width, buttons_height);
		GUI::GUIButton* show_list = CreateConfiguredGUIButton(rect, BTN_SHOWLIST);
		show_list->setText(StringFor("show_types_list"));

		show_list->setCallback( new ButtonCallback(*this) );
		gui_wnd.registerObject( show_list );
		button_x += button_width + button_x_disp; // moving X to next
	}
	{
		button_width -= 102;
		Geometry::Rect rect(button_x, buttons_y, button_width, buttons_height);
		GUI::GUIButton* about = CreateConfiguredGUIButton(rect, BTN_ABOUT);
		about->setText(StringFor("about_button"));

		about->setCallback( new ButtonCallback(*this) );
		gui_wnd.registerObject( about );
		button_x += button_width + button_x_disp; // moving X to next
	}
	if((! map_initialized) && (maker_wnd_size)) { // for first set of rendering size do initialization of map
		maker_wnd.initMap( maker_wnd_size ); // initializing map for screen size
		map_initialized = true;
	}
	return result;
};

// notifications and requests

void ConstructorWindow :: notificationHandler(ListMessageParams& params) {
	if(! params.to_list) {
		maker_wnd.selectType( params.index_clicked );
	}
};

void ConstructorWindow :: notificationHandler(ChosenMapParams& params) {
	Helpers::AutoLocker auto_locker(*this, map_params_request);
	if(! auto_locker) {
		map_params_request.param = params;
		return;
	}
	maker_wnd.initMap( params.map_size );
};

void ConstructorWindow :: notificationHandler(ObjectDialogParams& params) {
	maker_wnd.setParamsForSelectedObject( params.str );
};

void ConstructorWindow :: onFullUnlock() {
	WindowInterface::onFullUnlock();
	if(map_params_request.isSet()) {
		notificationHandler( map_params_request.param );
	}
};

// nested - callback

bool ConstructorWindow::SelectionCallback :: action(Constructor::SelectionCallbackParams& params) {
	client.object_params_wnd.setRenderSize( Geometry::Size(280, 370) );
	client.object_params_wnd.setParams( params.autopool_clone_of_selected );
	client.object_params_wnd.forcedRender();
	SupplyWindow(& client.object_params_wnd);
	return true;
};

bool ConstructorWindow::ButtonCallback :: action(GUI::CallbackParams& params) {
	if((params.action == GUI::KEY_MOUSE_LEFT) && hitCompleted(params)) {
		const size_t wnd_id = params.caller->getID();
		if(wnd_id == BTN_LOAD) {
			std::string fname = ROpenFileDialog("*.tmap", StringFor("tanks_map_phrase"));
			if(fname.length() > 0) {
				client.maker_wnd.readMap( fname );
			}
		} else if(wnd_id == BTN_SAVE) {
			std::string fname = WOpenFileDialog("*.tmap", StringFor("tanks_map_phrase"));
			if(fname.length() > 0) {
				client.maker_wnd.writeMap( fname );
			}
		} else if(wnd_id == BTN_CREATE) {
			client.map_params_wnd.setRenderSize( Geometry::Size(264, 100) );
			client.map_params_wnd.forcedRender();
			SupplyWindow(& client.map_params_wnd);
		} else if(wnd_id == BTN_SHOWLIST) {
			client.list_wnd.setRenderSize( Geometry::Size(256, 380) );
			client.list_wnd.forcedRender();
			SupplyWindow(& client.list_wnd);
		} else if(wnd_id == BTN_ABOUT) {
			ShowAboutWindow();
		}
	}
	return false;
};

}; // end Windows

}; // end Engine
