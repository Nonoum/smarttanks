#pragma once
#include "WindowInterface.h"

namespace Engine {

namespace Windows {

/*
	Opens About window, returns pointer to WindowInterface of it.
*/
WindowInterface* ShowAboutWindow();

/*
	Closes About window if it's opened.
*/
void CloseAboutWindow();

/*
	Opens Help window, returns pointer to WindowInterface of it.
*/
WindowInterface* ShowHelpWindow();

/*
	Closes Help window if it's opened.
*/
void CloseHelpWindow();


}; // end Windows

}; // end Engine
