#pragma once
#include "WindowInterface.h"

namespace Engine {

class ObjectInterface; // forward declaration

namespace Windows {

struct ObjectDialogParams {
	std::string str;
};

typedef AutoPoolUsing::Connector<ObjectDialogParams&> ObjectParamsConnector;

class ObjectParamsWindow : public WindowInterface,
							public ObjectParamsConnector {
	GUI::GUIWindow gui_wnd;
	GUI::GUIEdit* edit; // edit window object
	ObjectInterface* local_object_clone; // cloned object of specific class to work with parameters for it
public:
	ObjectParamsWindow();
	~ObjectParamsWindow();

	bool handleInput(Controls key, bool key_down, Geometry::Point pos); // overloading
	bool handleInput(char c, bool key_down); // overloading
	bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos); // overloading
	bool forcedRender(); // overloading
	// specific methods
	void setParams(ObjectInterface* cloned_autopool_object); // sets 'local_object_clone' and calls 'setText'
protected:
	bool resizeHandler(Geometry::Size size); // overloading
	void notificationHandler(ObjectDialogParams&); // overloading for connector
private:
	void setText(); // fills 'edit' subwindow with parameters of 'local_object_clone'
	bool doneCatcher(); // returns true if inputs are correct and window sent the notification
	//-----------------------------------
	DECLARE_TYPICAL_CALLBACK(ApplyButtonCallback, GUI::CallbackInterface, ObjectParamsWindow&, GUI::CallbackParams&)
};

}; // end Windows

}; // end Engine
