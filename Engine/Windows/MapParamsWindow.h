#pragma once
#include "WindowInterface.h"

namespace Engine {

namespace Windows {

struct ChosenMapParams {
	Geometry::Size map_size;
};

typedef AutoPoolUsing::Connector<ChosenMapParams&> MapParamsConnector;

class MapParamsWindow : public WindowInterface,
						public MapParamsConnector {
	GUI::GUIWindow gui_wnd;
	const Geometry::Size min_map_size;
	GUI::GUIEdit* edit_width; // edit window for width of map
	GUI::GUIEdit* edit_height; // -:- height
public:
	MapParamsWindow();
	~MapParamsWindow();

	bool handleInput(Controls key, bool key_down, Geometry::Point pos); // overloading
	bool handleInput(char c, bool key_down); // overloading
	bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos); // overloading
	bool forcedRender(); // overloading
protected:
	bool resizeHandler(Geometry::Size size); // overloading
	void notificationHandler(ChosenMapParams&); // overloading for connector
private:
	bool doneCatcher(); // returns true if inputs are correct and window sent the notification
	//-----------------------------------
	DECLARE_TYPICAL_CALLBACK(ApplyButtonCallback, GUI::CallbackInterface, MapParamsWindow&, GUI::CallbackParams&)
};

}; // end Windows

}; // end Engine
