#include "prototypes.h"
#include "../EngineConfig.h"

namespace Engine {

namespace Windows {

GUI::GUIButton* CreateConfiguredGUIButton(Geometry::Rect rect, size_t id) {
	const Graphic32::FontInfo font = EngineConfig::get().lFont();
	GUI::GUIButton* btn = new GUI::GUIButton(rect, font, GUI::GUIObject::DRAW_IMAGE_ALPHA, id);
	btn->setCachedImage("text-background");
	const uint32 text_colors[] = {0xFF80FF00, 0xFFFF8000, 0xFFFFFF00};
	GUI::SetTextColors(btn, text_colors);
	const uint32 colors[] = {0xFF000000, 0xE0000000, 0xC0000000};
	GUI::SetColors(btn, colors);
	return btn;
};

GUI::GUIEdit* CreateConfiguredGUIEdit(Geometry::Rect rect) {
	const Graphic32::FontInfo font = EngineConfig::get().lFont();
	GUI::GUIEdit* edit = new GUI::GUIEdit(rect, font, GUI::GUIObject::DRAW_IMAGE_SOLID);
	edit->setCachedImage("text-background");
	const uint32 text_colors[] = {0xFF80FF00, 0xFFFF8000, 0xFFFFFF00};
	GUI::SetTextColors(edit, text_colors);
	return edit;
};

static volatile bool quit_requested = false;

void RequestQuit(bool quit) {
	quit_requested = quit;
};

bool IsQuitRequested() {
	return quit_requested;
};

}; // end Windows

}; // end Engine
