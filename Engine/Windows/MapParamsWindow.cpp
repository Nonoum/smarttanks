#include "MapParamsWindow.h"
#include "../EngineConfig.h"
#include "../ResourceInitializer.h"
#include <sstream>

namespace Engine {

namespace Windows {

MapParamsWindow :: MapParamsWindow()
		: WindowInterface(StringFor("map_params_caption")),
		gui_wnd(Geometry::Point(), GUI::GUIObject::DRAW_IMAGE_SOLID),
		min_map_size(EngineConfig::get().minWindowSize()) { // min-map-size == min-window-size

	Engine::ResourceInitializer init_gui_images(Engine::ResourceInitializer::RES_GUI_IMAGES);

	gui_wnd.setOutputPosition( Geometry::Point(0, 0) );
	gui_wnd.setCallback( new GUICallback(*this) );
	gui_wnd.setCachedImage("window-background");
};

MapParamsWindow :: ~MapParamsWindow() {
};

bool MapParamsWindow :: handleInput(Controls key, bool key_down, Geometry::Point pos) { // empty
	return false;
};

bool MapParamsWindow :: handleInput(char c, bool key_down) {
	return gui_wnd.handleInput(c, key_down);
};

bool MapParamsWindow :: handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) {
	return gui_wnd.handleInput(key, key_down, pos);
};

bool MapParamsWindow :: forcedRender() {
	return gui_wnd.render(true);
};

bool MapParamsWindow :: resizeHandler(Geometry::Size size) {
	bool result = gui_wnd.setRenderSize(size);
	gui_wnd.clearObjects(false); // cleaning previous objects

	if(! size) {
		return result;
	}

	ssize item_y = 10;
	ssize item_x = 10;
	ssize item_width = 112;
	ssize item_height = 28;
	ssize item_x_disp = 20;
	ssize item_y_disp = 20;
	{
		Geometry::Rect rect(item_x, item_y, item_width, item_height);
		GUI::GUIButton* create_map = CreateConfiguredGUIButton(rect);
		create_map->setText(StringFor("approve_map_creation"));

		create_map->setCallback( new ApplyButtonCallback(*this) );
		gui_wnd.registerObject( create_map );
		item_y += item_height + item_y_disp;
	}
	{
		Geometry::Rect rect(item_x, item_y, item_width, item_height);
		edit_width = CreateConfiguredGUIEdit(rect);
		{
			std::ostringstream min_width;
			min_width << min_map_size.width();
			edit_width->setText( min_width.str().c_str() ); // initializing string here
		}
		gui_wnd.registerObject( edit_width );
		item_x += item_width + item_x_disp; // moving X to next
	}
	{
		Geometry::Rect rect(item_x, item_y, item_width, item_height);
		edit_height = CreateConfiguredGUIEdit(rect);
		{
			std::ostringstream min_height;
			min_height << min_map_size.width();
			edit_height->setText( min_height.str().c_str() ); // initializing string here
		}
		gui_wnd.registerObject( edit_height );
	}
	return result;
};

void MapParamsWindow :: notificationHandler(ChosenMapParams&) { // empty
};

bool MapParamsWindow :: doneCatcher() {
	// parsing inputs
	ssize inputs[2];
	GUI::GUIEdit* edits[2] = {edit_width, edit_height};
	const Geometry::Size max_limit_size = EngineConfig::get().maxMapSize();

	const ssize max_limits[2] = {max_limit_size.width(), max_limit_size.height()};
	const ssize min_limits[2] = {min_map_size.width(), min_map_size.height()};
	bool limits_failed = false;
	for(int i = 0; i < 2; ++i) {
		if(! edits[i]) {
			return false; // window hasn't been initialized
		}
		std::string str( edits[i]->getText() );
		std::istringstream iss( str );
		iss >> inputs[i];
		if(! iss) {
			return false; // can't parse
		}
		bool failed = false;
		if(inputs[i] > max_limits[i]) {
			inputs[i] = max_limits[i];
			failed = true;
		}
		if(inputs[i] < min_limits[i]) {
			inputs[i] = min_limits[i];
			failed = true;
		}
		if(failed) {
			// setting corrected size in UI
			std::ostringstream oss;
			oss << inputs[i];
			edits[i]->setText(oss.str().c_str());
			limits_failed = true;
		}
	}
	if(limits_failed) {
		return false;
	}
	ChosenMapParams params;
	params.map_size = Geometry::Size(inputs[0], inputs[1]);
	notify(params);
	return true;
};

// nested

bool MapParamsWindow::ApplyButtonCallback :: action(GUI::CallbackParams& params) {
	if(params.action == GUI::KEY_MOUSE_LEFT && (! params.key_down) && params.hit_area) {
		if(client.doneCatcher()) { // parameters are correct and has been sent
			client.close();
		}
	}
	return false;
};

}; // end Windows

}; // end Engine
