#include "../EngineConfig.h"
#include "TypesListWindow.h"
#include "../ResourceInitializer.h"

namespace Engine {

namespace Windows {

ListMessageParams :: ListMessageParams(const char* str)
		: to_list(true), string_to_push(str) {
};

ListMessageParams :: ListMessageParams(size_t index_string_clicked)
		: to_list(false), index_clicked(index_string_clicked) {

	string_to_push = NULL;
};

//------------------------------------------------------

TypesListWindow :: TypesListWindow()
		: WindowInterface(StringFor("types_list_caption")), listbox(NULL), wnd(Geometry::Point(0,0), GUI::GUIObject::DRAW_IMAGE_SOLID) {

	Engine::ResourceInitializer init_gui_images(Engine::ResourceInitializer::RES_GUI_IMAGES);

	wnd.setCallback( new GUICallback(*this) );
	wnd.setCachedImage("window-background");
};

TypesListWindow :: ~TypesListWindow() {
	listbox = NULL; // losing pointer
};

bool TypesListWindow :: handleInput(Controls key, bool key_down, Geometry::Point pos) { // empty
	return false;
};

bool TypesListWindow :: handleInput(char c, bool key_down) {
	return wnd.handleInput(c, key_down);
};

bool TypesListWindow :: handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) {
	return wnd.handleInput(key, key_down, pos);
};

bool TypesListWindow :: forcedRender() {
	return wnd.render(true);
};

bool TypesListWindow :: resizeHandler(Geometry::Size size) {
	bool result = wnd.setRenderSize(size);
	// deleting old listbox (clear window)
	wnd.clearObjects(false);

	if(! size) {
		return result;
	}

	Geometry::Rect rect(0, 0, size.width(), size.height());
	FontInfo font = EngineConfig::get().lFont();
	// allocating new listbox
	listbox = new GUI::GUIListBox(rect, font, GUI::GUIObject::DRAW_IMAGE_SOLID);
	//-- text colors
	const uint32 text_colors[] = {0x00FFFF00, 0x00FF0000, 0x00FFC0C0};
	GUI::SetTextColors(listbox, text_colors);
	listbox->setCachedImage("text-background");
	// recovering strings
	for(size_t i = 0; i < strings.size(); ++i) {
		listbox->pushString( strings[i].c_str() );
	}
	listbox->setCallback( new LocalListBoxCallback(*this) );
	// registering current
	wnd.registerObject(listbox);
	return result;
};

void TypesListWindow :: notificationHandler(ListMessageParams& params) {
	if(params.to_list) {
		strings.push_back( params.string_to_push ); // in local storage
		if(listbox) {
			listbox->pushString( params.string_to_push ); // in listbox object
			wnd.render(false); // not forced
		}
	}
};

// nested ------------------------------------------------------------------------------

bool TypesListWindow::LocalListBoxCallback :: action(GUI::CallbackParams& params) {
	if((params.action == GUI::KEY_MOUSE_LEFT) && (params.key_down)) {
		if(params.length != 0) {
			ListMessageParams _params(params.index);
			client.notify( _params ); // notification of clicking
		}
	}
	return false;
};

}; // end Windows

}; // end Engine
