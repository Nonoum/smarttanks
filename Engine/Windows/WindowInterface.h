#pragma once
#include "../imports.h"
#include "../Controls.h"
#include "../DisplayController.h"
#include "prototypes.h"

namespace Engine {

namespace Windows {

// Parameters and Callback to export rendered window fragments. interface from NnmLib : Graphic32::GUI::GUIWindow
// Implement it for target OS.
enum WindowCallbackCommand {
	WND_DRAW,
	WND_EXIT
};

struct WindowCallbackParams {
	const Graphic32::Img32* screen; // rendered image
	size_t length; // count of calls for this set (for same (screen))
	size_t index; // index of call for this set (for same (screen)) (or exit code if command is WND_EXIT)
	Geometry::Rect area; // area of screen to be redrawed
	Geometry::Point pos; // screen position
	const WindowCallbackCommand cmd; // current command
	const Geometry::Size total_window_size;
	// no default constructor
	WindowCallbackParams(const Graphic32::Img32* _screen, size_t _len, size_t _ind, Geometry::Rect _area, Geometry::Point _pos, Geometry::Size _total_window_size);
	WindowCallbackParams(int exit_code);
};

struct WindowCallbackBase {
	virtual bool action(WindowCallbackParams& params) = 0;
	virtual ~WindowCallbackBase() {};
};

//-------------------------------------------------------------------------------------------------------------

/*
	Common interface for any engine window.
*/
class WindowInterface : protected Helpers::ObjectLocker,
						public Helpers::CallbackStorage<WindowCallbackBase, WindowCallbackParams&> {
	const char* window_name;
	Geometry::Size render_size;
	Helpers::Request<Geometry::Size> resizing_request;
public:
	WindowInterface(const char* static_wnd_name);
	virtual ~WindowInterface();

	virtual void timerProc(size_t milliseconds); // empty implementation
	virtual bool forcedRender() = 0;
	virtual bool handleInput(Controls key, bool key_down, Geometry::Point pos) = 0; // engine events
	virtual bool handleInput(Graphic32::GUI::Controls key, bool key_down, Geometry::Point pos) = 0; // gui events
	virtual bool handleInput(char c, bool key_down) = 0; // gui text events
	void close(); // closes supplied window (doesn't destruct the window object)
	const char* getWindowName() const;
	bool setRenderSize(Geometry::Size size, bool forced = false); // doesn't update if size is not changed ant not forced
	Geometry::Size getRenderSize() const;
	virtual void onClose(); // called by OS on destroying OS window. empty implementation
protected:
	virtual bool resizeHandler(Geometry::Size size) = 0;
	void onFullUnlock(); // overloading
	// callback for gui subwindows
	DECLARE_TYPICAL_CALLBACK(GUICallback, GUI::CallbackInterface, WindowInterface&, GUI::CallbackParams&)
	// callback for engine subwindows
	DECLARE_TYPICAL_CALLBACK(EngineCallback, EngineCallbackBase, WindowInterface&, EngineCallbackParams&)
};

}; // end Windows

}; // end Engine
