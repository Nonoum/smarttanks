#include "ObjectSupplier.h"
#include <algorithm>
#include <stdexcept>

namespace Engine {

namespace Constructor {

struct ObjectTypeComparator {
	bool operator ()(ObjectInterface* obj1, ObjectInterface* obj2) const {
		return strcmp(obj1->name(), obj2->name()) < 0; // less
	};
};

ObjectSupplier :: ObjectSupplier() {
};

ObjectSupplier :: ~ObjectSupplier() {
	clearTypes();
};

void ObjectSupplier :: loadTypes(const ObjectTransporter& transporter) {
	clearTypes();
	transporter.bringAutoPoolCopies(types);
	std::sort(types.begin(), types.end(), ObjectTypeComparator());
};

size_t ObjectSupplier :: size() const {
	return types.size();
};

ObjectInterface* ObjectSupplier :: autoPoolObjectInterface(size_t index) const {
	if(index >= size()) {
		throw std::runtime_error("ObjectSupplier::autoPoolObjectInterface error: bad index");
	}
	return types[index]->autoPoolObjectInterface(false);
};

void ObjectSupplier :: getTypesNames(std::vector<std::string>& output) const {
	output.clear();
	for(size_t i = 0; i < types.size(); ++i) {
		output.push_back( types[i]->name() );
	}
};

void ObjectSupplier :: clearTypes() {
	std::for_each(types.begin(), types.end(), std::mem_fun(&ObjectInterface::removeFromPool));
	types.clear();
};

}; // end Constructor

}; // end Engine
