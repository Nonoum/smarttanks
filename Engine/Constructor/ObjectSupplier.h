#pragma once
#include "../ObjectTransporter.h"
#include <vector>

namespace Engine {

namespace Constructor {

/*
	Class is a factory of objects with types, registered at 'loadTypes' from ObjectTransporter.
	Class collects types by pointers of 'ObjectInterface' to object instances, and sorts
	list by type name.
*/

class ObjectSupplier : private NonCopyable {
	typedef std::vector<ObjectInterface*> CollectionType;
	CollectionType types; // list of types, sorted by name
public:
	ObjectSupplier();
	~ObjectSupplier();
	void loadTypes(const ObjectTransporter& transp);
	size_t size() const; // returns count of registered types
	void getTypesNames(std::vector<std::string>& output) const; // returns vector of types names in appropriate order
	ObjectInterface* autoPoolObjectInterface(size_t index) const; // returns new raw object in auto pool chain, by type index
private:
	void clearTypes();
};

}; // end Constructor

}; // end Engine
