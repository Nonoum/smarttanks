#pragma once
#include "../imports.h"
#include "../Controls.h"
#include "../EngineCommon.h"
#include "../EngineConfig.h"
#include "ObjectSupplier.h"

namespace Engine {

namespace Constructor {

/*
	Callback base and parameters for selection of object.
*/
struct SelectionCallbackParams {
	ObjectInterface* autopool_clone_of_selected;
};

struct SelectionCallbackBase {
	virtual bool action(SelectionCallbackParams&) = 0;
	virtual ~SelectionCallbackBase() {};
};

typedef Helpers::CallbackStorage<SelectionCallbackBase, SelectionCallbackParams&> SelectionCallbackStorage;

/*
	Map maker logic implementation (controls + render).
*/
class MapMaker : public EngineCommon,
					public SelectionCallbackStorage {
	ObjectSupplier factory;
	//-----------------------------------
	bool last_states[KEY_NKEYS]; // controls states
	size_t i_type_selected; // index of selected type
	Geometry::Point last_pos; // last mouse position
	Geometry::Point current_pos; // current mouse position
	Graphic32::TextPrinter printer; // printer for names of types
	typedef std::vector<ObjectInterface*> SelectionType;
	SelectionType selection;
	//-----------------------------------
	Helpers::Request<bool> render_request;
public:
	MapMaker();
	~MapMaker();
	void initMap(Geometry::Size map_size); // initializes map, sets unbreakable objects as map bounds
	void selectType(size_t index); // sets selected type for next object to be created
	void setCurrentCursorPos(Geometry::Point global_pos); // sets current cursor position (input - position on user window)
	bool render(bool forced);
	bool handleInput(Controls key, bool key_down);
	void setParamsForSelectedObject(const std::string& params);
	void getTypesNames(std::vector<std::string>& output); // returns vector of types names in appropriate order
protected:
	void onFullUnlock(); // overloading
private:
	bool drawObjectCorpse(const ObjectInterface* obj); // draws corpse for object and prints it's class name
	void createObject(Geometry::Rect offered); // adds to collection object with 'i_type_selected' and offered area
	void drawUndoneSelection(); // drawing current mouse selection
	void scrollMap(Geometry::Side side, ssize distance); // scrolls map with correcting specified parameters
	void selectTopMostByCurrentPos(bool group); // selects topmost object of which crosses the current cursor position
	void drawAdditionalInfo(); // draws additional information
};

}; // end Constructor

}; // end Engine
