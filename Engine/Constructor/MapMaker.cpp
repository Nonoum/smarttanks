#include "MapMaker.h"
#include "../Types/StoneWall.h"
#include "../Types/GroundFloor.h"
#include <sstream>
#include <stdexcept>

namespace Engine {

namespace Constructor {

static const size_t NO_TYPE_SELECTED = -1;

MapMaker :: MapMaker()
		: EngineCommon(false) {

	// preparing factory
	factory.loadTypes( object_transporter );
	i_type_selected = NO_TYPE_SELECTED;
	// preparing printer
	printer.create(EngineConfig::get().sFont());
	// initializing key states
	for(int key = KEY_ITERATOR; key < KEY_NKEYS; ++key) {
		last_states[key] = false;
	}
};

MapMaker :: ~MapMaker() {
};

void MapMaker :: getTypesNames(std::vector<std::string>& output) {
	factory.getTypesNames( output );
};

static inline void SetObjectSize(ObjectInterface& obj, Geometry::Size offered) {
	offered = offered.getMaximums( EngineConfig::get().minObjectSize() );
	obj.setSize(offered.width(), offered.height());
};

static void MoveObject(ObjectInterface* obj, Geometry::Side side, ssize distance) {
	assert(side != Geometry::SIDE_NONE && "A valid side expected");
	if(! obj) {
		return;
	}
	const ssize x = obj->x();
	const ssize y = obj->y();
	if(side == Geometry::SIDE_LEFT) { obj->setPoint(x - distance, y);
	} else if(side == Geometry::SIDE_RIGHT) { obj->setPoint(x + distance, y);
	} else if(side == Geometry::SIDE_DOWN) { obj->setPoint(x, y - distance);
	} else if(side == Geometry::SIDE_UP) { obj->setPoint(x, y + distance);
	}
};

static void StretchObject(ObjectInterface* obj, Geometry::Side side, ssize length) {
	assert(side != Geometry::SIDE_NONE && "A valid side expected");
	if(! obj) {
		return;
	}
	if(! obj->answer(ObjectInterface::ASK_SEPARABLE)) {
		return; // not stretchable
	}
	ssize w = obj->width();
	ssize h = obj->height();
	if(side == Geometry::SIDE_LEFT) { w = (length >= w) ? 1 : w - length;
	} else if(side == Geometry::SIDE_RIGHT) { w += length;
	} else if(side == Geometry::SIDE_DOWN) { h = (length >= h) ? 1 : h - length;
	} else if(side == Geometry::SIDE_UP) { h += length;
	}
	SetObjectSize(*obj, Geometry::Size(w, h));
};

void MapMaker :: selectTopMostByCurrentPos(bool group) {
	if(! group) {
		selection.clear();
	}
	DrawingLevel topmost(0);
	ObjectInterface* selected = NULL;

	for(CollectionType::LockIterator it(getObjects()); ! it.isDone(); ++it) {
		if((*it)->getRect() & current_pos) { // hit test
			if((! selected) || ((*it)->getLevel() > topmost)) {
				selected = *it;
				topmost = selected->getLevel();
			}
		}
	}
	if(selected != NULL) {
		if(std::find(selection.begin(), selection.end(), selected) == selection.end()) {
			selection.push_back(selected);
		}
	}
};

bool MapMaker :: handleInput(Controls key, bool key_down) {
	bool handled = true;
	const ssize step = last_states[KEY_CONTROL] ? EngineConfig::get().constructorBigStep() : 1; // step of moving/stretching
	//
	switch (key) {
	case KEY_ESC:
		selection.clear();
		break;
	case KEY_DOWN:
	case KEY_LEFT:
	case KEY_RIGHT:
	case KEY_UP:
		if(! key_down) {
			break;
		}
		if(! selection.size()) {
			scrollMap( KeyToSide(key), step );
		} else {
			if(last_states[KEY_SHIFT]) { // stretching selected object
				if(selection.size() == 1) {
					StretchObject(selection[0], KeyToSide(key), step);
				}
			} else { // moving whole selection
				for(SelectionType::iterator it = selection.begin(); it != selection.end(); ++it) {
					MoveObject(*it, KeyToSide(key), step);
				}
			}
		}
		break;
	case KEY_MOUSE_MOVE: // empty
		break;
	case KEY_MOUSE_DOUBLE:
	case KEY_MOUSE_LEFT: // object selection
		if(last_states[KEY_MOUSE_RIGHT]) {
			break; // *busy*
		}
		selectTopMostByCurrentPos(last_states[KEY_CONTROL]); // search selection

		if((selection.size() == 1) && (key == KEY_MOUSE_DOUBLE)) { // edit selection
			SelectionCallbackParams params;
			params.autopool_clone_of_selected = selection[0]->autoPoolObjectInterface(true);
			if(! SelectionCallbackStorage::sendCallback(params)) {
				params.autopool_clone_of_selected->removeFromPool();
			}
		}
		break;
	case KEY_MOUSE_RIGHT: // object addition
		if(key_down) {
			selection.clear();
			last_pos = current_pos;
		} else { // mouse up
			createObject( Geometry::Rect::positiveRect(last_pos, current_pos) );
		}
		break;
	case KEY_DELETE: // object removing
		if(selection.size()) {
			for(CollectionType::LockIterator it(getObjects()); ! it.isDone(); ) {
				SelectionType::iterator found = std::find(selection.begin(), selection.end(), *it);
				if(found != selection.end()) {
					(*it)->removeFromPool(); // removing object from pool
					selection.erase(found); // reseting selection
					it.remove(); // removing pointer from collection
					if(! selection.size()) {
						break;
					}
				} else {
					++it;
				}
			}
		}
		break;
	case KEY_RESTART: // cloning selection
		if(! key_down) {
			break;
		}
		for(size_t i = 0; i < selection.size(); ++i) {
			ObjectInterface* cloned = selection[i]->autoPoolObjectInterface(true);
			getObjects().addFirst(cloned);
			selection[i] = cloned;
		}
		break;
	default:
		handled = false;
	};
	last_states[key] = key_down;
	if(handled) {
		render(false);
	}
	return handled;
};

void MapMaker :: selectType(size_t index) {
	if(index >= factory.size()) {
		throw std::runtime_error("MapMaker::selectType error: bad type index");
	}
	i_type_selected = index;
};

void MapMaker :: createObject(Geometry::Rect offered) {
	if(i_type_selected == NO_TYPE_SELECTED) {
		return;
	}
	selection.clear();
	ObjectInterface* selected = factory.autoPoolObjectInterface(i_type_selected);
	selection.push_back(selected);
	// initializing
	selected->getPoint() = offered.getPoint(); // position
	if(selected->answer(ObjectInterface::ASK_SEPARABLE)) {
		SetObjectSize(*selected, offered.getSize());
	} // else - object has const size and it's been initialized in his constructor
	getObjects().addFirst(selected); // adding to collection
};

void MapMaker :: setCurrentCursorPos(Geometry::Point global_pos) {
	current_pos = convertCoordinates(false, global_pos - getGlobalScreenPos());
};

void MapMaker :: drawUndoneSelection() {
	if(! last_states[KEY_MOUSE_RIGHT]) {
		return;
	}
	const ssize interval = 7; // full dotted line interval
	const ssize v_interval = 5; // visible dotted line interval
	const Geometry::Rect rect = Geometry::Rect::positiveRect(last_pos, current_pos);
	const Geometry::Point pos = convertCoordinates(true, rect.getPoint());
	// TODO: add dotted rect/lines to Img32 ?
	const ssize x = pos.x();
	const ssize y = pos.y();
	const ssize wi = rect.width();
	const ssize he = rect.height();
	const uint32 color = EngineConfig::get().sCorpsesColor();
	for(ssize i = 0; i <= wi / interval; ++i) { // horizontal dotted lines
		const ssize add_start = i * interval;
		const ssize add_end = add_start + v_interval > wi ? wi : add_start + v_interval;
		getScreen().drawLine(x + add_start, y, x + add_end, y, color);
		getScreen().drawLine(x + add_start, y + he-1, x + add_end, y + he-1, color);
	}
	for(ssize i = 0; i <= he / interval; ++i) { // vertical dotted lines
		const ssize add_start = i * interval;
		const ssize add_end = add_start + v_interval > he ? he : add_start + v_interval;
		getScreen().drawLine(x, y + add_start, x, y + add_end, color);
		getScreen().drawLine(x + wi-1, y + add_start, x + wi-1, y + add_end, color);
	}
};

bool MapMaker :: render(bool forced/*unused*/) {
	Helpers::AutoLocker auto_locker(*this, render_request);
	if(! auto_locker) {
		render_request.param = forced;
		return false;
	}
	// cleaning screen
	getScreen().fillRect();
	// drawing grid
	const ssize grid_step = EngineConfig::get().gridStep();
	const uint32 grid_color = EngineConfig::get().gridColor();
	const Geometry::Point pos = convertCoordinates(true, Geometry::Point(0, 0));
	getScreen().drawGrid(pos.x(), pos.y(), 1, 1, grid_step, grid_step, grid_color);
	// drawing object corpses
	for(CollectionType::LockIterator it(getObjects()); ! it.isDone(); ++it) {
		drawObjectCorpse(*it);
	}
	drawUndoneSelection();
	drawAdditionalInfo();
	EngineCallbackParams params(&getScreen(), getGlobalScreenPos());
	DisplayController::sendCallback( params );
	return true;
};

bool MapMaker :: drawObjectCorpse(const ObjectInterface* obj) {
	// drawing corpse
	const uint32 color = (std::find(selection.begin(), selection.end(), obj) != selection.end()) ?
		EngineConfig::get().sCorpsesColor()
		: EngineConfig::get().corpsesColor();
	obj->drawCorpse(getEngineCommon(), color);
	// printing name of type
	const char* info = obj->name() ? obj->name() : "null";
	const Geometry::Point pos = convertCoordinates(true, obj->getPoint());
	printer.print(getScreen(), pos.x() + 1, pos.y() + 1, EngineConfig::get().textColor(), info);
	return true;
};

void MapMaker :: initMap(Geometry::Size map_size) {
	assert(EngineConfig::get().maxMapSize().canFit(map_size) && "Too large map, filtering is missed");
	setGameSize( map_size ); // size of game area, to add bound-objects
	clearObjects( getObjects() ); // cleaning previous objects
	const ssize depth = 2048; // depth of map borders here
	const ssize width = map_size.width();
	const ssize height = map_size.height();

	const Types::StoneWall dummy;
	const Geometry::Rect bound_obj_rects[] = {Geometry::Rect(-depth, -depth, depth, height + depth*2), // left border
											Geometry::Rect(width, -depth, depth, height + depth*2), // right border
											Geometry::Rect(0, -depth, width, depth), // bottom border
											Geometry::Rect(0, height, width, depth)}; // top border

	for(size_t i = 0; i < sizeof(bound_obj_rects)/sizeof(*bound_obj_rects); ++i) {
		ObjectInterface *obj = dummy.autoPoolObjectInterface(false);
		obj->getRect() = bound_obj_rects[i];
		getObjects().addFirst(obj);
	}
	const Types::GroundFloor floor;
	{
		ObjectInterface *flr = floor.autoPoolObjectInterface(false);
		flr->setRect(0, 0, width, height);
		getObjects().addFirst(flr);
	}
	resetDrawingOffset();
};

void MapMaker :: setParamsForSelectedObject(const std::string& params) {
	if(selection.size() != 1) {
		return;
	}
	selection[0]->set( params );
};

// requests handling

void MapMaker :: onFullUnlock() {
	EngineCommon::onFullUnlock();
	if(render_request.isSet()) {
		render(render_request.param);
	}
};

void MapMaker :: scrollMap(Geometry::Side side, ssize distance) {
	const ssize map_w = getGameSize().width();
	const ssize map_h = getGameSize().height();
	const ssize scr_w = getScreen().width();
	const ssize scr_h = getScreen().height();
	const Geometry::Point offset = convertCoordinates(false, Geometry::Point(0, 0));
	const ssize offs_x = offset.x();
	const ssize offs_y = offset.y();
	Geometry::Point adjust;

	switch (side) {
	case Geometry::SIDE_LEFT:
		distance = Helpers::min2(distance, offs_x);
		adjust.setPoint(-distance, 0);
		break;
	case Geometry::SIDE_RIGHT: {
			distance = Helpers::min2(distance, map_w - scr_w - offs_x);
			if(distance < 0) {
				return;
			}
			adjust.setPoint(distance, 0);
		}
		break;
	case Geometry::SIDE_DOWN:
		distance = Helpers::min2(distance, offs_y);
		adjust.setPoint(0, -distance);
		break;
	case Geometry::SIDE_UP: {
			distance = Helpers::min2(distance, map_h - scr_h - offs_y);
			if(distance < 0) {
				return;
			}
			adjust.setPoint(0, distance);
		}
	default:
		break;
	};
	adjustDrawingOffset( adjust );
};

void MapMaker :: drawAdditionalInfo() {
	std::ostringstream oss;
	const Geometry::Point offs = convertCoordinates(false, Geometry::Point(0, 0));
	const Geometry::Size gsize = getGameSize();
	size_t nrows = 0;
	ssize xstart, ystart, xend, yend;
	xstart = offs.x();
	ystart = offs.y();
	xend = xstart + getScreen().width();
	yend = ystart + getScreen().height();
	oss << "X: " << xstart << ".." << xend << " of " << gsize.width() << "\n"; ++nrows;
	oss << "Y: " << ystart << ".." << yend << " of " << gsize.height() << "\n"; ++nrows;
	oss << "objects: " << getObjects().getTotalCount() << "\n"; ++nrows;
	if(selection.size()) {
		oss << "has selection (" << selection.size() << ")\n"; ++nrows;
	}
	printer.print(getScreen(), 0, printer.getCHeight() * (nrows-1), ~0, oss.str().c_str(), NULL);
};

}; // end Constructor

}; // end Engine
