#include "ObjectTransporter.h"
#include "Types/BrickWall.h"
#include "Types/GroundFloor.h"
#include "Types/RockFloor.h"
#include "Types/DesertFloor.h"
#include "Types/StoneWall.h"
#include "Types/Grape.h"
#include "Types/Dynamite.h"
#include "Types/Barrels.h"
#include "Types/Player.h"
#include "Types/ArmorPick.h"
#include "Types/Enemy.h"
#include "Types/YellowBrickWall.h"
#include "Types/Speeder.h"
#include "Types/DesertGrassFloor.h"
#include "Types/DarkPlateFloor.h"
#include "Types/IronWall.h"
#include <stdexcept>
#include <assert.h>

namespace Engine {

ObjectTransporter :: ObjectTransporter() {
	uint32 current_id = 0;
	addType(++current_id, Types::BrickWall().autoPoolObjectInterface(), "Wall (Red Brick)"); // id = 1
	addType(++current_id, Types::GroundFloor().autoPoolObjectInterface(), "Floor (Ground)"); // id = 2
	addType(++current_id, Types::RockFloor().autoPoolObjectInterface(), "Floor (Rock)"); // id = 3
	addType(++current_id, Types::DesertFloor().autoPoolObjectInterface(), "Floor (Desert)"); // id = 4
	addType(++current_id, Types::StoneWall().autoPoolObjectInterface(), "Wall (Stone)"); // id = 5
	addType(++current_id, Types::Grape().autoPoolObjectInterface(), "Grape"); // id = 6
	addType(++current_id, Types::Dynamite().autoPoolObjectInterface(), "Dynamite"); // id = 7
	addType(++current_id, Types::Barrels().autoPoolObjectInterface(), "Barrels"); // id = 8
	addType(++current_id, Types::Player().autoPoolObjectInterface(), "Player"); // id = 9
	addType(++current_id, Types::ArmorPick().autoPoolObjectInterface(), "Armor"); // id = 10
	addType(++current_id, Types::Enemy().autoPoolObjectInterface(), "Enemy"); // id = 11
	addType(++current_id, Types::YellowBrickWall().autoPoolObjectInterface(), "Wall (Yellow Brick)"); // id = 12
	addType(++current_id, Types::Speeder().autoPoolObjectInterface(), "Speeder"); // id = 13
	addType(++current_id, Types::DesertGrassFloor().autoPoolObjectInterface(), "Floor (desert grass)"); // id = 14
	addType(++current_id, Types::DarkPlateFloor().autoPoolObjectInterface(), "Floor (dark plate)"); // id = 15
	addType(++current_id, Types::IronWall().autoPoolObjectInterface(), "Wall (Iron)"); // id = 16
};

ObjectTransporter :: ~ObjectTransporter() {
	for(CollectionType::iterator i = types.begin(); i != types.end(); ++i) {
		i->second->removeFromPool();
	}
	types.clear();
};

void ObjectTransporter :: bringAutoPoolCopies(std::vector<ObjectInterface*>& dst) const {
	for(CollectionType::const_iterator i = types.begin(); i != types.end(); ++i) {
		dst.push_back( i->second->autoPoolObjectInterface(true) ); // true == making clone
	}
};

usize ObjectTransporter :: write(std::ostream& dst, const ObjectInterface* object) const {
	assert(object != NULL && "A valid pointer to ObjectInterface expected");
	uint32 id = object->id();
	if(types.find(id) == types.end()) {
		throw std::runtime_error("ObjectTransporter::write error: type with this ID was not found!");
	}
	dst.write((char*)&id, sizeof(id));
	return sizeof(id) + object->write(dst);
};

ObjectInterface* ObjectTransporter :: read(std::istream& src) const {
	uint32 id = 0;
	src.read((char*)&id, sizeof(id));
	CollectionType::const_iterator iter = types.find(id);
	if(iter == types.end()) {
		throw std::runtime_error("ObjectTransporter::read error: type with this ID was not found!");
	}
	ObjectInterface* return_object = iter->second->autoPoolObjectInterface(false); // new raw object
	return_object->read(src); // reading to raw object
	return return_object;
};

void ObjectTransporter :: addType(uint32 id, ObjectInterface* object, const char* name) {
	id = object->id(id); // set (works only first time for each type)
	object->name(name); // same here
	if(types.find(id) != types.end()) {
		object->removeFromPool();
		throw std::runtime_error("ObjectTransporter::addType error: type with this ID is already registered!");
	}
	types[id] = object;
};

}; // end Engine
