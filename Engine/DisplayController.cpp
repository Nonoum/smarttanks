#include "DisplayController.h"
#include "EngineConfig.h"

namespace Engine {

EngineCallbackParams :: EngineCallbackParams(const Img32* img, Geometry::Point pos)
		: image(img), position(pos) {
};

//------------------------------------------------------

DisplayController :: DisplayController(bool _with_panel)
		: has_panel(_with_panel), render_size(0, 0), output_window_position(0, 0) {

	panel_pos.setPoint(0, 0);
};

DisplayController :: ~DisplayController() {
};

void DisplayController :: onFullUnlock() {
	if(resizing_request.isSet()) {
		setRenderSize(resizing_request.param);
	}
};

bool DisplayController :: setRenderSize(Geometry::Size size) {
	Helpers::AutoLocker auto_locker(*this, resizing_request);
	if(! auto_locker) {
		resizing_request.param = size;
		return false;
	}
	if(render_size == size) {
		return true;
	}
	render_size = size;
	screen_pos.setPoint(0, 0); // zero position as default (no panel)
	if(! size) { // zero size -> deallocating memory
		screen.dealloc();
		panel.dealloc();
		return true;
	}
	if(! has_panel) {
		screen.alloc(size.width(), size.height());
		panel.dealloc();
	} else {
		const ssize panel_h = Helpers::min2(EngineConfig::get().infoPanelHeight(), size.height());
		screen.alloc(size.width(), size.height() - panel_h);
		panel.alloc(size.width(), panel_h);
		screen_pos.setPoint(0, panel_h);
	}
	return true;
};

void DisplayController :: setOutputPosition(Geometry::Point pos) {
	output_window_position = pos;
};

Geometry::Size DisplayController :: getRenderSize() const {
	return render_size;
};

Geometry::Point DisplayController :: getOutputPosition() const {
	return output_window_position;
};

Geometry::Point DisplayController :: getGlobalScreenPos() const {
	return output_window_position + screen_pos;
};

Geometry::Point DisplayController :: getGlobalPanelPos() const {
	return output_window_position + panel_pos;
};

}; // end Engine
