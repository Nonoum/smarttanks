#pragma once

namespace Engine {

class ResourceInitializer {
public:
	enum Resources { // must be a sequence from zero, not flags. uses array of functions
		RES_FOR_SERIALIZED = 0,
		RES_FOR_GAME_ONLY,
		RES_LIBRARY,
		RES_GUI_IMAGES,
		RES_PROFILE_DATA,
		RES_GAME_GUI_IMAGES,
		RES_COMMON_GUI_IMAGES,
		RES_COUNT
	};
	ResourceInitializer(Resources required, bool forced = false);
private:
	static bool initialized[RES_COUNT];
};

}; // end Engine
