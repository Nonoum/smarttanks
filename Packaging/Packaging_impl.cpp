#include "Packaging_decl.h"
#include "../Engine/imports.h"

namespace Packaging {

#define STATIC_COUNT(arr) (sizeof(arr)/sizeof(*arr))

void PackResource(std::string path, std::string* tga_fnames, size_t count, const char* cimg_output_name) {
	Helpers::CustomAutoPtr<Img32> images(new Img32[count]);
	for(size_t i = 0; i < count; ++i) {
		images.get()[i].readTGA( (path + tga_fnames[i]).c_str() );
	}
	images.get()->writeCimgBatch(cimg_output_name, count);
};

#define GENERATE_NAMES_SEQUENCE(arr_name, start_name, count) \
	std::string arr_name[count]; \
	arr_name[0] = start_name; \
	for(size_t i = 1; i < count; ++i) { \
		arr_name[i] = Algorithms::NextName(arr_name[i-1]); \
	}

//------------------------

#define ADD_INPUT_PATH(sub_dir) "../../../_graphic_sources/"##sub_dir
#define ADD_OUTPUT_PATH(sub_dir) "../../../Output/"##sub_dir

void PackResourcesFile() {
	// don't change the order of names.
	std::string fnames[] = {
		"ground-floor.tga",
		"desert-floor.tga",
		"rock-floor.tga",
		"brick-wall.tga",
		"stone-wall.tga",
		"grape.tga",
		"dynamite.tga",
		"barrels.tga",
		"tank-green.tga",
		"tank-camouflage.tga",
		"bullet.tga",
		"yellow-brick-wall.tga",
		"desert-grass.tga",
		"dark-plate-floor.tga",
		"iron-wall.tga",
	};
	PackResource(ADD_INPUT_PATH("completed/resources/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("resources.cimgs"));
};

void PackEFireFile() {
	GENERATE_NAMES_SEQUENCE(fnames, "image0000.tga", 30);
	PackResource(ADD_INPUT_PATH("completed/efire/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("efire.cimgs"));
};

void PackBFireFile() {
	GENERATE_NAMES_SEQUENCE(fnames, "image0000.tga", 23);
	PackResource(ADD_INPUT_PATH("completed/bfire/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("bfire.cimgs"));
};

void PackBFire2File() {
	GENERATE_NAMES_SEQUENCE(fnames, "bfire2_00.tga", 23);
	PackResource(ADD_INPUT_PATH("completed/bfire2/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("bfire2.cimgs"));
};

void PackCloudFile() {
	GENERATE_NAMES_SEQUENCE(fnames, "image0000.tga", 20);
	PackResource(ADD_INPUT_PATH("completed/cloud/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("cloud.cimgs"));
};

void PackArmorFile() {
	GENERATE_NAMES_SEQUENCE(fnames, "image0000.tga", 11);
	PackResource(ADD_INPUT_PATH("completed/armor/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("armor.cimgs"));
};

void PackSSmokeFile() {
	GENERATE_NAMES_SEQUENCE(fnames, "00.tga", 11);
	PackResource(ADD_INPUT_PATH("completed/ssmoke/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("ssmoke.cimgs"));
};

void PackSpeederFile() {
	GENERATE_NAMES_SEQUENCE(fnames, "00.tga", 20);
	PackResource(ADD_INPUT_PATH("completed/speeder/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("speeder.cimgs"));
};

void PackPanelResourcesFile() {
	// don't change the order of names.
	std::string fnames[] = {
		"background.tga",
		"life.tga",
		"armor.tga",
		"numbers.tga",
		"game_over.tga",
		"you_won.tga",
		"speeder.tga",
		"coins_small.tga",
	};
	PackResource(ADD_INPUT_PATH("completed/panel_resources/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("panel_resources.cimgs"));
};

void PackGameGuiFile() {
	// don't change the order of names.
	std::string fnames[] = {
		"settings_button.tga",
		"about_button.tga",
		"back_button.tga",
		"exit_button.tga",
		"help_button.tga",
		"game_text_background.tga",
	};
	PackResource(ADD_INPUT_PATH("completed/game_gui/"), fnames, STATIC_COUNT(fnames), ADD_OUTPUT_PATH("game_gui.cimgs"));
};

void PackAll() {
	PackResourcesFile();
	PackEFireFile();
	PackBFireFile();
	PackCloudFile();
	PackArmorFile();
	PackSSmokeFile();
	PackPanelResourcesFile();
	PackGameGuiFile();
	PackSpeederFile();
	PackBFire2File();
};

}; // end Packaging
