#pragma once

namespace Packaging {

void PackResourcesFile();
void PackEFireFile();
void PackBFireFile();
void PackCloudFile();
void PackArmorFile();
void PackSSmokeFile();
void PackPanelResourcesFile();
void PackGameGuiFile();
void PackSpeederFile();
void PackBFire2File();

void PackAll();

}; // end Packaging
