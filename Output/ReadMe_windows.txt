﻿Smart Tanks 2.0.
Platform: Windows XP, 7, 8.


To start the game, launch "SmartTanks.exe" in "game" directory, then choose the level.
Ingame controls:
"Arrows" - move tank;
"Space" - shot;
"Esc" - exit to menu;
"R" - restart level;
"Shift" - stop tank;
"Ctrl" - switch manual / auto moving;
"Pause" - pause / unpause the game.

In settings menu available:
- changing game window resolution;
- changing enemy complexity level;
- option of quaking screen on explosions;
- achievements options.

Settings, scores and achievements won't be saved if you run the game from archive.

* Version 2.0's changes:
- added achievements system;
- 9 new maps;
- more textures and animations;
- improved AI for "Hard" and "Critical" levels;
- "speeder" feature.

--------------------------------------------------------------------------------------

Для старта игры запустить "SmartTanks.exe" в папке "game" и выбрать уровень.
Управление:
"Стрелки" - направить / двигать танк;
"Пробел" - выстрел;
"Esc" - выйти в меню;
"R" - рестарт уровня;
"Shift" - остановить танк;
"Ctrl" - сменить режим движения (с зажатыми клавишами / автоматический);
"Pause" - поставить / снять с паузы.

В меню настроек доступно:
- изменение размеров игрового окна;
- изменение уровня сложности противников;
- опция тряски экрана при взрывах;
- настройки улучшений.

Настройки, рекорды очков и улучшения не сохранятся при запуске игры из архива.

* Изменения версии 2.0:
- добавлена система улучшений;
- 9 новых карт;
- добавлены новые текстуры и анимации;
- улучшен интеллект соперников для уровней "Hard" и "Critical";
- фича "ускоритель".

--------------------------------------------------------------------------------------

gl & hf.