import platform

_platform_name = platform.uname()[0].lower();

def platformValue(vals_map):
	try:
		return vals_map[_platform_name];
	except KeyError:
		raise RuntimeError("Unknown platform. Current: {0}, available: {1}".format(_platform_name, vals_map.keys()))
