﻿Smart Tanks 2.0.
Platform: Linux x64 (also available for Windows XP, 7, 8).

Requirements:
- The game uses GTK+ dynamic libraries (most likely they're already installed on your machine).


To start the game, unpack the archive and launch "SmartTanks" in "SmartTanks/game/" directory, then choose the level.
Ingame controls:
"Arrows" - move tank;
"Space" - shot;
"Esc" - exit to menu;
"R" - restart level;
"Shift" - stop tank;
"Ctrl" - switch manual / auto moving;
"Pause" - pause / unpause the game.

In settings menu available:
- changing game window resolution;
- changing enemy complexity level;
- option of quaking screen on explosions;
- achievements options.

* Version 2.0's changes:
- added achievements system;
- 9 new maps;
- more textures and animations;
- improved AI for "Hard" and "Critical" levels;
- "speeder" feature.

--------------------------------------------------------------------------------------

Для старта игры - распаковать архив, запустить "SmartTanks" в папке "SmartTanks/game/" и выбрать уровень.
Управление:
"Стрелки" - направить / двигать танк;
"Пробел" - выстрел;
"Esc" - выйти в меню;
"R" - рестарт уровня;
"Shift" - остановить танк;
"Ctrl" - сменить режим движения (с зажатыми клавишами / автоматический);
"Pause" - поставить / снять с паузы.

В меню настроек доступно:
- изменение размеров игрового окна;
- изменение уровня сложности противников;
- опция тряски экрана при взрывах;
- настройки улучшений.

* Изменения версии 2.0:
- добавлена система улучшений;
- 9 новых карт;
- добавлены новые текстуры и анимации;
- улучшен интеллект соперников для уровней "Hard" и "Critical";
- фича "ускоритель".

--------------------------------------------------------------------------------------

gl & hf.
