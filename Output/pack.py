import os
from os.path import join, exists
import shutil # copy2

from utils import platformValue

common_dir = join(platformValue({'windows': 'win32', 'linux': 'linux'}), 'Smart Tanks')

game_dir = join(common_dir, 'game')
creator_dir = join(common_dir, 'creator')
stuff_dir = join(common_dir, 'stuff')

dirs_list = (game_dir, creator_dir, stuff_dir)

u_maps_dir = 'maps'

game_exec_name = platformValue({'windows': 'SmartTanks.exe', 'linux': 'SmartTanks'})
game_creator_name = platformValue({'windows': 'MapCreator.exe', 'linux': ''})
readme_source_name = platformValue({'windows': 'ReadMe_windows.txt', 'linux': 'ReadMe_linux.txt'})

mapping = dict()
## targets
mapping[game_exec_name] = join(game_dir, game_exec_name)
mapping[game_creator_name] = join(creator_dir, game_creator_name)
## common files
mapping['nnmlib1.1_fonts.cimgs'] = join(common_dir, 'nnmlib1.1_fonts.cimgs')
mapping['nnmlib1.1_gui.cimgs'] = join(common_dir, 'nnmlib1.1_gui.cimgs')
mapping['resources.cimgs'] = join(common_dir, 'resources.cimgs')
mapping['armor.cimgs'] = join(common_dir, 'armor.cimgs')
mapping['speeder.cimgs'] = join(common_dir, 'speeder.cimgs')
mapping['simple_background.cimg'] = join(common_dir, 'simple_background.cimg')
mapping['profile.dat'] = join(common_dir, 'profile.dat')
mapping[readme_source_name] = join(common_dir, 'ReadMe.txt')
## specific files
# game
mapping['game_gui.cimgs'] = join(game_dir, 'game_gui.cimgs')
mapping['bfire.cimgs'] = join(game_dir, 'bfire.cimgs')
mapping['bfire2.cimgs'] = join(game_dir, 'bfire2.cimgs')
mapping['cloud.cimgs'] = join(game_dir, 'cloud.cimgs')
mapping['efire.cimgs'] = join(game_dir, 'efire.cimgs')
mapping['ssmoke.cimgs'] = join(game_dir, 'ssmoke.cimgs')
mapping['panel_resources.cimgs'] = join(game_dir, 'panel_resources.cimgs')
mapping['game_menu_background.cimg'] = join(game_dir, 'game_menu_background.cimg')
# creator
mapping['background.tga'] = join(creator_dir, 'background.tga')
mapping['text_background.tga'] = join(creator_dir, 'text_background.tga')
# stuff
mapping[join(u_maps_dir, 'Demo level.tmap')] = join(stuff_dir, 'Demo level.tmap')
mapping[join(u_maps_dir, 'Classic.tmap')] = join(stuff_dir, 'Classic.tmap')
mapping[join(u_maps_dir, 'Make your way.tmap')] = join(stuff_dir, 'Make your way.tmap')
mapping[join(u_maps_dir, 'Labyrinth.tmap')] = join(stuff_dir, 'Labyrinth.tmap')
mapping[join(u_maps_dir, 'Massacre.tmap')] = join(stuff_dir, 'Massacre.tmap')
mapping[join(u_maps_dir, 'Trap in desert.tmap')] = join(stuff_dir, 'Trap in desert.tmap')
mapping[join(u_maps_dir, 'Angar.tmap')] = join(stuff_dir, 'Angar.tmap')
mapping[join(u_maps_dir, 'Run.tmap')] = join(stuff_dir, 'Run.tmap')
mapping[join(u_maps_dir, 'Classic big.tmap')] = join(stuff_dir, 'Classic big.tmap')
mapping[join(u_maps_dir, 'Battle field.tmap')] = join(stuff_dir, 'Battle field.tmap')
mapping[join(u_maps_dir, 'Duel.tmap')] = join(stuff_dir, 'Duel.tmap')
mapping[join(u_maps_dir, 'Willage.tmap')] = join(stuff_dir, 'Willage.tmap')
mapping[join(u_maps_dir, 'In cross fire.tmap')] = join(stuff_dir, 'In cross fire.tmap')
mapping[join(u_maps_dir, 'Alleviation.tmap')] = join(stuff_dir, 'Alleviation.tmap')
mapping[join(u_maps_dir, 'Insanity.tmap')] = join(stuff_dir, 'Insanity.tmap')

###############################
def checkoutDir(dir_name):
	if not exists(dir_name):
		os.makedirs(dir_name)

map(checkoutDir, dirs_list)

all_ok = True
for key in mapping:
	try:
		if len(key):
			shutil.copy2(key, mapping[key])
			print key + " copied to " + mapping[key]
	except IOError as e:
		print 'ERROR :: ' + str(e)
		all_ok = False

if not all_ok:
	print ""
	print "***ERRORS HAVE OCCURRED***"
os.system(platformValue({'windows': 'pause', 'linux': ''}))
