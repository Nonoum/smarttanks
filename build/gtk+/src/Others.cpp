#include <gtk/gtk.h>
#include <assert.h>
#include "WindowData.h"

std::string Engine::Windows::ROpenFileDialog(const char *szExtensions, const char *szFormatInfo) {
	return ""; // TODO:
};

std::string Engine::Windows::WOpenFileDialog(const char *szExtensions, const char *szFormatInfo) {
	return ""; // TODO:
};

void Engine::Windows::ShowMessage(const char* caption, const char* message) {
	GtkWidget* dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
							"%s\n\n%s: %s", Engine::StringFor("game_caption"), caption, message);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
};
