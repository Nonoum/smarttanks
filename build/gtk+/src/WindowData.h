#pragma once
#include "WindowCanvas.h"
#include "../../../Engine/Windows/WindowInterface.h"
#include <map>

struct WindowData {
	WindowCanvas canvas;
	Engine::Windows::WindowInterface* const client;
	GtkWidget* const window;
	size_t width;
	size_t height;
	size_t buttons_state;

	WindowData(Engine::Windows::WindowInterface* _client, GtkWidget* gtk_window)
			: client(_client), window(gtk_window), width(0), height(0) {
		buttons_state = -1;
	};
};

// windows map forward declaration
typedef std::map<Engine::Windows::WindowInterface*, WindowData*> WindowsType;
WindowsType& WindowsMap();
