#include <gtk/gtk.h>
#include "WindowCanvas.h"

WindowCanvas :: WindowCanvas()
		: data(NULL), wi(0), he(0), pixbuf(NULL) {
};

WindowCanvas :: ~WindowCanvas() {
	dealloc();
};

void WindowCanvas :: dealloc() {
	delete [] data;
	data = NULL;
	wi = 0;
	he = 0;
	if(pixbuf) {
		g_object_unref(pixbuf);
	}
	pixbuf = NULL;
};

void WindowCanvas :: alloc(size_t width, size_t height) {
	if(width != wi || height != he) {
		dealloc();
		data = (guchar*) new char[width * height * sizeof(uint32)];
		wi = width;
		he = height;
		fillAlpha();
		pixbuf = gdk_pixbuf_new_from_data(data, GDK_COLORSPACE_RGB, TRUE, 8, wi, he, wi*sizeof(uint32), NULL, NULL);
	}
};

void WindowCanvas :: fillAlpha() {
	const size_t cnt = wi * he;
	for(size_t i = 0; i < cnt; ++i) {
		data[i*4 + 3] = 0xff;
	}
};

void WindowCanvas :: draw(const Img32& img, Geometry::Point pos) {
	const usize ystart = he - 1 - pos.y();
	const usize yend = Helpers::max2<ssize>(-1, ystart - img.height());
	const usize xstart = pos.x();
	const usize xend = Helpers::min2(wi, xstart + img.width());
	for(usize ydst = yend+1, y = img.height()-1; ydst < ystart; ++ydst, --y) {
		const guchar* src = (const guchar*)(img.getData() + y*img.width());
		guchar* dst = data + ((ydst * wi + xstart) * sizeof(uint32));
		for(usize x = xstart; x < xend; ++x) {
			dst[0] = src[2];
			dst[1] = src[1];
			dst[2] = src[0];
			dst += sizeof(uint32);
			src += sizeof(uint32);
		}
	}
};

GdkPixbuf* WindowCanvas :: getPixBuf() {
	return pixbuf;
};
