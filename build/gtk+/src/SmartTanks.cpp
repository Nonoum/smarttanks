#include <gtk/gtk.h>
#include <stdexcept>
#include <sys/time.h>

#include "WindowData.h"
#include "../../../Engine/ProfileData.h"
#include "../../../Engine/EngineConfig.h"
#include "../../../Engine/ResourceInitializer.h"
#include "../../../Engine/Windows/GameWindow.h"

typedef Engine::Windows::GameWindow TargetWindow;

static gboolean timer_function(size_t period);

static size_t get_time_millis() {
	struct timeval te;
	gettimeofday(&te, NULL);
	return te.tv_sec*1000 + te.tv_usec/1000;
};

static void init_timer(size_t millis, size_t period) {
	g_timeout_add(millis, (GSourceFunc)timer_function, (gpointer)period);
};

static gboolean timer_function(size_t period) {
	if(Engine::Windows::IsQuitRequested()) {
		Engine::Windows::RequestQuit(false);
		Engine::Windows::QuitApplication();
		return FALSE;
	}
	const size_t start_time = get_time_millis();
	for(WindowsType::iterator it = WindowsMap().begin(); it != WindowsMap().end(); ++it) {
		try {
			it->first->timerProc(period);
		} catch(std::exception& e) {
			Engine::Windows::ShowMessage(Engine::StringFor("error_caption"), e.what());
		}
	}
	const size_t end_time = get_time_millis();
	const size_t millis = period - (end_time - start_time);
	if(millis < period) { // normal
		init_timer(millis, period);
	} else { // overflowed period
		init_timer(period, period);
	}
	return FALSE;
};

int main(int argc, char *argv[]) {
	gtk_init(&argc, &argv);
	const char* msg_error = "An error has occurred.";
	const char* advise_msg = "\nTry to reinstall the package.";
	try {
		// resources
		Engine::ResourceInitializer init_nnmlib(Engine::ResourceInitializer::RES_LIBRARY);
		Engine::ResourceInitializer init_common_gui(Engine::ResourceInitializer::RES_COMMON_GUI_IMAGES);
		Engine::ResourceInitializer init_data(Engine::ResourceInitializer::RES_PROFILE_DATA);
		//------------------------------------------------------------
		TargetWindow wnd;
		wnd.setRenderSize( Engine::ProfileData::get().screenSize() );
		SupplyWindow(&wnd);

		const size_t period = 1000 / Engine::EngineConfig::get().fps();
		init_timer( period, period );

		gtk_main();
	} catch (std::exception& e) {
		std::string s = e.what();
		s.append(advise_msg);
		Engine::Windows::ShowMessage(msg_error, s.c_str());
		return 1;
	} catch (...) {
		std::string s = msg_error;
		s.append(advise_msg);
		Engine::Windows::ShowMessage(msg_error, s.c_str());
		return 2;
	}
	return 0;
};
