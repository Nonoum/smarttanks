#pragma once
#include <gtk/gtk.h>
#include "../../../Engine/imports.h"

class Img32; // forward decl

class WindowCanvas {
	guchar* data;
	size_t wi;
	size_t he;
	GdkPixbuf* pixbuf;

	void dealloc();
	void fillAlpha();
public:
	WindowCanvas();
	~WindowCanvas();
	void alloc(size_t width, size_t height);
	void draw(const Img32& img, Geometry::Point pos);
	GdkPixbuf* getPixBuf();
};
