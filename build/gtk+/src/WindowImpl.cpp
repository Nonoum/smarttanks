#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <cairo.h>
#include <cairo-xlib.h>

#include <assert.h>

#include "WindowData.h"


WindowsType& WindowsMap() {
	static WindowsType wnds;
	return wnds;
};

//-----------------------------------------------------------------------------------

// on window closing

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
	WindowsType::iterator iter = WindowsMap().find( ((WindowData*)data)->client );
	if(iter != WindowsMap().end()) {
		iter->first->onClose();
		iter->first->setCallback(NULL);
		WindowsMap().erase(iter);
		delete (WindowData*)data;
	}

	if(WindowsMap().size() == 0) {
		gtk_main_quit();
	}
	return FALSE;
};

// internal closing request

static void close_window(WindowData* wdata) {
	gtk_widget_destroy(wdata->window);
	delete_event(wdata->window, NULL, wdata);
};

// keyboard part

static void handle_engine_key(GdkEventKey* event, WindowData* wdata) {
	Engine::Controls key;
	switch (event->keyval) {
	case GDK_KEY_Left: key = Engine::KEY_LEFT; break;
	case GDK_KEY_Right: key = Engine::KEY_RIGHT; break;
	case GDK_KEY_Up: key = Engine::KEY_UP; break;
	case GDK_KEY_Down: key = Engine::KEY_DOWN; break;
	case GDK_KEY_Delete: key = Engine::KEY_DELETE; break;
	case GDK_KEY_Pause: key = Engine::KEY_PAUSE; break;
	case GDK_KEY_Shift_L: key = Engine::KEY_SHIFT; break;
	case GDK_KEY_Shift_R: key = Engine::KEY_SHIFT; break;
	case GDK_KEY_Control_L: key = Engine::KEY_CONTROL; break;
	case GDK_KEY_Control_R: key = Engine::KEY_CONTROL; break;
	case GDK_KEY_Escape: key = Engine::KEY_ESC; break;
	case GDK_KEY_space: key = Engine::KEY_SHOOT; break;
	case GDK_KEY_R:
	case GDK_KEY_r:
		key = Engine::KEY_RESTART;
		break;
	default:
		return;
	};
	wdata->client->handleInput(key, (event->type == GDK_KEY_PRESS), Geometry::Point());
};

static void handle_gui_key(GdkEventKey* event, WindowData* wdata) {
	Graphic32::GUI::Controls key;
	switch (event->keyval) {
	case GDK_KEY_Left: key = Graphic32::GUI::KEY_LEFT; break;
	case GDK_KEY_Right: key = Graphic32::GUI::KEY_RIGHT; break;
	case GDK_KEY_Up: key = Graphic32::GUI::KEY_UP; break;
	case GDK_KEY_Down: key = Graphic32::GUI::KEY_DOWN; break;
	case GDK_KEY_Shift_L: key = Graphic32::GUI::KEY_CAPTURE; break;
	case GDK_KEY_Shift_R: key = Graphic32::GUI::KEY_CAPTURE; break;
	case GDK_KEY_Page_Up: key = Graphic32::GUI::KEY_YSTART; break;
	case GDK_KEY_Page_Down: key = Graphic32::GUI::KEY_YEND; break;
	case GDK_KEY_Home: key = Graphic32::GUI::KEY_XSTART; break;
	case GDK_KEY_End: key = Graphic32::GUI::KEY_XEND; break;
	case GDK_KEY_Insert:
		if(event->state & GDK_CONTROL_MASK) {
			key = Graphic32::GUI::KEY_COPY;
		} else if(event->state & GDK_SHIFT_MASK) {
			key = Graphic32::GUI::KEY_PASTE;
		} else {
			return;
		}
		break;
	default:
		return;
	};
	wdata->client->handleInput(key, (event->type == GDK_KEY_PRESS), Geometry::Point());
};

static gboolean key_press(GtkWidget* widget, GdkEventKey* event, gpointer data) {
	WindowData* const wdata = ((WindowData*)data);
	handle_engine_key(event, wdata);
	handle_gui_key(event, wdata);
	return FALSE;
};

// mouse part

static gboolean button_press(GtkWidget* widget, GdkEventButton *event, gpointer data) {
	WindowData* const wdata = ((WindowData*)data);
	const Geometry::Point pos(event->x, wdata->height - 1 - event->y);
	const size_t state_diff = event->state ^ wdata->buttons_state;
	wdata->buttons_state = event->state;

	if(state_diff & GDK_BUTTON1_MASK) { // left mouse button
		bool key_down = event->state & GDK_BUTTON1_MASK ? false : true;
		wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_LEFT, key_down, pos);
		wdata->client->handleInput(Engine::KEY_MOUSE_LEFT, key_down, pos);
	} else if(state_diff & GDK_BUTTON3_MASK) { // right mouse button
		bool key_down = event->state & GDK_BUTTON3_MASK ? false : true;
		wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_RIGHT, key_down, pos);
		wdata->client->handleInput(Engine::KEY_MOUSE_RIGHT, key_down, pos);
	}
	// TODO: handle doubleclick
	return FALSE;
};

static gboolean mouse_motion(GtkWidget* widget, GdkEventButton *event, gpointer data) {
	WindowData* const wdata = ((WindowData*)data);
	const Geometry::Point pos(event->x, wdata->height - 1 - event->y);
	wdata->client->handleInput(Graphic32::GUI::KEY_MOUSE_MOVE, false, pos);
	wdata->client->handleInput(Engine::KEY_MOUSE_MOVE, false, pos);
	return FALSE;
};

// drawing part

static gboolean on_drawingarea_event(GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	cairo_t * const cr = gdk_cairo_create(widget->window);
	const GdkPixbuf* pbuf = ((WindowData*)data)->canvas.getPixBuf();
	gdk_cairo_set_source_pixbuf(cr, pbuf, 0, 0);
	cairo_rectangle(cr, 0, 0, gdk_pixbuf_get_width(pbuf), gdk_pixbuf_get_height(pbuf));
	cairo_fill(cr);
	cairo_destroy(cr);
	return FALSE;
};

// callback implementation

class CallbackImpl : public Engine::Windows::WindowCallbackBase {
	WindowData* const wdata;
public:
	CallbackImpl(WindowData* wd) : wdata(wd) {
	};
	bool action(Engine::Windows::WindowCallbackParams& params) {
		if(params.cmd == Engine::Windows::WND_DRAW) {
			if(params.screen && params.index == 0) {
				wdata->canvas.draw(*params.screen, params.pos);
				gtk_widget_queue_draw(wdata->window);
			}
		} else if(params.cmd == Engine::Windows::WND_EXIT) {
			close_window(wdata);
		}
		return false;
	};
};

// engine implementations

static void set_window_geometry(WindowData* wdata) {
	GdkGeometry geo;
	geo.min_width = wdata->width;
	geo.max_width = wdata->width;
	geo.min_height = wdata->height;
	geo.max_height = wdata->height;
	gtk_window_set_geometry_hints(GTK_WINDOW(wdata->window), NULL, &geo, GdkWindowHints(GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE));
};

bool Engine::Windows::SupplyWindow(WindowInterface* client) {
	if(! client) {
		return false;
	}
	WindowsType::const_iterator iter = WindowsMap().find(client);
	if(iter != WindowsMap().end()) {
		const Geometry::Size sz = iter->first->getRenderSize();
		WindowData* const wdata = iter->second;
		if(sz != Geometry::Size(wdata->width, wdata->height)) {
			wdata->width = sz.width();
			wdata->height = sz.height();
			wdata->canvas.alloc(wdata->width, wdata->height);
			wdata->client->forcedRender();
			set_window_geometry(wdata);
			gtk_window_resize(GTK_WINDOW(wdata->window), wdata->width, wdata->height);
		}
		return true;
	}
	GtkWidget* const window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	WindowData* const wdata = new WindowData(client, window);
	WindowsMap()[client] = wdata;
	client->setCallback(new CallbackImpl(wdata));

	gtk_window_set_title(GTK_WINDOW(window), client->getWindowName());
	gtk_widget_set_events(window, GDK_BUTTON_PRESS_MASK
				| GDK_BUTTON_RELEASE_MASK
				| GDK_BUTTON_MOTION_MASK
				| GDK_POINTER_MOTION_MASK);

	g_signal_connect(window, "delete-event", G_CALLBACK(delete_event), wdata);

	{
		wdata->width = client->getRenderSize().width();
		wdata->height = client->getRenderSize().height();
		if(! wdata->width) wdata->width = 320;
		if(! wdata->height) wdata->height = 240;
		wdata->canvas.alloc(wdata->width, wdata->height);
		gtk_window_set_default_size(GTK_WINDOW(window), wdata->width, wdata->height);
		set_window_geometry(wdata);
		client->forcedRender();
	}

	g_signal_connect(window, "key_press_event", (GtkSignalFunc)key_press, wdata);
	g_signal_connect(window, "key_release_event", (GtkSignalFunc)key_press, wdata);

	g_signal_connect(window, "button_press_event", (GtkSignalFunc)button_press, wdata);
	g_signal_connect(window, "button_release_event", (GtkSignalFunc)button_press, wdata);
	g_signal_connect(window, "motion_notify_event", (GtkSignalFunc)mouse_motion, wdata);

	GtkWidget* drawing_area = gtk_drawing_area_new();
	g_signal_connect(G_OBJECT(drawing_area), "expose-event", G_CALLBACK(on_drawingarea_event), wdata);
	gtk_container_add(GTK_CONTAINER(window), drawing_area);

	gtk_widget_show_all(window);
	return true;
};

void Engine::Windows::QuitApplication() {
	while(WindowsMap().begin() != WindowsMap().end()) {
		close_window(WindowsMap().begin()->second);
	}
};
