// SmartTanks.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "Resource.h"
#include <stdexcept>

#include "../../../Engine/ProfileData.h"
#include "../../../Engine/EngineConfig.h"

#define BUILD_GAME // builds game (map-creator in other way)

//#define BUILD_PKG // builds packager

#ifdef BUILD_GAME //---------------------
#include "../../../Engine/Windows/GameWindow.h"
using namespace Engine::Windows;

typedef GameWindow TargetWindow;
#include "mmsystem.h"
#pragma comment (lib, "winmm.lib")
#else
#include "../../../Engine/Windows/ConstructorWindow.h"
using namespace Engine::Windows;

typedef ConstructorWindow TargetWindow;
#endif //--------------------------------

#ifdef BUILD_PKG
#include "../../../Packaging/Packaging_decl.h"
#endif

#include "../../../Engine/ResourceInitializer.h"
#include <map>

HINSTANCE hInstance;

typedef std::map<HWND, WindowInterface*> WindowsMap;
WindowsMap windows_map;

static volatile bool timer_processing = false;

// timer ----------------------------------------------------
#ifdef BUILD_GAME

void CALLBACK TimerProc(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2) {
	if(timer_processing) {
		return;
	}
	for(WindowsMap::iterator it = windows_map.begin(); it != windows_map.end(); ++it) {
		PostMessage(it->first, WM_USER, dwUser, 0);
		break;
	}
};

class WindowsTimer {
	MMRESULT mmtimer;
public:
	WindowsTimer(size_t milliseconds) {
		mmtimer = 0;
		TIMECAPS tc;
		int period;
		DWORD_PTR userParam = milliseconds;

		timeGetDevCaps(&tc, sizeof(TIMECAPS));
		if( tc.wPeriodMin > milliseconds*2 ) {
			throw std::runtime_error(Engine::StringFor("error_init_timer"));
		}
		period = tc.wPeriodMin <= milliseconds ? milliseconds : tc.wPeriodMin;
		timeBeginPeriod(1);
		mmtimer = timeSetEvent(period, 1, TimerProc, userParam, TIME_PERIODIC);
	};
	~WindowsTimer() {
		timeEndPeriod(1);
		timeKillEvent(mmtimer);
	};
};

#endif

// main ----------------------------------------------------

#include "../../../Engine/AnimationController.h"

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow) {
#ifdef BUILD_PKG
	Packaging::PackAll();
	return 0;
#endif
	::hInstance = hInstance;
	const char* msg_error = "An error has occurred.";
	const char* advise_msg = "\nTry to reinstall the package.";
	try {
		// resources
		Engine::ResourceInitializer init_nnmlib(Engine::ResourceInitializer::RES_LIBRARY);
		Engine::ResourceInitializer init_common_gui(Engine::ResourceInitializer::RES_COMMON_GUI_IMAGES);
		Engine::ResourceInitializer init_data(Engine::ResourceInitializer::RES_PROFILE_DATA);
		//------------------------------------------------------------
		TargetWindow wnd;
		wnd.setRenderSize( Engine::ProfileData::get().screenSize() );
		SupplyWindow(&wnd);
#ifdef BUILD_GAME
		WindowsTimer w_timer(1000/Engine::EngineConfig::get().fps());
#endif
		//------------------------------------------------------------
		MSG msg;
		while(windows_map.size() > 0) {
			while(GetMessage(&msg, NULL, 0, 0)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				if(Engine::Windows::IsQuitRequested()) {
					Engine::Windows::RequestQuit(false);
					Engine::Windows::QuitApplication();
				}
			}
		}
		return msg.wParam;
	} catch (std::exception& e) {
		std::string s = e.what();
		s.append(advise_msg);
		Engine::Windows::ShowMessage(msg_error, s.c_str());
		return 1;
	} catch (...) {
		std::string s = msg_error;
		s.append(advise_msg);
		Engine::Windows::ShowMessage(msg_error, s.c_str());
		return 2;
	}
	return 0;
};

//---------------------------------------------------------------------------------------------------

void DrawInHDC(HDC hdc, Geometry::Point wnd_pos, Geometry::Rect area, const Graphic32::Img32& img) {
	if(! img) {
		return;
	}
	BITMAPINFO bm_info;
	bm_info.bmiHeader.biSize = 40;
	bm_info.bmiHeader.biSizeImage = 0;
	bm_info.bmiHeader.biWidth = img.width();
	bm_info.bmiHeader.biHeight = img.height();
	bm_info.bmiHeader.biPlanes = 1;
	bm_info.bmiHeader.biBitCount = 32;
	bm_info.bmiHeader.biCompression = BI_RGB;
	int x = wnd_pos.x() + area.x();
	int y = wnd_pos.y() + img.height() - area.y() - area.height();
	int xr = x + area.width();
	int yu = y + area.height();
	SetDIBitsToDevice(hdc, x, y, area.width(), area.height(),
		area.x(), 0, 0, area.height(), img.getData(), &bm_info, DIB_RGB_COLORS);
};

class DrawingCallback : public WindowCallbackBase {
	HWND hwnd;
	HDC hdc;
public:
	DrawingCallback(HWND _hwnd, HDC _hdc) : hwnd(_hwnd), hdc(_hdc) {
	};
	bool action(WindowCallbackParams& params) {
		if(params.cmd == WND_DRAW) {
			if(params.screen) {
				if(params.index == 0) {
					Geometry::Rect rect = params.area; // ... TODO: find how to use SetDIBitsToDevice to draw part of image
					rect.setRect(0, 0, params.screen->width(), params.screen->height()); // yest drawing full image
					// recalculating from-down-to-up to from-up-to-down coordinates
					Geometry::Size total = params.total_window_size;
					Geometry::Point pos = params.pos;
					pos.setPoint( params.pos.x(), total.height() - (params.pos.y() + params.screen->height()) );
					DrawInHDC(hdc, pos, rect, *params.screen);
				}
			}
		} else if(params.cmd == WND_EXIT) {
			DestroyWindow(hwnd);
		}
		return false;
	};
};

// gui handlers

bool HandleButtonGUIInput(WPARAM wParam, bool key_down, WindowInterface* window) {
	Graphic32::GUI::Controls key;
	Geometry::Point pos;

	switch (wParam) {
	case VK_LEFT: key = Graphic32::GUI::KEY_LEFT; break;
	case VK_RIGHT: key = Graphic32::GUI::KEY_RIGHT; break;
	case VK_UP: key = Graphic32::GUI::KEY_UP; break;
	case VK_DOWN: key = Graphic32::GUI::KEY_DOWN; break;
	case VK_SHIFT: key = Graphic32::GUI::KEY_CAPTURE; break;
	case VK_PRIOR: key = Graphic32::GUI::KEY_YSTART; break;
	case VK_NEXT: key = Graphic32::GUI::KEY_YEND; break;
	case VK_HOME: key = Graphic32::GUI::KEY_XSTART; break;
	case VK_END: key = Graphic32::GUI::KEY_XEND; break;
	case VK_INSERT:
		if(GetKeyState(VK_SHIFT) & (~1)) {
			key = Graphic32::GUI::KEY_PASTE;
		} else if(GetKeyState(VK_CONTROL) & (~1)) {
			key = Graphic32::GUI::KEY_COPY;
		} else {
			return false;
		}
		break;
	default:
		return false;
	};
	return window->handleInput(key, key_down, pos);
};

bool HandleMouseGUIInput(WPARAM wParam, LPARAM lParam, UINT message, WindowInterface* window) {
	Graphic32::GUI::Controls key;
	int x = LOWORD(lParam);
	int y = HIWORD(lParam);
	int height = window->getRenderSize().height();
	Geometry::Point pos = Geometry::Point(x, height - y);
	bool key_down = false;

	switch (message) {
	case WM_LBUTTONDOWN: key = Graphic32::GUI::KEY_MOUSE_LEFT; key_down = true; break;
	case WM_LBUTTONUP: key = Graphic32::GUI::KEY_MOUSE_LEFT; key_down = false; break;
	case WM_RBUTTONDOWN: key = Graphic32::GUI::KEY_MOUSE_RIGHT; key_down = true; break;
	case WM_RBUTTONUP: key = Graphic32::GUI::KEY_MOUSE_RIGHT; key_down = false; break;
	case WM_MOUSEMOVE: key = Graphic32::GUI::KEY_MOUSE_MOVE; key_down = false; break;
	case WM_LBUTTONDBLCLK: key = Graphic32::GUI::KEY_MOUSE_DOUBLE; key_down = true; break;
	default:
		return false;
	};
	return window->handleInput(key, key_down, pos);
};

// engine handlers

bool HandleButtonEngineInput(WPARAM wParam, bool key_down, WindowInterface* window) {
	Engine::Controls key;
	Geometry::Point pos;

	switch (wParam) {
	case VK_LEFT: key = Engine::KEY_LEFT; break;
	case VK_RIGHT: key = Engine::KEY_RIGHT; break;
	case VK_UP: key = Engine::KEY_UP; break;
	case VK_DOWN: key = Engine::KEY_DOWN; break;
	case VK_DELETE: key = Engine::KEY_DELETE; break;
	case VK_PAUSE: key = Engine::KEY_PAUSE; break;
	case VK_SHIFT: key = Engine::KEY_SHIFT; break;
	case VK_CONTROL: key = Engine::KEY_CONTROL; break;
	case VK_ESCAPE: key = Engine::KEY_ESC; break;
	case 'R': key = Engine::KEY_RESTART; break;
	case VK_SPACE: key = Engine::KEY_SHOOT; break;
	default:
		return false;
	};
	return window->handleInput(key, key_down, pos);
};

bool HandleMouseEngineInput(WPARAM wParam, LPARAM lParam, UINT message, WindowInterface* window) {
	Engine::Controls key;
	int x = LOWORD(lParam);
	int y = HIWORD(lParam);
	int height = window->getRenderSize().height();
	Geometry::Point pos = Geometry::Point(x, height - y);
	bool key_down = false;

	switch (message) {
	case WM_LBUTTONDOWN: key = Engine::KEY_MOUSE_LEFT; key_down = true; break;
	case WM_LBUTTONUP: key = Engine::KEY_MOUSE_LEFT; key_down = false; break;
	case WM_RBUTTONDOWN: key = Engine::KEY_MOUSE_RIGHT; key_down = true; break;
	case WM_RBUTTONUP: key = Engine::KEY_MOUSE_RIGHT; key_down = false; break;
	case WM_MOUSEMOVE: key = Engine::KEY_MOUSE_MOVE; key_down = false; break;
	case WM_LBUTTONDBLCLK: key = Engine::KEY_MOUSE_DOUBLE; key_down = true; break;
	default:
		return false;
	};
	return window->handleInput(key, key_down, pos);
};

void SetClientSize(HWND hwnd, int client_width, int client_height) {
	if(IsWindow(hwnd)) {
		DWORD dwStyle = GetWindowLongPtr(hwnd, GWL_STYLE);
		DWORD dwExStyle = GetWindowLongPtr(hwnd, GWL_EXSTYLE);
		RECT rc = {0, 0, client_width, client_height};

		AdjustWindowRectEx(&rc, dwStyle, FALSE, dwExStyle);
		SetWindowPos(hwnd, NULL, 0, 0, rc.right - rc.left, rc.bottom - rc.top, SWP_NOZORDER | SWP_NOMOVE);
	}
};

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	WindowInterface* window = NULL;

	WindowsMap::iterator it = windows_map.find(hwnd);
	if(it == windows_map.end()) {
		// on window creating
		it = windows_map.find(0); // initial storage
		if(it == windows_map.end()) {
			return 0;
		}
		window = it->second;
		windows_map.erase(0);
		windows_map[hwnd] = window;
	} else {
		window = it->second;
	}

	switch (message)
	{
	case WM_CREATE:
		window->setCallback( new DrawingCallback(hwnd, GetDC(hwnd)) );
		return 0;

	case WM_SIZE: {
			const Geometry::Size os_size(LOWORD(lParam), HIWORD(lParam));
			const Geometry::Size render_size = window->getRenderSize();
			if(render_size == os_size) {
				SetFocus(hwnd);
				return 0;
			}
			SetClientSize(hwnd, render_size.width(), render_size.height());
		}
		return 0;

	case WM_KEYUP:
		HandleButtonGUIInput(wParam, false, window);
		HandleButtonEngineInput(wParam, false, window);
		return 0;

	case WM_KEYDOWN:
		HandleButtonGUIInput(wParam, true, window);
		HandleButtonEngineInput(wParam, true, window);
		return 0;

	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDBLCLK:
		HandleMouseGUIInput(wParam, lParam, message, window);
		HandleMouseEngineInput(wParam, lParam, message, window);
		return 0;

	case WM_CHAR:
		if(window)
			window->handleInput(wParam, true);
		return 0;

	case WM_PAINT:
		window->forcedRender();
		break;

	case WM_ERASEBKGND: // must be empty
		return 0;

	case WM_USER:
		timer_processing = true;
		for(WindowsMap::iterator it = windows_map.begin(); it != windows_map.end(); ++it) {
			try {
				it->second->timerProc( wParam );
			} catch (std::exception& e) {
				Engine::Windows::ShowMessage(Engine::StringFor("error_caption"), e.what());
			}
		}
		timer_processing = false;
		return 0;

	case WM_DESTROY:
		window->onClose();
		window->setCallback(NULL); // removing callback
		windows_map.erase(hwnd); // designating window
		PostQuitMessage(0);
		return 0;

	default:
		break;
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
};

bool Engine::Windows::SupplyWindow(WindowInterface* client) {
	if(! client) {
		return false;
	}
	// checking if window for this client is already exist
	for(WindowsMap::const_iterator it = windows_map.begin(); it != windows_map.end(); ++it) {
		if(it->second == client) { // window for this client is already exist
			RECT rc;
			GetClientRect(it->first, &rc);
			const int width = rc.right - rc.left;
			const int height = rc.bottom - rc.top;
			SendMessage(it->first, WM_SIZE, 0, MAKELONG(width, height));
			return true;
		}
	}
	const long WINDOWSTYLE = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
	static bool registered = false;
	static WNDCLASSEX wndclass;
	static const char* class_name = "Smart Tanks Window class";

	const char* window_name = client->getWindowName();
	if(! client->getRenderSize()) {
		assert(false && "non-initialized window size");
		client->setRenderSize(Geometry::Size(640, 480));
	}
	const Geometry::Size window_size = client->getRenderSize();

	if(! registered) {
		// registering class for windows
		wndclass.cbSize        = sizeof(wndclass);
		wndclass.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndclass.lpfnWndProc   = WndProc;
		wndclass.cbClsExtra    = 0;
		wndclass.cbWndExtra    = 0;
		wndclass.hInstance     = hInstance;
		wndclass.hIcon         = LoadIcon(hInstance,(LPSTR)IDI_SMARTTANKS);
		wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		wndclass.lpszMenuName  = NULL;
		wndclass.lpszClassName = class_name;
		wndclass.hIconSm       = LoadIcon(hInstance,(LPSTR)IDI_SMARTTANKS);

		RegisterClassEx(&wndclass);
		registered = true;
	}
	// creating window and assigning client to it
	windows_map[0] = client; // saving in pending place
	HWND hwnd = CreateWindow(class_name, window_name,
		WINDOWSTYLE,
		CW_USEDEFAULT, CW_USEDEFAULT,
		window_size.width(), window_size.height(),
		NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, SW_NORMAL);
	UpdateWindow(hwnd);
	return true;
};

// file dialogs

inline void CommonInitOFN(OPENFILENAME &ofn) {
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = NULL;
	ofn.hInstance = NULL;
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 0;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle = NULL;
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = NULL;
	ofn.lCustData = NULL;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
}; // end common init open file name struct

std::string Engine::Windows::ROpenFileDialog(const char *szExtensions, const char *szFormatInfo) {
	char buffer[1024];
	strcpy_s(buffer, sizeof(buffer), szExtensions);
	// struct
	OPENFILENAME ofn;
	CommonInitOFN(ofn);
	ofn.lpstrFilter = szFormatInfo; // format info
	ofn.lpstrFile = buffer;
	ofn.nMaxFile = sizeof(buffer);
	ofn.Flags = OFN_OVERWRITEPROMPT | OFN_EXTENSIONDIFFERENT;
	if(! GetOpenFileName(&ofn)) {
		return std::string("");
	}
	return std::string(buffer);
}; // end R(read) open file dialog

std::string Engine::Windows::WOpenFileDialog(const char *szExtensions, const char *szFormatInfo) {
	char buffer[1024];
	strcpy_s(buffer, sizeof(buffer), szExtensions);
	// struct
	OPENFILENAME ofn;
	CommonInitOFN(ofn);
	ofn.lpstrFilter = szFormatInfo; // format info
	ofn.lpstrFile = buffer;
	ofn.nMaxFile = sizeof(buffer);
	ofn.Flags = OFN_OVERWRITEPROMPT | OFN_EXTENSIONDIFFERENT;
	if(! GetSaveFileName(&ofn)) {
		return std::string("");
	}
	return std::string(buffer);
}; // end W(write) open file dialog

void Engine::Windows::ShowMessage(const char* caption, const char* message) {
	MessageBox(0, message, caption, MB_OK | MB_ICONERROR);
};

void Engine::Windows::QuitApplication() {
	for(WindowsMap::iterator it = windows_map.begin(); it != windows_map.end(); ++it) {
		PostMessage(it->first, WM_DESTROY, 0, 0);
	}
};
